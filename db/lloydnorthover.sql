-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 05, 2017 at 11:03 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.15

SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lloydnorthover`
--

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_aiowps_events`
--

CREATE TABLE `ln2017_aiowps_events` (
  `id` bigint(20) NOT NULL,
  `event_type` varchar(150) NOT NULL DEFAULT '',
  `username` varchar(150) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) DEFAULT NULL,
  `referer_info` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `country_code` varchar(50) DEFAULT NULL,
  `event_data` longtext
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_aiowps_failed_logins`
--

CREATE TABLE `ln2017_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) NOT NULL DEFAULT ''
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_aiowps_global_meta`
--

CREATE TABLE `ln2017_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) NOT NULL,
  `meta_key2` varchar(255) NOT NULL,
  `meta_key3` varchar(255) NOT NULL,
  `meta_key4` varchar(255) NOT NULL,
  `meta_key5` varchar(255) NOT NULL,
  `meta_value1` varchar(255) NOT NULL,
  `meta_value2` text NOT NULL,
  `meta_value3` text NOT NULL,
  `meta_value4` longtext NOT NULL,
  `meta_value5` longtext NOT NULL
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_aiowps_login_activity`
--

CREATE TABLE `ln2017_aiowps_login_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) NOT NULL DEFAULT '',
  `login_country` varchar(150) NOT NULL DEFAULT '',
  `browser_type` varchar(150) NOT NULL DEFAULT ''
) ;

--
-- Dumping data for table `ln2017_aiowps_login_activity`
--

INSERT INTO `ln2017_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'joesayegh', '2017-07-03 10:15:37', '2017-07-03 14:58:53', '::1', '', ''),
(2, 1, 'joesayegh', '2017-07-03 13:58:18', '2017-07-03 14:58:53', '::1', '', ''),
(3, 1, 'joesayegh', '2017-07-03 14:59:15', '2017-07-03 15:59:16', '::1', '', ''),
(4, 1, 'joesayegh', '2017-07-03 15:59:27', '2017-07-04 13:00:02', '::1', '', ''),
(5, 1, 'joesayegh', '2017-07-03 16:27:54', '2017-07-04 13:00:02', '::1', '', ''),
(6, 1, 'joesayegh', '2017-07-03 17:18:45', '2017-07-04 13:00:02', '::1', '', ''),
(7, 1, 'joesayegh', '2017-07-04 09:57:46', '2017-07-04 13:00:02', '::1', '', ''),
(8, 1, 'joesayegh', '2017-07-04 13:27:37', '0000-00-00 00:00:00', '::1', '', ''),
(9, 1, 'joesayegh', '2017-07-04 15:23:16', '0000-00-00 00:00:00', '::1', '', ''),
(10, 1, 'joesayegh', '2017-07-05 09:49:44', '0000-00-00 00:00:00', '::1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_aiowps_login_lockdown`
--

CREATE TABLE `ln2017_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) NOT NULL DEFAULT '',
  `lock_reason` varchar(128) NOT NULL DEFAULT '',
  `unlock_key` varchar(128) NOT NULL DEFAULT ''
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_aiowps_permanent_block`
--

CREATE TABLE `ln2017_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL,
  `blocked_ip` varchar(100) NOT NULL DEFAULT '',
  `block_reason` varchar(128) NOT NULL DEFAULT '',
  `country_origin` varchar(50) NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0'
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_commentmeta`
--

CREATE TABLE `ln2017_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_comments`
--

CREATE TABLE `ln2017_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_links`
--

CREATE TABLE `ln2017_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_options`
--

CREATE TABLE `ln2017_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ;

--
-- Dumping data for table `ln2017_options`
--

INSERT INTO `ln2017_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8888/lloydnorthover', 'yes'),
(2, 'home', 'http://localhost:8888/lloydnorthover', 'yes'),
(3, 'blogname', 'Lloyd Northover', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'joesayegh@square-solve.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:87:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:37:\"disable-comments/disable-comments.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'ln2017', 'yes'),
(41, 'stylesheet', 'ln2017', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '0', 'yes'),
(59, 'thumbnail_size_h', '0', 'yes'),
(60, 'thumbnail_crop', '', 'yes'),
(61, 'medium_size_w', '0', 'yes'),
(62, 'medium_size_h', '0', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '0', 'yes'),
(65, 'large_size_h', '0', 'yes'),
(66, 'image_default_link_type', '', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Europe/London', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '2', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'ln2017_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'cron', 'a:5:{i:1499245765;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1499245778;a:1:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1499245889;a:2:{s:23:\"aiowps_daily_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:24:\"aiowps_hourly_cron_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1499247947;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(108, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1497863743;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(112, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-4.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-4.8.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-4.8-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-4.8-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"4.8\";s:7:\"version\";s:3:\"4.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1499244412;s:15:\"version_checked\";s:3:\"4.8\";s:12:\"translations\";a:0:{}}', 'no'),
(121, 'can_compress_scripts', '1', 'no'),
(137, '_transient_timeout_plugin_slugs', '1499331498', 'no'),
(138, '_transient_plugin_slugs', 'a:3:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";i:2;s:37:\"disable-comments/disable-comments.php\";}', 'no'),
(139, 'recently_activated', 'a:1:{s:27:\"wp-optimize/wp-optimize.php\";i:1499075482;}', 'yes'),
(144, 'disable_comments_options', 'a:5:{s:19:\"disabled_post_types\";a:3:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:10:\"attachment\";}s:17:\"remove_everywhere\";b:1;s:9:\"permanent\";b:0;s:16:\"extra_post_types\";b:0;s:10:\"db_version\";i:6;}', 'yes'),
(151, 'WPLANG', '', 'yes'),
(155, 'current_theme', 'ln2017', 'yes'),
(156, 'theme_mods_ln2017', 'a:2:{i:0;b:0;s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(157, 'theme_switched', '', 'yes'),
(159, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1499244422;s:7:\"checked\";a:1:{s:6:\"ln2017\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(166, 'acf_version', '5.5.14', 'yes'),
(539, '_site_transient_timeout_browser_1eab6c1f14471261b4cb2e511a8d9b95', '1499245781', 'no'),
(540, '_site_transient_browser_1eab6c1f14471261b4cb2e511a8d9b95', 'a:9:{s:8:\"platform\";s:9:\"Macintosh\";s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:6:\"10.1.1\";s:10:\"update_url\";s:28:\"http://www.apple.com/safari/\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/safari.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/safari.png\";s:15:\"current_version\";s:1:\"5\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(652, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1499082986', 'no'),
(653, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4344;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2483;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2352;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:2168;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1821;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1585;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1563;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1429;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1344;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1330;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1324;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1269;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1261;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1110;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1051;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1040;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:989;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:932;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:816;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:795;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:793;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:766;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:760;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:667;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:658;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:657;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:650;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:644;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:635;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:633;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:614;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:594;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:593;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:582;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:582;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:570;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:569;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:557;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:552;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:537;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:531;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:531;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:518;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:507;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:500;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:492;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:490;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:477;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:467;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:467;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:465;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:463;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:441;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:437;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:435;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:434;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:434;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:421;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:419;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:409;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:405;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:402;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:399;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:398;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:396;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:394;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:378;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:376;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:370;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:368;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:350;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:340;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:332;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:331;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:330;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:326;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:324;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:322;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:320;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:320;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:318;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:314;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:308;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:297;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:297;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:290;}s:3:\"tag\";a:3:{s:4:\"name\";s:3:\"tag\";s:4:\"slug\";s:3:\"tag\";s:5:\"count\";i:290;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:288;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:288;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:283;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:282;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:277;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:277;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:276;}s:8:\"lightbox\";a:3:{s:4:\"name\";s:8:\"lightbox\";s:4:\"slug\";s:8:\"lightbox\";s:5:\"count\";i:273;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:271;}s:14:\"administration\";a:3:{s:4:\"name\";s:14:\"administration\";s:4:\"slug\";s:14:\"administration\";s:5:\"count\";i:265;}s:7:\"captcha\";a:3:{s:4:\"name\";s:7:\"captcha\";s:4:\"slug\";s:7:\"captcha\";s:5:\"count\";i:264;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:263;}s:5:\"cache\";a:3:{s:4:\"name\";s:5:\"cache\";s:4:\"slug\";s:5:\"cache\";s:5:\"count\";i:263;}}', 'no'),
(658, 'aiowpsec_db_version', '1.9', 'yes'),
(659, 'aio_wp_security_configs', 'a:82:{s:19:\"aiowps_enable_debug\";s:0:\"\";s:36:\"aiowps_remove_wp_generator_meta_info\";s:1:\"1\";s:25:\"aiowps_prevent_hotlinking\";s:1:\"1\";s:28:\"aiowps_enable_login_lockdown\";s:1:\"1\";s:28:\"aiowps_allow_unlock_requests\";s:0:\"\";s:25:\"aiowps_max_login_attempts\";i:5;s:24:\"aiowps_retry_time_period\";i:5;s:26:\"aiowps_lockout_time_length\";i:60;s:28:\"aiowps_set_generic_login_msg\";s:0:\"\";s:26:\"aiowps_enable_email_notify\";s:0:\"\";s:20:\"aiowps_email_address\";s:26:\"joesayegh@square-solve.com\";s:27:\"aiowps_enable_forced_logout\";s:1:\"1\";s:25:\"aiowps_logout_time_period\";i:180;s:39:\"aiowps_enable_invalid_username_lockdown\";s:0:\"\";s:43:\"aiowps_instantly_lockout_specific_usernames\";a:0:{}s:32:\"aiowps_unlock_request_secret_key\";s:20:\"shyx1lzp7u82fgn0escn\";s:35:\"aiowps_lockdown_enable_whitelisting\";s:0:\"\";s:36:\"aiowps_lockdown_allowed_ip_addresses\";s:0:\"\";s:26:\"aiowps_enable_whitelisting\";s:0:\"\";s:27:\"aiowps_allowed_ip_addresses\";s:0:\"\";s:27:\"aiowps_enable_login_captcha\";s:1:\"1\";s:34:\"aiowps_enable_custom_login_captcha\";s:1:\"1\";s:25:\"aiowps_captcha_secret_key\";s:20:\"dqljtqiyc607l81xh3q5\";s:42:\"aiowps_enable_manual_registration_approval\";s:1:\"1\";s:39:\"aiowps_enable_registration_page_captcha\";s:1:\"1\";s:35:\"aiowps_enable_registration_honeypot\";s:0:\"\";s:27:\"aiowps_enable_random_prefix\";s:0:\"\";s:31:\"aiowps_enable_automated_backups\";s:0:\"\";s:26:\"aiowps_db_backup_frequency\";s:1:\"4\";s:25:\"aiowps_db_backup_interval\";s:1:\"2\";s:26:\"aiowps_backup_files_stored\";s:1:\"2\";s:32:\"aiowps_send_backup_email_address\";s:0:\"\";s:27:\"aiowps_backup_email_address\";s:26:\"joesayegh@square-solve.com\";s:27:\"aiowps_disable_file_editing\";s:0:\"\";s:37:\"aiowps_prevent_default_wp_file_access\";s:1:\"1\";s:22:\"aiowps_system_log_file\";s:9:\"error_log\";s:26:\"aiowps_enable_blacklisting\";s:0:\"\";s:26:\"aiowps_banned_ip_addresses\";s:0:\"\";s:28:\"aiowps_enable_basic_firewall\";s:1:\"1\";s:31:\"aiowps_enable_pingback_firewall\";s:0:\"\";s:38:\"aiowps_disable_xmlrpc_pingback_methods\";s:0:\"\";s:34:\"aiowps_block_debug_log_file_access\";s:1:\"1\";s:26:\"aiowps_disable_index_views\";s:1:\"1\";s:30:\"aiowps_disable_trace_and_track\";s:0:\"\";s:28:\"aiowps_forbid_proxy_comments\";s:0:\"\";s:29:\"aiowps_deny_bad_query_strings\";s:1:\"1\";s:34:\"aiowps_advanced_char_string_filter\";s:0:\"\";s:25:\"aiowps_enable_5g_firewall\";s:0:\"\";s:25:\"aiowps_enable_6g_firewall\";s:0:\"\";s:26:\"aiowps_enable_custom_rules\";s:0:\"\";s:19:\"aiowps_custom_rules\";s:0:\"\";s:25:\"aiowps_enable_404_logging\";s:0:\"\";s:28:\"aiowps_enable_404_IP_lockout\";s:0:\"\";s:30:\"aiowps_404_lockout_time_length\";s:2:\"60\";s:28:\"aiowps_404_lock_redirect_url\";s:16:\"http://127.0.0.1\";s:31:\"aiowps_enable_rename_login_page\";s:0:\"\";s:28:\"aiowps_enable_login_honeypot\";s:0:\"\";s:43:\"aiowps_enable_brute_force_attack_prevention\";s:0:\"\";s:30:\"aiowps_brute_force_secret_word\";s:0:\"\";s:24:\"aiowps_cookie_brute_test\";s:0:\"\";s:44:\"aiowps_cookie_based_brute_force_redirect_url\";s:16:\"http://127.0.0.1\";s:59:\"aiowps_brute_force_attack_prevention_pw_protected_exception\";s:0:\"\";s:51:\"aiowps_brute_force_attack_prevention_ajax_exception\";s:0:\"\";s:19:\"aiowps_site_lockout\";s:0:\"\";s:23:\"aiowps_site_lockout_msg\";s:0:\"\";s:30:\"aiowps_enable_spambot_blocking\";s:1:\"1\";s:29:\"aiowps_enable_comment_captcha\";s:1:\"1\";s:31:\"aiowps_enable_autoblock_spam_ip\";s:0:\"\";s:33:\"aiowps_spam_ip_min_comments_block\";s:0:\"\";s:32:\"aiowps_enable_automated_fcd_scan\";s:0:\"\";s:25:\"aiowps_fcd_scan_frequency\";s:1:\"4\";s:24:\"aiowps_fcd_scan_interval\";s:1:\"2\";s:28:\"aiowps_fcd_exclude_filetypes\";s:0:\"\";s:24:\"aiowps_fcd_exclude_files\";s:0:\"\";s:26:\"aiowps_send_fcd_scan_email\";s:0:\"\";s:29:\"aiowps_fcd_scan_email_address\";s:26:\"joesayegh@square-solve.com\";s:27:\"aiowps_fcds_change_detected\";b:0;s:22:\"aiowps_copy_protection\";s:0:\"\";s:40:\"aiowps_prevent_site_display_inside_frame\";s:0:\"\";s:32:\"aiowps_prevent_users_enumeration\";s:0:\"\";s:28:\"aiowps_block_fake_googlebots\";s:1:\"1\";s:35:\"aiowps_enable_lost_password_captcha\";s:1:\"1\";}', 'yes'),
(669, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1499245095;s:7:\"checked\";a:3:{s:34:\"advanced-custom-fields-pro/acf.php\";s:6:\"5.5.14\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:5:\"4.2.8\";s:37:\"disable-comments/disable-comments.php\";s:3:\"1.6\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:5:\"4.2.8\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";}s:37:\"disable-comments/disable-comments.php\";O:8:\"stdClass\":6:{s:2:\"id\";s:30:\"w.org/plugins/disable-comments\";s:4:\"slug\";s:16:\"disable-comments\";s:6:\"plugin\";s:37:\"disable-comments/disable-comments.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/disable-comments/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/disable-comments.zip\";}}}', 'no'),
(679, 'wp-optimize-enable-auto-backup', 'false', 'yes'),
(707, '_transient_timeout_aiowps_logout_payload', '1499171402', 'no'),
(708, '_transient_aiowps_logout_payload', 'a:2:{s:11:\"redirect_to\";s:60:\"http://localhost:8888/lloydnorthover/wp-admin/admin-ajax.php\";s:3:\"msg\";s:35:\"aiowps_login_msg_id=session_expired\";}', 'no'),
(736, '_site_transient_timeout_browser_2530788aa4dc77cd103c326a9d29b54b', '1499703529', 'no'),
(737, '_site_transient_browser_2530788aa4dc77cd103c326a9d29b54b', 'a:9:{s:8:\"platform\";s:9:\"Macintosh\";s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"57.0.2987.133\";s:10:\"update_url\";s:28:\"http://www.google.com/chrome\";s:7:\"img_src\";s:49:\"http://s.wordpress.org/images/browsers/chrome.png\";s:11:\"img_src_ssl\";s:48:\"https://wordpress.org/images/browsers/chrome.png\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;}', 'no'),
(743, '_transient_timeout_aiowps_captcha_string_info_znpadxsp27', '1499160461', 'no'),
(744, '_transient_aiowps_captcha_string_info_znpadxsp27', 'MTQ5OTE1ODY2MWRxbGp0cWl5YzYwN2w4MXhoM3E1Ng==', 'no'),
(745, '_transient_timeout_aiowps_captcha_string_info_6b9q9heo50', '1499160462', 'no'),
(746, '_transient_aiowps_captcha_string_info_6b9q9heo50', 'MTQ5OTE1ODY2MmRxbGp0cWl5YzYwN2w4MXhoM3E1NA==', 'no'),
(765, '_transient_timeout_aiowps_captcha_string_info_35igzrd4t1', '1499171418', 'no'),
(766, '_transient_aiowps_captcha_string_info_35igzrd4t1', 'MTQ5OTE2OTYxOGRxbGp0cWl5YzYwN2w4MXhoM3E1OA==', 'no'),
(774, '_transient_timeout_aiowps_captcha_string_info_waik7mw1pr', '1499179972', 'no'),
(775, '_transient_aiowps_captcha_string_info_waik7mw1pr', 'MTQ5OTE3ODE3MmRxbGp0cWl5YzYwN2w4MXhoM3E1MjQ=', 'no'),
(776, '_transient_timeout_aiowps_captcha_string_info_r4yi2szttd', '1499179976', 'no'),
(777, '_transient_aiowps_captcha_string_info_r4yi2szttd', 'MTQ5OTE3ODE3NmRxbGp0cWl5YzYwN2w4MXhoM3E1MTY=', 'no'),
(805, '_site_transient_timeout_theme_roots', '1499246217', 'no'),
(806, '_site_transient_theme_roots', 'a:1:{s:6:\"ln2017\";s:7:\"/themes\";}', 'no'),
(807, '_transient_timeout_aiowps_captcha_string_info_b0upk31hn0', '1499246233', 'no'),
(808, '_transient_aiowps_captcha_string_info_b0upk31hn0', 'MTQ5OTI0NDQzM2RxbGp0cWl5YzYwN2w4MXhoM3E1Ng==', 'no'),
(809, '_transient_timeout_aiowps_captcha_string_info_sce38hx2ii', '1499246251', 'no'),
(810, '_transient_aiowps_captcha_string_info_sce38hx2ii', 'MTQ5OTI0NDQ1MWRxbGp0cWl5YzYwN2w4MXhoM3E1OA==', 'no'),
(811, '_transient_timeout_users_online', '1499246385', 'no'),
(812, '_transient_users_online', 'a:1:{i:0;a:3:{s:7:\"user_id\";i:1;s:13:\"last_activity\";d:1499248185;s:10:\"ip_address\";s:3:\"::1\";}}', 'no'),
(813, '_transient_is_multi_author', '0', 'yes'),
(816, '_transient_timeout_acf_plugin_info_pro', '1499248721', 'no');
INSERT INTO `ln2017_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(817, '_transient_acf_plugin_info_pro', 'a:16:{s:4:\"name\";s:26:\"Advanced Custom Fields PRO\";s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:8:\"homepage\";s:37:\"https://www.advancedcustomfields.com/\";s:7:\"version\";s:6:\"5.5.14\";s:6:\"author\";s:13:\"Elliot Condon\";s:10:\"author_url\";s:28:\"http://www.elliotcondon.com/\";s:12:\"contributors\";s:12:\"elliotcondon\";s:8:\"requires\";s:5:\"3.6.0\";s:6:\"tested\";s:5:\"4.8.0\";s:4:\"tags\";a:62:{i:0;s:11:\"5.6.0-beta2\";i:1;s:11:\"5.6.0-beta1\";i:2;s:5:\"5.5.9\";i:3;s:5:\"5.5.7\";i:4;s:5:\"5.5.5\";i:5;s:5:\"5.5.3\";i:6;s:5:\"5.5.2\";i:7;s:6:\"5.5.13\";i:8;s:6:\"5.5.12\";i:9;s:6:\"5.5.11\";i:10;s:6:\"5.5.10\";i:11;s:5:\"5.5.1\";i:12;s:5:\"5.5.0\";i:13;s:5:\"5.4.8\";i:14;s:5:\"5.4.7\";i:15;s:5:\"5.4.6\";i:16;s:5:\"5.4.5\";i:17;s:5:\"5.4.4\";i:18;s:5:\"5.4.3\";i:19;s:5:\"5.4.2\";i:20;s:5:\"5.4.1\";i:21;s:5:\"5.4.0\";i:22;s:5:\"5.3.8\";i:23;s:5:\"5.3.7\";i:24;s:5:\"5.3.6\";i:25;s:5:\"5.3.5\";i:26;s:5:\"5.3.4\";i:27;s:5:\"5.3.3\";i:28;s:5:\"5.3.2\";i:29;s:6:\"5.3.10\";i:30;s:5:\"5.3.1\";i:31;s:5:\"5.3.0\";i:32;s:5:\"5.2.9\";i:33;s:5:\"5.2.8\";i:34;s:5:\"5.2.7\";i:35;s:5:\"5.2.6\";i:36;s:5:\"5.2.5\";i:37;s:5:\"5.2.4\";i:38;s:5:\"5.2.3\";i:39;s:5:\"5.2.2\";i:40;s:5:\"5.2.1\";i:41;s:5:\"5.2.0\";i:42;s:5:\"5.1.9\";i:43;s:5:\"5.1.8\";i:44;s:5:\"5.1.7\";i:45;s:5:\"5.1.6\";i:46;s:5:\"5.1.5\";i:47;s:5:\"5.1.4\";i:48;s:5:\"5.1.3\";i:49;s:5:\"5.1.2\";i:50;s:5:\"5.1.1\";i:51;s:5:\"5.1.0\";i:52;s:5:\"5.0.9\";i:53;s:5:\"5.0.8\";i:54;s:5:\"5.0.7\";i:55;s:5:\"5.0.6\";i:56;s:5:\"5.0.5\";i:57;s:5:\"5.0.4\";i:58;s:5:\"5.0.3\";i:59;s:5:\"5.0.2\";i:60;s:5:\"5.0.1\";i:61;s:5:\"5.0.0\";}s:6:\"tagged\";s:123:\"acf, advanced, custom, field, fields, custom field, custom fields, simple fields, magic fields, more fields, repeater, edit\";s:11:\"description\";s:4337:\"<p>Advanced Custom Fields is the perfect solution for any WordPress website which needs more flexible data like other Content Management Systems. </p>\n<ul><li>Visually create your Fields</li><li>Select from multiple input types (text, textarea, wysiwyg, image, file, page link, post object, relationship, select, checkbox, radio buttons, date picker, true / false, repeater, flexible content, gallery and more to come!)</li><li>Assign your fields to multiple edit pages (via custom location rules)</li><li>Easily load data through a simple and friendly API</li><li>Uses the native WordPress custom post type for ease of use and fast processing</li><li>Uses the native WordPress metadata for ease of use and fast processing</li></ul>\n<h4> Field Types </h4>\n<ul><li>Text (type text, api returns text)</li><li>Text Area (type text, api returns text)</li><li>Number (type number, api returns integer)</li><li>Email (type email, api returns text)</li><li>Password (type password, api returns text)</li><li>WYSIWYG (a WordPress wysiwyg editor, api returns html)</li><li>Image (upload an image, api returns the url)</li><li>File (upload a file, api returns the url)</li><li>Select (drop down list of choices, api returns chosen item)</li><li>Checkbox (tickbox list of choices, api returns array of choices)</li><li>Radio Buttons ( radio button list of choices, api returns chosen item)</li><li>True / False (tick box with message, api returns true or false)</li><li>Page Link (select 1 or more page, post or custom post types, api returns the selected url)</li><li>Post Object (select 1 or more page, post or custom post types, api returns the selected post objects)</li><li>Relationship (search, select and order post objects with a tidy interface, api returns the selected post objects)</li><li>Taxonomy (select taxonomy terms with options to load, display and save, api returns the selected term objects)</li><li>User (select 1 or more WP users, api returns the selected user objects)</li><li>Google Maps (interactive map, api returns lat,lng,address data)</li><li>Date Picker (jquery date picker, options for format, api returns string)</li><li>Color Picker (WP color swatch picker)</li><li>Tab (Group fields into tabs)</li><li>Message (Render custom messages into the fields)</li><li>Repeater (ability to create repeatable blocks of fields!)</li><li>Flexible Content (ability to create flexible blocks of fields!)</li><li>Gallery (Add, edit and order multiple images in 1 simple field)</li><li>[Custom](<a href=\"https://www.advancedcustomfields.com/resources/tutorials/creating-a-new-field-type/)\">www.advancedcustomfields.com/resources/tutorials/creating-a-new-field-type/)</a> (Create your own field type!)</li></ul>\n<h4> Tested on </h4>\n<ul><li>Mac Firefox 	:)</li><li>Mac Safari 	:)</li><li>Mac Chrome	:)</li><li>PC Safari 	:)</li><li>PC Chrome		:)</li><li>PC Firefox	:)</li><li>iPhone Safari :)</li><li>iPad Safari 	:)</li><li>PC ie7		:S</li></ul>\n<h4> Website </h4>\n<p><a href=\"https://www.advancedcustomfields.com/\">www.advancedcustomfields.com/</a></p>\n<h4> Documentation </h4>\n<ul><li>[Getting Started](<a href=\"https://www.advancedcustomfields.com/resources/#getting-started)\">www.advancedcustomfields.com/resources/#getting-started)</a></li><li>[Field Types](<a href=\"https://www.advancedcustomfields.com/resources/#field-types)\">www.advancedcustomfields.com/resources/#field-types)</a></li><li>[Functions](<a href=\"https://www.advancedcustomfields.com/resources/#functions)\">www.advancedcustomfields.com/resources/#functions)</a></li><li>[Actions](<a href=\"https://www.advancedcustomfields.com/resources/#actions)\">www.advancedcustomfields.com/resources/#actions)</a></li><li>[Filters](<a href=\"https://www.advancedcustomfields.com/resources/#filters)\">www.advancedcustomfields.com/resources/#filters)</a></li><li>[How to guides](<a href=\"https://www.advancedcustomfields.com/resources/#how-to)\">www.advancedcustomfields.com/resources/#how-to)</a></li><li>[Tutorials](<a href=\"https://www.advancedcustomfields.com/resources/#tutorials)\">www.advancedcustomfields.com/resources/#tutorials)</a></li></ul>\n<h4> Bug Submission and Forum Support </h4>\n<p><a href=\"http://support.advancedcustomfields.com/\">support.advancedcustomfields.com/</a></p>\n<h4> Please Vote and Enjoy </h4>\n<p>Your votes really make a difference! Thanks.</p>\n\";s:12:\"installation\";s:467:\"<ol><li>Upload <code>advanced-custom-fields</code> to the <code>/wp-content/plugins/</code> directory</li><li>Activate the plugin through the <code>Plugins</code> menu in WordPress</li><li>Click on the new menu item \"Custom Fields\" and create your first Custom Field Group!</li><li>Your custom field group will now appear on the page / post / template you specified in the field group\'s location rules!</li><li>Read the documentation to display your data: </li></ol>\n\";s:9:\"changelog\";s:4168:\"<h4> 5.5.14 </h4>\n<ul><li>Core: Minor bug fixes</li></ul>\n<h4> 5.5.13 </h4>\n<ul><li>Clone field: Improved <code>Fields</code> setting to show all fields within a matching field group search</li><li>Flexible Content field: Fixed bug causing <code>layout_title</code> filter to fail when field is cloned</li><li>Flexible Content field: Added missing <code>translate_field</code> function</li><li>WYSIWYG field: Fixed JS error when using CKEditor plugin</li><li>Date Picker field: Improved <code>Display Format</code> and <code>Return Format</code> settings UI</li><li>Time Picker field: Same as above</li><li>Datetime Picker field: Same as above</li><li>Core: Added new <code>remove_wp_meta_box</code> setting</li><li>Core: Added constants ACF, ACF_PRO, ACF_VERSION and ACF_PATH</li><li>Core: Improved compatibility with Select2 v4 including sortable functionality</li><li>Language: Updated Portuguese translation - thanks to Pedro Mendonça</li></ul>\n<h4> 5.5.12 </h4>\n<ul><li>Tab field: Allowed HTML within field label to show in tab</li><li>Core: Improved plugin update class</li><li>Language: Updated Portuguese translation - thanks to Pedro Mendonça</li><li>Language: Updated Brazilian Portuguese translation - thanks to Rafael Ribeiro</li></ul>\n<h4> 5.5.11 </h4>\n<ul><li>Google Map field: Added new <code>google_map_init</code> JS action</li><li>Core: Minor fixes and improvements</li><li>Language: Updated Swiss German translation - thanks to Raphael Hüni</li><li>Language: Updated French translation - thanks to Maxime Bernard-Jacquet</li></ul>\n<h4> 5.5.10 </h4>\n<ul><li>API: Added new functionality to the `acf_form()` function:</li><li>- added new <code>html_updated_message</code> setting</li><li>- added new <code>html_submit_button</code> setting</li><li>- added new <code>html_submit_spinner</code> setting</li><li>- added new <code>acf/pre_submit_form</code> filter run when form is successfully submit (before saving $_POST)</li><li>- added new <code>acf/submit_form</code> action run when form is successfully submit (after saving $_POST)</li><li>- added new <code>%post_id%</code> replace string to the <code>return</code> setting</li><li>- added new encryption logic to prevent $_POST exploits</li><li>- added new `acf_register_form()` function</li><li>Core: Fixed bug preventing values being loaded on a new post/page preview</li><li>Core: Fixed missing <code>Bulk Actions</code> dropdown on sync screen when no field groups exist</li><li>Core: Fixed bug ignoring PHP field groups if exists in JSON</li><li>Core: Minor fixes and improvements</li></ul>\n<h4> 5.5.9 </h4>\n<ul><li>Core: Fixed bug causing ACF4 PHP field groups to be ignored if missing ‘key’ setting</li></ul>\n<h4> 5.5.8 </h4>\n<ul><li>Flexible Content: Added logic to better <code>clean up</code> data when re-ordering layouts</li><li>oEmbed field: Fixed bug causing incorrect width and height settings in embed HTML</li><li>Core: Fixed bug causing incorrect Select2 CSS version loading for WooCommerce 2.7</li><li>Core: Fixed bug preventing <code>min-height</code> style being applied to floating width fields</li><li>Core: Added new JS <code>init</code> actions for wysiwyg, date, datetime, time and select2 fields</li><li>Core: Minor fixes and improvements</li></ul>\n<h4> 5.5.7 </h4>\n<ul><li>Core: Fixed bug causing `get_field()` to return incorrect data for sub fields registered via PHP code.</li></ul>\n<h4> 5.5.6 </h4>\n<ul><li>Core: Fixed bug causing license key to be ignored after changing url from http to https</li><li>Core: Fixed Select2 (v4) bug where <code>allow null</code> setting would not correctly save empty value</li><li>Core: Added new <code>acf/validate_field</code> filter</li><li>Core: Added new <code>acf/validate_field_group</code> filter</li><li>Core: Added new <code>acf/validate_post_id</code> filter</li><li>Core: Added new <code>row_index_offset</code> setting</li><li>Core: Fixed bug causing value loading issues for a taxonomy term in WP < 4.4</li><li>Core: Minor fixes and improvements</li></ul>\n<h4> 5.5.5 </h4>\n<ul><li>File field: Fixed bug creating draft post when saving an empty value</li><li>Image field: Fixed bug mentioned above</li></ul>\n\";s:14:\"upgrade_notice\";s:551:\"<h4> 5.2.7 </h4>\n<ul><li>Field class names have changed slightly in v5.2.7 from `field_type-{$type}` to `acf-field-{$type}`. This change was introduced to better optimise JS performance. The previous class names can be added back in with the following filter: <a href=\"https://www.advancedcustomfields.com/resources/acfcompatibility/\">www.advancedcustomfields.com/resources/acfcompatibility/</a></li></ul>\n<h4> 3.0.0 </h4>\n<ul><li>Editor is broken in WordPress 3.3</li></ul>\n<h4> 2.1.4 </h4>\n<ul><li>Adds post_id column back into acf_values</li></ul>\n\";s:7:\"banners\";a:2:{s:3:\"low\";s:65:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.png\";s:4:\"high\";s:66:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.png\";}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_postmeta`
--

CREATE TABLE `ln2017_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ;

--
-- Dumping data for table `ln2017_postmeta`
--

INSERT INTO `ln2017_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(8, 2, '_edit_last', '1'),
(9, 2, '_edit_lock', '1499185599:1'),
(14, 10, '_edit_last', '1'),
(15, 10, '_edit_lock', '1499094854:1'),
(16, 2, 'content', 'a:7:{i:0;s:22:\"text_block_two_columns\";i:1;s:12:\"slick_slider\";i:2;s:16:\"case_study_block\";i:3;s:10:\"team_block\";i:4;s:11:\"offer_block\";i:5;s:22:\"office_locations_block\";i:6;s:22:\"text_block_two_columns\";}'),
(17, 2, '_content', 'field_59479e957e9d0'),
(2216, 2, 'content_1_section_id', 'slider'),
(2217, 2, '_content_1_section_id', 'field_594a4577c96d7'),
(2218, 2, 'content_1_slider_type', 'lazy-single-item'),
(2219, 2, '_content_1_slider_type', 'field_594a45a4c96d8'),
(2220, 2, 'content_1_slider_width', 'widescreen'),
(2221, 2, '_content_1_slider_width', 'field_594a45d8c96d9'),
(2222, 2, 'content_1_slides', 'a:3:{i:0;s:3:\"251\";i:1;s:3:\"252\";i:2;s:3:\"253\";}'),
(2223, 2, '_content_1_slides', 'field_594a4739c96da'),
(2224, 2, 'content_2_section_id', 'case-study'),
(2225, 2, '_content_2_section_id', 'field_594a81ce79f5d'),
(2226, 2, 'content_2_padding_top', '12'),
(2227, 2, '_content_2_padding_top', 'field_594bbc21066ba'),
(2228, 2, 'content_2_padding_bottom', ''),
(2229, 2, '_content_2_padding_bottom', 'field_594bbc31066bb'),
(2230, 2, 'content_2_case_study_info_0_case_study_image', '254'),
(2231, 2, '_content_2_case_study_info_0_case_study_image', 'field_594a81fd79f5f'),
(2232, 2, 'content_2_case_study_info_0_case_study_id', 'tuc'),
(2233, 2, '_content_2_case_study_info_0_case_study_id', 'field_594a826c79f60'),
(2234, 2, 'content_2_case_study_info_0_case_study_title', 'TUC'),
(2235, 2, '_content_2_case_study_info_0_case_study_title', 'field_594a82e479f61'),
(2236, 2, 'content_2_case_study_info_0_case_study_subtitle', 'Expressing the modern context for a historic legend'),
(2237, 2, '_content_2_case_study_info_0_case_study_subtitle', 'field_594a834479f62'),
(2238, 2, 'content_2_case_study_info_0_case_study_description', 'The TUC (Trades Union Congress) is an iconic institution with a historic impact on progressing workers rights that few others can match. However, it is either over associated with its past or even not recognised at all, particularly among younger audiences, for its role and relevance in a modern society that features an increasingly insecure working environment. \r\n\r\nWhile our previous identity stood the test of time for over 20 years, it was time to review and update. Our qualitative and quantitative research identified several themes and homed in on the power of people joining together to make a positive difference. So we updated the passive ‘Britain at work’ brand line with our new proactive ‘changing the world of work for good’ expression, reflecting the TUCs modern context. A fresh new design identity strikes the right balance between a more contemporary style for a new generation and the gravitas and weight that this historic legend deserves.'),
(2239, 2, '_content_2_case_study_info_0_case_study_description', 'field_594a837e79f63'),
(2240, 2, 'content_2_case_study_info_0_case_study_what_we_did', '<ul class=\"brand-offers\">\r\n 	<li class=\"brand_underline\">Brand Strategy</li>\r\n 	<li class=\"brand_underline\">Brand Design</li>\r\n 	<li class=\"brand_underline\">Brand Communications</li>\r\n 	<li class=\"brand_underline\">Creative Direction</li>\r\n 	<li class=\"brand_underline\">Brand Environment</li>\r\n 	<li class=\"brand_underline\">Campaign</li>\r\n</ul>'),
(2241, 2, '_content_2_case_study_info_0_case_study_what_we_did', 'field_594a838e79f64'),
(2242, 2, 'content_2_case_study_info_0_case_study_body_image', '149'),
(2243, 2, '_content_2_case_study_info_0_case_study_body_image', 'field_594a83dd79f65'),
(2244, 2, 'content_2_case_study_info_1_case_study_image', '255'),
(2245, 2, '_content_2_case_study_info_1_case_study_image', 'field_594a81fd79f5f'),
(2246, 2, 'content_2_case_study_info_1_case_study_id', 'boi'),
(2247, 2, '_content_2_case_study_info_1_case_study_id', 'field_594a826c79f60'),
(2248, 2, 'content_2_case_study_info_1_case_study_title', 'Bank of Ireland'),
(2249, 2, '_content_2_case_study_info_1_case_study_title', 'field_594a82e479f61'),
(2250, 2, 'content_2_case_study_info_1_case_study_subtitle', 'Powering culture change through partnership'),
(2251, 2, '_content_2_case_study_info_1_case_study_subtitle', 'field_594a834479f62'),
(2252, 2, 'content_2_case_study_info_1_case_study_description', 'Bank of Ireland UK has been going from strength to strength, serving a diverse range of audiences from mortgage intermediaries to SMEs to retail bank consumers in N.Ireland. They are the strategic partner to the Post Office and The AA, providing a full suite of their financial services.\r\n\r\nWorking as a true partner with their customers in far more breadth and depth than any other bank, they needed to bring their story to life and embed their ideal values and behaviors into their organisation. We help them drive that culture change across the thousands of staff. Following stakeholder research, we created a new ‘elevator pitch’ around ‘Living Partnership’ and a visual design system to communicate the brand. Our ongoing support covers launch events, video, literature, digital and complete office environment visual design.'),
(2253, 2, '_content_2_case_study_info_1_case_study_description', 'field_594a837e79f63'),
(2254, 2, 'content_2_case_study_info_1_case_study_what_we_did', '<ul class=\"brand-offers\">\r\n 	<li class=\"brand_underline\">Brand Strategy</li>\r\n 	<li class=\"brand_underline\">Brand Design</li>\r\n 	<li class=\"brand_underline\">Brand Communications</li>\r\n 	<li class=\"brand_underline\">Creative Direction</li>\r\n 	<li class=\"brand_underline\">Brand Environment</li>\r\n 	<li class=\"brand_underline\">Campaign</li>\r\n</ul>'),
(2255, 2, '_content_2_case_study_info_1_case_study_what_we_did', 'field_594a838e79f64'),
(2256, 2, 'content_2_case_study_info_1_case_study_body_image', '135'),
(2257, 2, '_content_2_case_study_info_1_case_study_body_image', 'field_594a83dd79f65'),
(2258, 2, 'content_2_case_study_info_2_case_study_image', '256'),
(2259, 2, '_content_2_case_study_info_2_case_study_image', 'field_594a81fd79f5f'),
(2260, 2, 'content_2_case_study_info_2_case_study_id', 'informa'),
(2261, 2, '_content_2_case_study_info_2_case_study_id', 'field_594a826c79f60'),
(2262, 2, 'content_2_case_study_info_2_case_study_title', 'Informa'),
(2263, 2, '_content_2_case_study_info_2_case_study_title', 'field_594a82e479f61'),
(2264, 2, 'content_2_case_study_info_2_case_study_subtitle', 'An intelligent approach to business intelligence'),
(2265, 2, '_content_2_case_study_info_2_case_study_subtitle', 'field_594a834479f62'),
(2266, 2, 'content_2_case_study_info_2_case_study_description', 'Informa Business Intelligence, a world leading knowledge and information business, looked to us to advise on a clear brand strategy and new architecture for its diverse portfolio of brands.\r\n\r\nWe engaged with stakeholders across all corners of the globe to redefine the business to better leverage its unrivalled expertise.\r\n\r\nThe new brand, visual language and messaging all centre around ‘competitive advantage’, reinforcing the strength and depth of the offer and allowing Business Intelligence to take advantage in the market.'),
(2267, 2, '_content_2_case_study_info_2_case_study_description', 'field_594a837e79f63'),
(2268, 2, 'content_2_case_study_info_2_case_study_what_we_did', '<ul class=\"brand-offers\">\r\n 	<li class=\"brand_underline\">Brand Strategy</li>\r\n 	<li class=\"brand_underline\">Brand Design</li>\r\n 	<li class=\"brand_underline\">Brand Communications</li>\r\n 	<li class=\"brand_underline\">Creative Direction</li>\r\n 	<li class=\"brand_underline\">Brand Environment</li>\r\n 	<li class=\"brand_underline\">Campaign</li>\r\n</ul>'),
(2269, 2, '_content_2_case_study_info_2_case_study_what_we_did', 'field_594a838e79f64'),
(2270, 2, 'content_2_case_study_info_2_case_study_body_image', '136'),
(2271, 2, '_content_2_case_study_info_2_case_study_body_image', 'field_594a83dd79f65'),
(2272, 2, 'content_2_case_study_info_3_case_study_image', '257'),
(2273, 2, '_content_2_case_study_info_3_case_study_image', 'field_594a81fd79f5f'),
(2274, 2, 'content_2_case_study_info_3_case_study_id', 'warwick'),
(2275, 2, '_content_2_case_study_info_3_case_study_id', 'field_594a826c79f60'),
(2276, 2, 'content_2_case_study_info_3_case_study_title', 'Warwick Business School'),
(2277, 2, '_content_2_case_study_info_3_case_study_title', 'field_594a82e479f61'),
(2278, 2, 'content_2_case_study_info_3_case_study_subtitle', 'Unlocking a powerful brand hiding within'),
(2279, 2, '_content_2_case_study_info_3_case_study_subtitle', 'field_594a834479f62'),
(2280, 2, 'content_2_case_study_info_3_case_study_description', 'While recognised as one of the leading European business schools, WBS had no shared understanding or clear expression of its brand. Embracing our research and strategic approach, the critical academic group supported us in unlocking their real brand within. \r\n\r\nBuilding on their most distinctive attribute - a more entrepreneurial mindset – we developed a new positioning and long term brand line ‘For the Change Makers’. Accompanied by a new visual design identity to extend their existing core logo, we created a new design language, transforming their brand into a transparent cube, a visual puzzle, playing to their values of intellectual curiosity and restlessness. Constantly changing, each facet opens up a new chapter of their ‘Change Maker’ story.'),
(2281, 2, '_content_2_case_study_info_3_case_study_description', 'field_594a837e79f63'),
(2282, 2, 'content_2_case_study_info_3_case_study_what_we_did', '<ul class=\"brand-offers\">\r\n 	<li class=\"brand_underline\">Brand Strategy</li>\r\n 	<li class=\"brand_underline\">Brand Design</li>\r\n 	<li class=\"brand_underline\">Brand Communications</li>\r\n 	<li class=\"brand_underline\">Creative Direction</li>\r\n 	<li class=\"brand_underline\">Brand Environment</li>\r\n 	<li class=\"brand_underline\">Campaign</li>\r\n</ul>'),
(2283, 2, '_content_2_case_study_info_3_case_study_what_we_did', 'field_594a838e79f64'),
(2284, 2, 'content_2_case_study_info_3_case_study_body_image', '137'),
(2285, 2, '_content_2_case_study_info_3_case_study_body_image', 'field_594a83dd79f65'),
(2286, 2, 'content_2_case_study_info_4_case_study_image', '258'),
(2287, 2, '_content_2_case_study_info_4_case_study_image', 'field_594a81fd79f5f'),
(2288, 2, 'content_2_case_study_info_4_case_study_id', 'manchester'),
(2289, 2, '_content_2_case_study_info_4_case_study_id', 'field_594a826c79f60'),
(2290, 2, 'content_2_case_study_info_4_case_study_title', 'Manchester Business School'),
(2291, 2, '_content_2_case_study_info_4_case_study_title', 'field_594a82e479f61'),
(2292, 2, 'content_2_case_study_info_4_case_study_subtitle', 'Uniting a pioneering past and energetic future'),
(2293, 2, '_content_2_case_study_info_4_case_study_subtitle', 'field_594a834479f62'),
(2294, 2, 'content_2_case_study_info_4_case_study_description', 'Creating a unifying brand for the merger of two of the UK’s most distinguished universities to become one of the largest and most respected international seats of learning.\r\n\r\nThe brand references the University’s pioneering past, while the visual language embraces its exciting future. Thoughtful brand architecture and a flexible system speak to a diverse range of internal and external audiences.\r\n\r\nWinning the CASE Circle of Excellence Award, the Times Higher ‘University of the Year’ and becoming students most popular choice was the icing on the cake.'),
(2295, 2, '_content_2_case_study_info_4_case_study_description', 'field_594a837e79f63'),
(2296, 2, 'content_2_case_study_info_4_case_study_what_we_did', '<ul class=\"brand-offers\">\r\n 	<li class=\"brand_underline\">Brand Strategy</li>\r\n 	<li class=\"brand_underline\">Brand Design</li>\r\n 	<li class=\"brand_underline\">Brand Communications</li>\r\n 	<li class=\"brand_underline\">Creative Direction</li>\r\n 	<li class=\"brand_underline\">Brand Environment</li>\r\n 	<li class=\"brand_underline\">Campaign</li>\r\n</ul>'),
(2297, 2, '_content_2_case_study_info_4_case_study_what_we_did', 'field_594a838e79f64'),
(2298, 2, 'content_2_case_study_info_4_case_study_body_image', '134'),
(2299, 2, '_content_2_case_study_info_4_case_study_body_image', 'field_594a83dd79f65'),
(2300, 2, 'content_2_case_study_info_5_case_study_image', '259'),
(2301, 2, '_content_2_case_study_info_5_case_study_image', 'field_594a81fd79f5f'),
(2302, 2, 'content_2_case_study_info_5_case_study_id', 'wesleyan'),
(2303, 2, '_content_2_case_study_info_5_case_study_id', 'field_594a826c79f60'),
(2304, 2, 'content_2_case_study_info_5_case_study_title', 'Wesleyan'),
(2305, 2, '_content_2_case_study_info_5_case_study_title', 'field_594a82e479f61'),
(2306, 2, 'content_2_case_study_info_5_case_study_subtitle', 'The feeling is mutual'),
(2307, 2, '_content_2_case_study_info_5_case_study_subtitle', 'field_594a834479f62'),
(2308, 2, 'content_2_case_study_info_5_case_study_description', 'Wesleyan is a specialist mutual society, looking after the very specific needs of doctors, dentists, lawyers and teachers for over 170 years. Special because they are specialist.\r\n\r\nCreating a brand that tells this unique story and frames the special relationship between client and adviser. Told through a distinct visual language and confident messaging that heroes the ‘We’ in Wesleyan.'),
(2309, 2, '_content_2_case_study_info_5_case_study_description', 'field_594a837e79f63'),
(2310, 2, 'content_2_case_study_info_5_case_study_what_we_did', '<ul class=\"brand-offers\">\r\n 	<li class=\"brand_underline\">Brand Strategy</li>\r\n 	<li class=\"brand_underline\">Brand Design</li>\r\n 	<li class=\"brand_underline\">Brand Communications</li>\r\n 	<li class=\"brand_underline\">Creative Direction</li>\r\n 	<li class=\"brand_underline\">Brand Environment</li>\r\n 	<li class=\"brand_underline\">Campaign</li>\r\n</ul>'),
(2311, 2, '_content_2_case_study_info_5_case_study_what_we_did', 'field_594a838e79f64'),
(2312, 2, 'content_2_case_study_info_5_case_study_body_image', '137'),
(2313, 2, '_content_2_case_study_info_5_case_study_body_image', 'field_594a83dd79f65'),
(2314, 2, 'content_2_case_study_info', '6'),
(2315, 2, '_content_2_case_study_info', 'field_594a81e279f5e'),
(2374, 2, 'content_0_section_id', 'intro'),
(2375, 2, '_content_0_section_id', 'field_594bc09c4427e'),
(2376, 2, 'content_0_column_width', '50-50'),
(2377, 2, '_content_0_column_width', 'field_594bc09c4427f'),
(2378, 2, 'content_0_padding_top', '0'),
(2379, 2, '_content_0_padding_top', 'field_594bc09c44283'),
(2380, 2, 'content_0_padding_bottom', '10'),
(2381, 2, '_content_0_padding_bottom', 'field_594bc09c44284'),
(2382, 2, 'content_0_background_color', ''),
(2383, 2, '_content_0_background_color', 'field_594bc09c44280'),
(2384, 2, 'content_0_background_image', ''),
(2385, 2, '_content_0_background_image', 'field_594bc09c44281'),
(2386, 2, 'content_0_background_position', 'center center'),
(2387, 2, '_content_0_background_position', 'field_594bc09c44282'),
(2388, 2, 'content_0_left_column_text', '<div class=\"strap-line\"><span class=\"nobel-text\">making the complex simple</span>\r\n<span class=\"heavy-metal-text\">then the simple compelling</span></div>'),
(2389, 2, '_content_0_left_column_text', 'field_594bc09c44285'),
(2390, 2, 'content_0_right_column_text', '<div class=\"intro-text\">Since 1975 we\'ve created some of the world\'s best known, iconic and long lasting design identities. Today our leading edge brand research, consulting and design equips our clients with more insightful brand intelligence and more engaging creative content.</div>'),
(2391, 2, '_content_0_right_column_text', 'field_594bc09c44286'),
(2596, 2, 'content_3_section_id', 'people'),
(2597, 2, '_content_3_section_id', 'field_594bc8bbec634'),
(2598, 2, 'content_3_padding_top', ''),
(2599, 2, '_content_3_padding_top', 'field_594bc8bbec635'),
(2600, 2, 'content_3_padding_bottom', ''),
(2601, 2, '_content_3_padding_bottom', 'field_594bc8bbec636'),
(2602, 2, 'content_3_team_member_content_0_team_member_image', '263'),
(2603, 2, '_content_3_team_member_content_0_team_member_image', 'field_594bc8bbec637'),
(2604, 2, 'content_3_team_member_content_0_team_member_info', '<div class=\"heavy-metal-text\">Jane Doe</div>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.'),
(2605, 2, '_content_3_team_member_content_0_team_member_info', 'field_594bc8bbec638'),
(2606, 2, 'content_3_team_member_content_1_team_member_image', '262'),
(2607, 2, '_content_3_team_member_content_1_team_member_image', 'field_594bc8bbec637'),
(2608, 2, 'content_3_team_member_content_1_team_member_info', '<div class=\"heavy-metal-text\">John Doe</div>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.'),
(2609, 2, '_content_3_team_member_content_1_team_member_info', 'field_594bc8bbec638'),
(2610, 2, 'content_3_team_member_content_2_team_member_image', '261'),
(2611, 2, '_content_3_team_member_content_2_team_member_image', 'field_594bc8bbec637'),
(2612, 2, 'content_3_team_member_content_2_team_member_info', '<div class=\"heavy-metal-text\">Bob Smith</div>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.'),
(2613, 2, '_content_3_team_member_content_2_team_member_info', 'field_594bc8bbec638'),
(2614, 2, 'content_3_team_member_content_3_team_member_image', '260'),
(2615, 2, '_content_3_team_member_content_3_team_member_image', 'field_594bc8bbec637'),
(2616, 2, 'content_3_team_member_content_3_team_member_info', '<div class=\"heavy-metal-text\">Dave Jones</div>\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.'),
(2617, 2, '_content_3_team_member_content_3_team_member_info', 'field_594bc8bbec638'),
(2618, 2, 'content_3_team_member_content', '4'),
(2619, 2, '_content_3_team_member_content', 'field_594bc914ec63f'),
(2692, 2, 'content_2_case_study_info_0_title_color', '#000000'),
(2693, 2, '_content_2_case_study_info_0_title_color', 'field_594d1a6db8302'),
(2694, 2, 'content_2_case_study_info_1_title_color', '#ffffff'),
(2695, 2, '_content_2_case_study_info_1_title_color', 'field_594d1a6db8302'),
(2696, 2, 'content_2_case_study_info_2_title_color', '#000000'),
(2697, 2, '_content_2_case_study_info_2_title_color', 'field_594d1a6db8302'),
(2698, 2, 'content_2_case_study_info_3_title_color', '#000000'),
(2699, 2, '_content_2_case_study_info_3_title_color', 'field_594d1a6db8302'),
(2700, 2, 'content_2_case_study_info_4_title_color', '#ffffff'),
(2701, 2, '_content_2_case_study_info_4_title_color', 'field_594d1a6db8302'),
(2702, 2, 'content_2_case_study_info_5_title_color', '#000000'),
(2703, 2, '_content_2_case_study_info_5_title_color', 'field_594d1a6db8302'),
(2739, 2, 'content_2_case_study_info_0_case_study_content', 'a:6:{i:0;s:14:\"case_study_fwi\";i:1;s:14:\"case_study_hwi\";i:2;s:22:\"case_study_image_quote\";i:3;s:14:\"case_study_fwi\";i:4;s:14:\"case_study_hwi\";i:5;s:14:\"case_study_hwi\";}'),
(2740, 2, '_content_2_case_study_info_0_case_study_content', 'field_595a5c9476e73'),
(2741, 2, 'content_2_case_study_info_1_case_study_content', ''),
(2742, 2, '_content_2_case_study_info_1_case_study_content', 'field_595a5c9476e73'),
(2743, 2, 'content_2_case_study_info_2_case_study_content', ''),
(2744, 2, '_content_2_case_study_info_2_case_study_content', 'field_595a5c9476e73'),
(2745, 2, 'content_2_case_study_info_3_case_study_content', ''),
(2746, 2, '_content_2_case_study_info_3_case_study_content', 'field_595a5c9476e73'),
(2747, 2, 'content_2_case_study_info_4_case_study_content', ''),
(2748, 2, '_content_2_case_study_info_4_case_study_content', 'field_595a5c9476e73'),
(2749, 2, 'content_2_case_study_info_5_case_study_content', ''),
(2750, 2, '_content_2_case_study_info_5_case_study_content', 'field_595a5c9476e73'),
(2751, 2, 'content_2_case_study_info_0_case_study_fwi_one', ''),
(2752, 2, '_content_2_case_study_info_0_case_study_fwi_one', 'field_594a83dd79f65'),
(2753, 2, 'content_2_case_study_info_0_case_study_hwi_one', ''),
(2754, 2, '_content_2_case_study_info_0_case_study_hwi_one', 'field_594d364acd8da'),
(2755, 2, 'content_2_case_study_info_0_case_study_hwi_two', ''),
(2756, 2, '_content_2_case_study_info_0_case_study_hwi_two', 'field_5950ce730216e'),
(2757, 2, 'content_2_case_study_info_0_case_study_quote', 'Thank you so much for this, it was so good, I was delighted!'),
(2758, 2, '_content_2_case_study_info_0_case_study_quote', 'field_594d368bcd8db'),
(2759, 2, 'content_2_case_study_info_0_case_study_fwi_two', ''),
(2760, 2, '_content_2_case_study_info_0_case_study_fwi_two', 'field_5950ced802170'),
(2761, 2, 'content_2_case_study_info_0_case_study_hwi_four', ''),
(2762, 2, '_content_2_case_study_info_0_case_study_hwi_four', 'field_5950ce7d0216f'),
(2763, 2, 'content_2_case_study_info_0_case_study_hwi_five', ''),
(2764, 2, '_content_2_case_study_info_0_case_study_hwi_five', 'field_5950ceee02171'),
(2765, 2, 'content_2_case_study_info_0_case_study_hwi_six', ''),
(2766, 2, '_content_2_case_study_info_0_case_study_hwi_six', 'field_5950cef702172'),
(2767, 2, 'content_2_case_study_info_0_case_study_hwi_seven', ''),
(2768, 2, '_content_2_case_study_info_0_case_study_hwi_seven', 'field_5950cf0302173'),
(2769, 2, 'content_2_case_study_info_1_case_study_fwi_one', ''),
(2770, 2, '_content_2_case_study_info_1_case_study_fwi_one', 'field_594a83dd79f65'),
(2771, 2, 'content_2_case_study_info_1_case_study_hwi_one', ''),
(2772, 2, '_content_2_case_study_info_1_case_study_hwi_one', 'field_594d364acd8da'),
(2773, 2, 'content_2_case_study_info_1_case_study_hwi_two', ''),
(2774, 2, '_content_2_case_study_info_1_case_study_hwi_two', 'field_5950ce730216e'),
(2775, 2, 'content_2_case_study_info_1_case_study_quote', 'A big thank you for all the hard work you’ve collectively put in. It’s all coming together really well. Please do say thanks to the designers who’ve worked behind the scenes too.'),
(2776, 2, '_content_2_case_study_info_1_case_study_quote', 'field_594d368bcd8db'),
(2777, 2, 'content_2_case_study_info_1_case_study_fwi_two', ''),
(2778, 2, '_content_2_case_study_info_1_case_study_fwi_two', 'field_5950ced802170'),
(2779, 2, 'content_2_case_study_info_1_case_study_hwi_four', ''),
(2780, 2, '_content_2_case_study_info_1_case_study_hwi_four', 'field_5950ce7d0216f'),
(2781, 2, 'content_2_case_study_info_1_case_study_hwi_five', ''),
(2782, 2, '_content_2_case_study_info_1_case_study_hwi_five', 'field_5950ceee02171'),
(2783, 2, 'content_2_case_study_info_1_case_study_hwi_six', ''),
(2784, 2, '_content_2_case_study_info_1_case_study_hwi_six', 'field_5950cef702172'),
(2785, 2, 'content_2_case_study_info_1_case_study_hwi_seven', ''),
(2786, 2, '_content_2_case_study_info_1_case_study_hwi_seven', 'field_5950cf0302173'),
(2787, 2, 'content_2_case_study_info_2_case_study_fwi_one', ''),
(2788, 2, '_content_2_case_study_info_2_case_study_fwi_one', 'field_594a83dd79f65'),
(2789, 2, 'content_2_case_study_info_2_case_study_hwi_one', ''),
(2790, 2, '_content_2_case_study_info_2_case_study_hwi_one', 'field_594d364acd8da'),
(2791, 2, 'content_2_case_study_info_2_case_study_hwi_two', ''),
(2792, 2, '_content_2_case_study_info_2_case_study_hwi_two', 'field_5950ce730216e'),
(2793, 2, 'content_2_case_study_info_2_case_study_quote', 'Simon, Jeremy and their team at Lloyd Northover proved to be highly valued partners to help us address a complex brand architecture challenge with their stakeholder engagement, strategic analysis and very well crafted design identity system.'),
(2794, 2, '_content_2_case_study_info_2_case_study_quote', 'field_594d368bcd8db'),
(2795, 2, 'content_2_case_study_info_2_case_study_fwi_two', ''),
(2796, 2, '_content_2_case_study_info_2_case_study_fwi_two', 'field_5950ced802170'),
(2797, 2, 'content_2_case_study_info_2_case_study_hwi_four', ''),
(2798, 2, '_content_2_case_study_info_2_case_study_hwi_four', 'field_5950ce7d0216f'),
(2799, 2, 'content_2_case_study_info_2_case_study_hwi_five', ''),
(2800, 2, '_content_2_case_study_info_2_case_study_hwi_five', 'field_5950ceee02171'),
(2801, 2, 'content_2_case_study_info_2_case_study_hwi_six', ''),
(2802, 2, '_content_2_case_study_info_2_case_study_hwi_six', 'field_5950cef702172'),
(2803, 2, 'content_2_case_study_info_2_case_study_hwi_seven', ''),
(2804, 2, '_content_2_case_study_info_2_case_study_hwi_seven', 'field_5950cf0302173'),
(2805, 2, 'content_2_case_study_info_3_case_study_fwi_one', ''),
(2806, 2, '_content_2_case_study_info_3_case_study_fwi_one', 'field_594a83dd79f65'),
(2807, 2, 'content_2_case_study_info_3_case_study_hwi_one', ''),
(2808, 2, '_content_2_case_study_info_3_case_study_hwi_one', 'field_594d364acd8da'),
(2809, 2, 'content_2_case_study_info_3_case_study_hwi_two', ''),
(2810, 2, '_content_2_case_study_info_3_case_study_hwi_two', 'field_5950ce730216e'),
(2811, 2, 'content_2_case_study_info_3_case_study_quote', 'Jeremy, Simon and the team at Lloyd Northover are the ideal combination of brand strategy firepower and design craft at the highest level. Highly collaborative, they helped us unlock, navigate and create a significant step change in how we articulate our brand to the world.'),
(2812, 2, '_content_2_case_study_info_3_case_study_quote', 'field_594d368bcd8db'),
(2813, 2, 'content_2_case_study_info_3_case_study_fwi_two', ''),
(2814, 2, '_content_2_case_study_info_3_case_study_fwi_two', 'field_5950ced802170'),
(2815, 2, 'content_2_case_study_info_3_case_study_hwi_four', ''),
(2816, 2, '_content_2_case_study_info_3_case_study_hwi_four', 'field_5950ce7d0216f'),
(2817, 2, 'content_2_case_study_info_3_case_study_hwi_five', ''),
(2818, 2, '_content_2_case_study_info_3_case_study_hwi_five', 'field_5950ceee02171'),
(2819, 2, 'content_2_case_study_info_3_case_study_hwi_six', ''),
(2820, 2, '_content_2_case_study_info_3_case_study_hwi_six', 'field_5950cef702172'),
(2821, 2, 'content_2_case_study_info_3_case_study_hwi_seven', ''),
(2822, 2, '_content_2_case_study_info_3_case_study_hwi_seven', 'field_5950cf0302173'),
(2823, 2, 'content_2_case_study_info_4_case_study_fwi_one', ''),
(2824, 2, '_content_2_case_study_info_4_case_study_fwi_one', 'field_594a83dd79f65'),
(2825, 2, 'content_2_case_study_info_4_case_study_hwi_one', ''),
(2826, 2, '_content_2_case_study_info_4_case_study_hwi_one', 'field_594d364acd8da'),
(2827, 2, 'content_2_case_study_info_4_case_study_hwi_two', ''),
(2828, 2, '_content_2_case_study_info_4_case_study_hwi_two', 'field_5950ce730216e'),
(2829, 2, 'content_2_case_study_info_4_case_study_quote', ''),
(2830, 2, '_content_2_case_study_info_4_case_study_quote', 'field_594d368bcd8db'),
(2831, 2, 'content_2_case_study_info_4_case_study_fwi_two', ''),
(2832, 2, '_content_2_case_study_info_4_case_study_fwi_two', 'field_5950ced802170'),
(2833, 2, 'content_2_case_study_info_4_case_study_hwi_four', ''),
(2834, 2, '_content_2_case_study_info_4_case_study_hwi_four', 'field_5950ce7d0216f'),
(2835, 2, 'content_2_case_study_info_4_case_study_hwi_five', ''),
(2836, 2, '_content_2_case_study_info_4_case_study_hwi_five', 'field_5950ceee02171'),
(2837, 2, 'content_2_case_study_info_4_case_study_hwi_six', ''),
(2838, 2, '_content_2_case_study_info_4_case_study_hwi_six', 'field_5950cef702172'),
(2839, 2, 'content_2_case_study_info_4_case_study_hwi_seven', ''),
(2840, 2, '_content_2_case_study_info_4_case_study_hwi_seven', 'field_5950cf0302173'),
(2841, 2, 'content_2_case_study_info_5_case_study_fwi_one', ''),
(2842, 2, '_content_2_case_study_info_5_case_study_fwi_one', 'field_594a83dd79f65'),
(2843, 2, 'content_2_case_study_info_5_case_study_hwi_one', ''),
(2844, 2, '_content_2_case_study_info_5_case_study_hwi_one', 'field_594d364acd8da'),
(2845, 2, 'content_2_case_study_info_5_case_study_hwi_two', ''),
(2846, 2, '_content_2_case_study_info_5_case_study_hwi_two', 'field_5950ce730216e'),
(2847, 2, 'content_2_case_study_info_5_case_study_quote', ''),
(2848, 2, '_content_2_case_study_info_5_case_study_quote', 'field_594d368bcd8db'),
(2849, 2, 'content_2_case_study_info_5_case_study_fwi_two', ''),
(2850, 2, '_content_2_case_study_info_5_case_study_fwi_two', 'field_5950ced802170'),
(2851, 2, 'content_2_case_study_info_5_case_study_hwi_four', ''),
(2852, 2, '_content_2_case_study_info_5_case_study_hwi_four', 'field_5950ce7d0216f'),
(2853, 2, 'content_2_case_study_info_5_case_study_hwi_five', ''),
(2854, 2, '_content_2_case_study_info_5_case_study_hwi_five', 'field_5950ceee02171'),
(2855, 2, 'content_2_case_study_info_5_case_study_hwi_six', ''),
(2856, 2, '_content_2_case_study_info_5_case_study_hwi_six', 'field_5950cef702172'),
(2857, 2, 'content_2_case_study_info_5_case_study_hwi_seven', ''),
(2858, 2, '_content_2_case_study_info_5_case_study_hwi_seven', 'field_5950cf0302173'),
(2859, 2, 'content_2_case_study_info_0_case_study_hwi_three', ''),
(2860, 2, '_content_2_case_study_info_0_case_study_hwi_three', 'field_5950cfe2345de'),
(2861, 2, 'content_2_case_study_info_1_case_study_hwi_three', ''),
(2862, 2, '_content_2_case_study_info_1_case_study_hwi_three', 'field_5950cfe2345de'),
(2863, 2, 'content_2_case_study_info_2_case_study_hwi_three', ''),
(2864, 2, '_content_2_case_study_info_2_case_study_hwi_three', 'field_5950cfe2345de'),
(2865, 2, 'content_2_case_study_info_3_case_study_hwi_three', ''),
(2866, 2, '_content_2_case_study_info_3_case_study_hwi_three', 'field_5950cfe2345de'),
(2867, 2, 'content_2_case_study_info_4_case_study_hwi_three', ''),
(2868, 2, '_content_2_case_study_info_4_case_study_hwi_three', 'field_5950cfe2345de'),
(2869, 2, 'content_2_case_study_info_5_case_study_hwi_three', ''),
(2870, 2, '_content_2_case_study_info_5_case_study_hwi_three', 'field_5950cfe2345de'),
(2871, 2, 'content_2_case_study_info_0_case_study_quote_author', 'Antonia Bance, Head of Communications and Campaigns, TUC'),
(2872, 2, '_content_2_case_study_info_0_case_study_quote_author', 'field_5950d0fdd40d0'),
(2873, 2, 'content_2_case_study_info_1_case_study_quote_author', 'Jo Thomson, Head of Brand and Communications, Bank of Ireland UK'),
(2874, 2, '_content_2_case_study_info_1_case_study_quote_author', 'field_5950d0fdd40d0'),
(2875, 2, 'content_2_case_study_info_2_case_study_quote_author', 'Gary Nugent, Director of Sales and Marketing, Informa PLC'),
(2876, 2, '_content_2_case_study_info_2_case_study_quote_author', 'field_5950d0fdd40d0'),
(2877, 2, 'content_2_case_study_info_3_case_study_quote_author', 'Professor Andy Lockett, Dean of Warwick Business School'),
(2878, 2, '_content_2_case_study_info_3_case_study_quote_author', 'field_5950d0fdd40d0'),
(2879, 2, 'content_2_case_study_info_4_case_study_quote_author', ''),
(2880, 2, '_content_2_case_study_info_4_case_study_quote_author', 'field_5950d0fdd40d0'),
(2881, 2, 'content_2_case_study_info_5_case_study_quote_author', ''),
(2882, 2, '_content_2_case_study_info_5_case_study_quote_author', 'field_5950d0fdd40d0'),
(2934, 2, 'content_5_section_id', 'office-locations'),
(2935, 2, '_content_5_section_id', 'field_59522086e7425'),
(2936, 2, 'content_5_padding_top', ''),
(2937, 2, '_content_5_padding_top', 'field_59522086e7426'),
(2938, 2, 'content_5_padding_bottom', ''),
(2939, 2, '_content_5_padding_bottom', 'field_59522086e7427'),
(2940, 2, 'content_5_office_location_content_0_office_location_image', '282'),
(2941, 2, '_content_5_office_location_content_0_office_location_image', 'field_59522086e7428'),
(2942, 2, 'content_5_office_location_content_0_office_location_address_link', 'https://goo.gl/maps/34b777sgmEt'),
(2943, 2, '_content_5_office_location_content_0_office_location_address_link', 'field_59522086e7429'),
(2944, 2, 'content_5_office_location_content_0_office_location_address_display', '90 Tottenham Court Road\r\nLondon\r\nW1T 4TJ'),
(2945, 2, '_content_5_office_location_content_0_office_location_address_display', 'field_59522086e742a'),
(2946, 2, 'content_5_office_location_content_0_office_location_tel_link', '+442072918484'),
(2947, 2, '_content_5_office_location_content_0_office_location_tel_link', 'field_59522086e742b'),
(2948, 2, 'content_5_office_location_content_0_office_location_tel_display', '+44 (0)20 7291 8484'),
(2949, 2, '_content_5_office_location_content_0_office_location_tel_display', 'field_59522086e742c'),
(2950, 2, 'content_5_office_location_content_1_office_location_image', '283'),
(2951, 2, '_content_5_office_location_content_1_office_location_image', 'field_59522086e7428'),
(2952, 2, 'content_5_office_location_content_1_office_location_address_link', 'https://goo.gl/maps/KovD7azsYd42'),
(2953, 2, '_content_5_office_location_content_1_office_location_address_link', 'field_59522086e7429'),
(2954, 2, 'content_5_office_location_content_1_office_location_address_display', '71 Fifth Avenue, 8th Floor\r\nNew York\r\nNY 10003'),
(2955, 2, '_content_5_office_location_content_1_office_location_address_display', 'field_59522086e742a'),
(2956, 2, 'content_5_office_location_content_1_office_location_tel_link', '+12125083400'),
(2957, 2, '_content_5_office_location_content_1_office_location_tel_link', 'field_59522086e742b'),
(2958, 2, 'content_5_office_location_content_1_office_location_tel_display', '+1 212 5083 400'),
(2959, 2, '_content_5_office_location_content_1_office_location_tel_display', 'field_59522086e742c'),
(2960, 2, 'content_5_office_location_content_2_office_location_image', '284'),
(2961, 2, '_content_5_office_location_content_2_office_location_image', 'field_59522086e7428'),
(2962, 2, 'content_5_office_location_content_2_office_location_address_link', 'https://goo.gl/maps/peTkzDTVzH72'),
(2963, 2, '_content_5_office_location_content_2_office_location_address_link', 'field_59522086e7429'),
(2964, 2, 'content_5_office_location_content_2_office_location_address_display', 'Block B, 4/F, Building 3\r\nNo. 570 Yong Jia Road\r\nShanghai, 200031'),
(2965, 2, '_content_5_office_location_content_2_office_location_address_display', 'field_59522086e742a'),
(2966, 2, 'content_5_office_location_content_2_office_location_tel_link', '+862164868903'),
(2967, 2, '_content_5_office_location_content_2_office_location_tel_link', 'field_59522086e742b'),
(2968, 2, 'content_5_office_location_content_2_office_location_tel_display', '+86 21 6486 8903'),
(2969, 2, '_content_5_office_location_content_2_office_location_tel_display', 'field_59522086e742c'),
(2970, 2, 'content_5_office_location_content', '3'),
(2971, 2, '_content_5_office_location_content', 'field_595220a3e7437'),
(3025, 251, '_wp_attached_file', 'city_9.jpg'),
(3026, 251, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:850;s:4:\"file\";s:10:\"city_9.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3027, 252, '_wp_attached_file', 'nature_4.jpg'),
(3028, 252, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:850;s:4:\"file\";s:12:\"nature_4.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3029, 253, '_wp_attached_file', 'nightlife_4.jpg'),
(3030, 253, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:850;s:4:\"file\";s:15:\"nightlife_4.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3031, 254, '_wp_attached_file', 'TUC.jpg'),
(3032, 254, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:568;s:4:\"file\";s:7:\"TUC.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3033, 255, '_wp_attached_file', 'BOI.png'),
(3034, 255, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:568;s:4:\"file\";s:7:\"BOI.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3035, 256, '_wp_attached_file', 'Informa.png'),
(3036, 256, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:568;s:4:\"file\";s:11:\"Informa.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3037, 257, '_wp_attached_file', 'warwick.jpg'),
(3038, 257, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:568;s:4:\"file\";s:11:\"warwick.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3039, 258, '_wp_attached_file', 'manchester.jpg'),
(3040, 258, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:568;s:4:\"file\";s:14:\"manchester.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3041, 259, '_wp_attached_file', 'wesleyan.png'),
(3042, 259, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:568;s:4:\"file\";s:12:\"wesleyan.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3043, 260, '_wp_attached_file', 'member-1_bw.jpg'),
(3044, 260, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:400;s:4:\"file\";s:15:\"member-1_bw.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3045, 261, '_wp_attached_file', 'member-2_bw.jpg'),
(3046, 261, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:400;s:4:\"file\";s:15:\"member-2_bw.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3047, 262, '_wp_attached_file', 'member-3_bw.jpg'),
(3048, 262, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:400;s:4:\"file\";s:15:\"member-3_bw.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3049, 263, '_wp_attached_file', 'member-4_bw.jpg'),
(3050, 263, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:400;s:4:\"file\";s:15:\"member-4_bw.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3051, 264, '_wp_attached_file', 'london.jpg'),
(3052, 264, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:640;s:4:\"file\";s:10:\"london.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3053, 265, '_wp_attached_file', 'new-york.jpg'),
(3054, 265, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:640;s:4:\"file\";s:12:\"new-york.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3055, 266, '_wp_attached_file', 'shanghai.jpg'),
(3056, 266, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:640;s:4:\"file\";s:12:\"shanghai.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3057, 267, '_wp_attached_file', 'tuc_1.png'),
(3058, 267, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1167;s:6:\"height\";i:543;s:4:\"file\";s:9:\"tuc_1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3059, 268, '_wp_attached_file', 'tuc_2.jpg'),
(3060, 268, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:398;s:4:\"file\";s:9:\"tuc_2.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3061, 269, '_wp_attached_file', 'tuc_3.jpg'),
(3062, 269, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:398;s:4:\"file\";s:9:\"tuc_3.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3063, 270, '_wp_attached_file', 'tuc_4.jpg'),
(3064, 270, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:398;s:4:\"file\";s:9:\"tuc_4.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3065, 271, '_wp_attached_file', 'tuc_5.jpg'),
(3066, 271, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1167;s:6:\"height\";i:544;s:4:\"file\";s:9:\"tuc_5.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3067, 272, '_wp_attached_file', 'tuc_6.jpg'),
(3068, 272, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:398;s:4:\"file\";s:9:\"tuc_6.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3069, 273, '_wp_attached_file', 'tuc_7.jpg'),
(3070, 273, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:398;s:4:\"file\";s:9:\"tuc_7.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3071, 274, '_wp_attached_file', 'tuc_8.jpg'),
(3072, 274, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:398;s:4:\"file\";s:9:\"tuc_8.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3073, 275, '_wp_attached_file', 'tuc_9.jpg'),
(3074, 275, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:576;s:6:\"height\";i:398;s:4:\"file\";s:9:\"tuc_9.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3085, 278, '_wp_attached_file', 'full-width.png'),
(3086, 278, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1167;s:6:\"height\";i:543;s:4:\"file\";s:14:\"full-width.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3087, 279, '_wp_attached_file', 'slider.png'),
(3088, 279, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:850;s:4:\"file\";s:10:\"slider.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3089, 280, '_wp_attached_file', 'case-study.png'),
(3090, 280, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:568;s:4:\"file\";s:14:\"case-study.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3127, 2, 'content_6_section_id', 'footer'),
(3128, 2, '_content_6_section_id', 'field_594bc09c4427e'),
(3129, 2, 'content_6_column_width', '50-50'),
(3130, 2, '_content_6_column_width', 'field_594bc09c4427f'),
(3131, 2, 'content_6_padding_top', '5'),
(3132, 2, '_content_6_padding_top', 'field_594bc09c44283'),
(3133, 2, 'content_6_padding_bottom', '20'),
(3134, 2, '_content_6_padding_bottom', 'field_594bc09c44284'),
(3135, 2, 'content_6_background_color', ''),
(3136, 2, '_content_6_background_color', 'field_594bc09c44280'),
(3137, 2, 'content_6_background_image', ''),
(3138, 2, '_content_6_background_image', 'field_594bc09c44281');
INSERT INTO `ln2017_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(3139, 2, 'content_6_background_position', 'center center'),
(3140, 2, '_content_6_background_position', 'field_594bc09c44282'),
(3141, 2, 'content_6_left_column_text', '<div class=\"worked-with-box\">\r\n<p class=\"heavy-metal-text\">Who we\'ve worked with</p>\r\n\r\n<ul class=\"worked-with\">\r\n 	<li>BAA</li>\r\n 	<li>Barbican</li>\r\n 	<li>Belfast City Council</li>\r\n 	<li>British Citizenship</li>\r\n 	<li>British Council</li>\r\n 	<li>BUPA</li>\r\n 	<li>John Lewis</li>\r\n 	<li>John Paul Getty Museums</li>\r\n 	<li>Millennium Hotels</li>\r\n 	<li>NS&amp;I</li>\r\n 	<li>Ofqual</li>\r\n 	<li>Regent\'s University London</li>\r\n 	<li>SOAS</li>\r\n 	<li>University College Cork</li>\r\n</ul>\r\n</div>'),
(3142, 2, '_content_6_left_column_text', 'field_594bc09c44285'),
(3143, 2, 'content_6_right_column_text', '<div class=\"contact-box wild-sand-bkg\">\r\n\r\n<div class=\"contact-email\">\r\n<a href=\"mailto:studio@lloydnorthover.com\">studio@lloydnorthover.com</a>\r\n</div>\r\n<div class=\"contact-tel\">\r\n<a class=\"contact-tel\" href=\"tel:02072918400\">T +44 (0)20 7291 8400</a>\r\n</div>\r\n\r\n<div class=\"social-icons\">\r\n<a href=\"https://uk.linkedin.com/company/lloyd-northover\" target=\"_blank\"><i class=\"fa fa-linkedin fa-2x\" aria-hidden=\"true\"></i></a> &nbsp; <a href=\"https://twitter.com/LloydNorthover\" target=\"_blank\"><i class=\"fa fa-twitter fa-fw fa-2x\" aria-hidden=\"true\"></i></a> <a href=\"https://www.facebook.com/Lloyd-Northover-403411660052705/\" target=\"_blank\"><i class=\"fa fa-facebook fa-fw fa-2x\" aria-hidden=\"true\"></i></a>\r\n</div>\r\n\r\n<div class=\"sister-agency\">\r\nSister agency to <a href=\"http://www.holmesandmarchant.com/\" target=\"_blank\">Holmes &amp; Marchant</a> and a member of <a href=\"http://www.msqpartners.com/\" target=\"_blank\">MSQ Partners Limited</a>\r\n</div>\r\n</div>'),
(3144, 2, '_content_6_right_column_text', 'field_594bc09c44286'),
(3159, 2, 'content_2_case_study_info_0_modal_bkg_color', '#ffffff'),
(3160, 2, '_content_2_case_study_info_0_modal_bkg_color', 'field_59527c7261cba'),
(3161, 2, 'content_2_case_study_info_1_modal_bkg_color', '#FFFFFF'),
(3162, 2, '_content_2_case_study_info_1_modal_bkg_color', 'field_59527c7261cba'),
(3163, 2, 'content_2_case_study_info_2_modal_bkg_color', '#FFFFFF'),
(3164, 2, '_content_2_case_study_info_2_modal_bkg_color', 'field_59527c7261cba'),
(3165, 2, 'content_2_case_study_info_3_modal_bkg_color', '#FFFFFF'),
(3166, 2, '_content_2_case_study_info_3_modal_bkg_color', 'field_59527c7261cba'),
(3167, 2, 'content_2_case_study_info_4_modal_bkg_color', '#FFFFFF'),
(3168, 2, '_content_2_case_study_info_4_modal_bkg_color', 'field_59527c7261cba'),
(3169, 2, 'content_2_case_study_info_5_modal_bkg_color', '#ffffff'),
(3170, 2, '_content_2_case_study_info_5_modal_bkg_color', 'field_59527c7261cba'),
(3171, 282, '_wp_attached_file', 'london_ldn.jpg'),
(3172, 282, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:640;s:4:\"file\";s:14:\"london_ldn.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3173, 283, '_wp_attached_file', 'new-york_ny.jpg'),
(3174, 283, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:640;s:4:\"file\";s:15:\"new-york_ny.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3175, 284, '_wp_attached_file', 'shanghai_sh.jpg'),
(3176, 284, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:640;s:4:\"file\";s:15:\"shanghai_sh.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3195, 286, '_wp_attached_file', 'offer_graphic.png'),
(3196, 286, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:302;s:6:\"height\";i:302;s:4:\"file\";s:17:\"offer_graphic.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3215, 2, 'content_4_section_id', 'what-we-offer'),
(3216, 2, '_content_4_section_id', 'field_595379b2268ed'),
(3217, 2, 'content_4_padding_top', '20'),
(3218, 2, '_content_4_padding_top', 'field_595379b2268ee'),
(3219, 2, 'content_4_padding_bottom', '50'),
(3220, 2, '_content_4_padding_bottom', 'field_595379b2268ef'),
(3221, 2, 'content_4_offer_content_left', '<p class=\"heavy-metal-text\">What do we offer?</p>\r\n\r\n<ul class=\"brand-offers\">\r\n 	<li class=\"brand_underline\">Brand Research</li>\r\n 	<li class=\"brand_underline\">Brand Strategy</li>\r\n 	<li class=\"brand_underline\">Brand Design</li>\r\n 	<li class=\"brand_underline\">Brand Communications</li>\r\n</ul>'),
(3222, 2, '_content_4_offer_content_left', 'field_595379b2268f3'),
(3223, 2, 'content_4_modal_bkg_color', '#fdbb27'),
(3224, 2, '_content_4_modal_bkg_color', 'field_595379b2268f6'),
(3225, 2, 'content_4_modal_content_left', '<div class=\"black-text grey-underline pad-right-30\">Brand Research</div>\r\n<div class=\"white-text\">Brand Intelligence (Data/Analytics)\r\nStakeholder Research\r\nConsumer and Trends Research\r\nCompetitor Audit</div>\r\n\r\n<div class=\"pad-top-10 black-text grey-underline\">Brand Strategy</div>\r\n<div class=\"white-text\">Positioning/Architecture Development\r\nStakeholder buy in\r\nPlanning</div>\r\n\r\n<div class=\"pad-top-10 black-text grey-underline\">Brand Design</div>\r\n<div class=\"white-text\">Corporate Identity\r\nPhysical Design\r\nDigital Design\r\nNaming</div>\r\n\r\n<div class=\"pad-top-10 black-text grey-underline\">Brand Communications</div>\r\n<div class=\"white-text\">Brand Activation and Communications\r\nEmployee Communications\r\nInvestor/Annual Reports\r\nBrand Guidelines\r\nDAM Systems\r\nBrand Intelligence (Monitoring)</div>'),
(3226, 2, '_content_4_modal_content_left', 'field_595379b2268f2'),
(3227, 2, 'content_4_modal_content_right', '<div class=\"offer-image\">\r\n<img src=\"http://localhost:8888/lloydnorthover/wp-content/uploads/offer_graphic.png\" alt=\"\" width=\"302\" height=\"302\" size-full wp-image-286\" />\r\n</div>'),
(3228, 2, '_content_4_modal_content_right', 'field_59537a3226904'),
(3229, 2, 'content_4_offer_content_right', ''),
(3230, 2, '_content_4_offer_content_right', 'field_59537a4426905'),
(3231, 2, 'content_4_offer_id_one', 'offer'),
(3232, 2, '_content_4_offer_id_one', 'field_5953ca8a91a50'),
(3233, 2, 'content_4_offer_id_two', 'idea'),
(3234, 2, '_content_4_offer_id_two', 'field_5953cd88a0e5a'),
(3235, 2, 'content_4_modal_bkg_color_one', '#fdbb27'),
(3236, 2, '_content_4_modal_bkg_color_one', 'field_595379b2268f6'),
(3237, 2, 'content_4_modal_content_left_one', '<div class=\"black-text white-underline pad-right-30\">Brand Research</div>\r\n<div class=\"white-text\">Brand Intelligence (Data/Analytics)\r\nStakeholder Research\r\nConsumer and Trends Research\r\nCompetitor Audit</div>\r\n<div class=\"pad-top-10 black-text white-underline\">Brand Strategy</div>\r\n<div class=\"white-text\">Positioning/Architecture Development\r\nStakeholder buy in\r\nPlanning</div>\r\n<div class=\"pad-top-10 black-text white-underline\">Brand Design</div>\r\n<div class=\"white-text\">Corporate Identity\r\nPhysical Design\r\nDigital Design\r\nNaming</div>\r\n<div class=\"pad-top-10 black-text white-underline\">Brand Communications</div>\r\n<div class=\"white-text\">Brand Activation and Communications\r\nEmployee Communications\r\nInvestor/Annual Reports\r\nBrand Guidelines\r\nDAM Systems\r\nBrand Intelligence (Monitoring)</div>'),
(3238, 2, '_content_4_modal_content_left_one', 'field_595379b2268f2'),
(3239, 2, 'content_4_modal_content_right_one', '<div class=\"offer-image\">\r\n<img src=\"http://localhost:8888/lloydnorthover/wp-content/uploads/offer_graphic.png\" alt=\"\" width=\"302\" height=\"302\" />\r\n</div>'),
(3240, 2, '_content_4_modal_content_right_one', 'field_59537a3226904'),
(3241, 2, 'content_4_modal_bkg_color_two', '#fdbb27'),
(3242, 2, '_content_4_modal_bkg_color_two', 'field_5953d8aaa8baa'),
(3243, 2, 'content_4_modal_content_left_two', '<div class=\"white-text pad-right-30\">\r\nIf you’re planning a start up or lead an early stage venture then get in touch. \r\n\r\nEvery year we help one or two ventures that we see future value in. We will develop the brand research, strategy, positioning, naming, design and more in return for some sort of mutually agreed interest (equity stake, revenue share or something else of value to us).\r\n\r\nIn the last year we supported two ventures you see here – the creation of the new Bibelot online jewellery brand and the branding of a unique talent management agency, Actorum. \r\n\r\nThink of it as brand design venture capital. So let\'s talk about your plans.\r\n</div>'),
(3244, 2, '_content_4_modal_content_left_two', 'field_5953d8c5a8bab'),
(3245, 2, 'content_4_modal_content_right_two', ''),
(3246, 2, '_content_4_modal_content_right_two', 'field_5953d8d5a8bac'),
(3247, 2, 'content_4_offer_id_three', 'brand'),
(3248, 2, '_content_4_offer_id_three', 'field_5954ba7f657f9'),
(3249, 2, 'content_4_modal_bkg_color_three', '#fdbb27'),
(3250, 2, '_content_4_modal_bkg_color_three', 'field_5954ba6e657f8'),
(3251, 2, 'content_4_modal_content_left_three', ''),
(3252, 2, '_content_4_modal_content_left_three', 'field_5954ba61657f7'),
(3253, 2, 'content_4_modal_content_right_three', ''),
(3254, 2, '_content_4_modal_content_right_three', 'field_5954ba56657f6'),
(3255, 2, 'content_4_offer_content_right_first', '<p class=\"heavy-metal-text\">Need help with a great business idea?</p>\r\n<p class=\"brand_underline\">Brand Ventures</p>'),
(3256, 2, '_content_4_offer_content_right_first', 'field_59537a4426905'),
(3257, 2, 'content_4_offer_content_right_second', '<p class=\"heavy-metal-text\">Is your brand on brand?</p>\r\n<p class=\"brand_underline\">Brand Dashboard</p>'),
(3258, 2, '_content_4_offer_content_right_second', 'field_5954cc4087fea'),
(3283, 2, 'content_4_modal_content_left_hwi_one', ''),
(3284, 2, '_content_4_modal_content_left_hwi_one', 'field_59550c72b9450'),
(3285, 2, 'content_4_modal_content_left_hwi_two', ''),
(3286, 2, '_content_4_modal_content_left_hwi_two', 'field_59550c8cb9451'),
(3287, 2, 'content_4_modal_content_right_hwi_one', ''),
(3288, 2, '_content_4_modal_content_right_hwi_one', 'field_59550c1eb944e'),
(3289, 2, 'content_4_modal_content_right_hwi_two', ''),
(3290, 2, '_content_4_modal_content_right_hwi_two', 'field_59550c61b944f'),
(3291, 312, '_wp_attached_file', 'brand-purpose-story.png'),
(3292, 312, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:476;s:6:\"height\";i:251;s:4:\"file\";s:23:\"brand-purpose-story.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(3293, 2, 'content_4_modal_content_three_intro', '<div class=\"white-text pad-right-30 pad-bottom-20\">\r\nOur leading edge Brand Dashboard combines AI ‘deep learning’ tools with expert analysis to give you greater breadth, depth and validity to brand intelligence. Driving more insightful brand strategy development and providing a new way to monitor your brand consistency over time, this is brand analytics for the 21st century.\r\n</div>'),
(3294, 2, '_content_4_modal_content_three_intro', 'field_595524d492611'),
(3295, 2, 'content_4_modal_content_three_left', '<div class=\"black-text white-underline\">Inputs/Scan</div>\r\n<div class=\"white-text\">\r\nWe scan all brand language for:\r\n<ul>\r\n	<li>Client brand</li>\r\n	<li>All competitors</li>\r\n	<li>Web sites</li>\r\n	<li>3rd party sites</li>\r\n	<li>LinkedIN data</li>\r\n	<li>Social channels</li>\r\n	<li>Incl. embedded (e.g. pdf) content</li>\r\n	<li>Intranet/employee comms (given access)</li>\r\n	<li>Client brand’s Content Marketing</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"pad-top-10 black-text white-underline\">Deep Learning</div>\r\n<div class=\"white-text\">\r\nOur AI tool distils and clusters vast text quantities of 100m+\r\nAs used by Netflix for 100m user reviews and Badoo for 300m.\r\n</div>\r\n\r\n<div class=\"pad-top-10 black-text white-underline\">Analysis</div>\r\n<div class=\"white-text\">\r\nWe zoom in, analyse and identify patterns to determine:\r\n<ul>\r\n	<li>Category language (for commonality & outliers)</li>\r\n	<li>Client brand language</li>\r\n	<li>White space opportunities (Positioning)</li>\r\n	<li>Changes over time (Monitoring)</li>\r\n	<li>Audience characteristics</li>\r\n	<li>Trends</li>\r\n</ul>\r\n</div>\r\n\r\n<div class=\"pad-top-10 black-text white-underline\">Output</div>\r\n<div class=\"white-text\">\r\nPositioning development\r\nWider, more objective, fact based data to unlock (re)positioning opportunity\r\n\r\nBrand monitoring\r\nQuarterly Brand Dashboard to show degree to which you (marketing and people) are ‘on brand’ and changes over time.\r\n</div>'),
(3296, 2, '_content_4_modal_content_three_left', 'field_5954ba61657f7'),
(3297, 2, 'content_4_modal_content_three_right', '<div class=\"offer-image\"><img src=\"http://localhost:8888/lloydnorthover/wp-content/uploads/offer_graphic.png\" alt=\"\" width=\"302\" height=\"302\" /></div>'),
(3298, 2, '_content_4_modal_content_three_right', 'field_5954ba56657f6'),
(3299, 2, 'content_4_mini_case_study_info_0_mcs_description', '<div class=\"white-text pad-right-30\">\r\nThe new Bibelot online jewellery brand. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n</div>'),
(3300, 2, '_content_4_mini_case_study_info_0_mcs_description', 'field_595616c25882e'),
(3301, 2, 'content_4_mini_case_study_info_0_mcs_image_one', '280'),
(3302, 2, '_content_4_mini_case_study_info_0_mcs_image_one', 'field_59550c72b9450'),
(3303, 2, 'content_4_mini_case_study_info_0_mcs_image_two', '280'),
(3304, 2, '_content_4_mini_case_study_info_0_mcs_image_two', 'field_59550c8cb9451'),
(3305, 2, 'content_4_mini_case_study_info_0_mcs_image_three', '280'),
(3306, 2, '_content_4_mini_case_study_info_0_mcs_image_three', 'field_5956172a58830'),
(3315, 2, 'content_4_mini_case_study_info', '2'),
(3316, 2, '_content_4_mini_case_study_info', 'field_5956168f5882d'),
(3317, 2, 'content_4_mini_case_study_info_1_mcs_description', '<div class=\"white-text pad-right-30 pad-bottom-30\">\r\nThe unique talent management agency, Actorum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\r\n</div>'),
(3318, 2, '_content_4_mini_case_study_info_1_mcs_description', 'field_595616c25882e'),
(3319, 2, 'content_4_mini_case_study_info_1_mcs_image_one', '280'),
(3320, 2, '_content_4_mini_case_study_info_1_mcs_image_one', 'field_59550c72b9450'),
(3321, 2, 'content_4_mini_case_study_info_1_mcs_image_two', '280'),
(3322, 2, '_content_4_mini_case_study_info_1_mcs_image_two', 'field_59550c8cb9451'),
(3323, 2, 'content_4_mini_case_study_info_1_mcs_image_three', '280'),
(3324, 2, '_content_4_mini_case_study_info_1_mcs_image_three', 'field_5956172a58830'),
(3325, 2, 'content_4_modal_content_two', '<div class=\"white-text pad-right-30 pad-bottom-30\">\r\nIf you’re planning a start up or lead an early stage venture then get in touch. \r\n\r\nEvery year we help one or two ventures that we see future value in. We will develop the brand research, strategy, positioning, naming, design and more in return for some sort of mutually agreed interest (equity stake, revenue share or something else of value to us).\r\n\r\nIn the last year we supported two ventures you see here – the creation of the new Bibelot online jewellery brand and the branding of a unique talent management agency, Actorum. \r\n\r\nThink of it as brand design venture capital. So let\'s talk about your plans.\r\n</div>'),
(3326, 2, '_content_4_modal_content_two', 'field_5953d8c5a8bab'),
(3327, 2, 'content_4_mini_case_study_info_0_mcs_title', 'Bibelot'),
(3328, 2, '_content_4_mini_case_study_info_0_mcs_title', 'field_59565bdfb42bb'),
(3329, 2, 'content_4_mini_case_study_info_0_mcs_subtitle', 'Lorem ipsum'),
(3330, 2, '_content_4_mini_case_study_info_0_mcs_subtitle', 'field_59565c08b42bc'),
(3331, 2, 'content_4_mini_case_study_info_1_mcs_title', 'Actorum'),
(3332, 2, '_content_4_mini_case_study_info_1_mcs_title', 'field_59565bdfb42bb'),
(3333, 2, 'content_4_mini_case_study_info_1_mcs_subtitle', 'Lorem ipsum'),
(3334, 2, '_content_4_mini_case_study_info_1_mcs_subtitle', 'field_59565c08b42bc'),
(3357, 2, 'content_2_case_study_info_0_case_study_youtube_video', 'https://www.youtube.com/embed/OPf0YbXqDm0'),
(3358, 2, '_content_2_case_study_info_0_case_study_youtube_video', 'field_595a48f6e72d1'),
(3359, 2, 'content_2_case_study_info_1_case_study_youtube_video', ''),
(3360, 2, '_content_2_case_study_info_1_case_study_youtube_video', 'field_595a48f6e72d1'),
(3361, 2, 'content_2_case_study_info_2_case_study_youtube_video', ''),
(3362, 2, '_content_2_case_study_info_2_case_study_youtube_video', 'field_595a48f6e72d1'),
(3363, 2, 'content_2_case_study_info_3_case_study_youtube_video', ''),
(3364, 2, '_content_2_case_study_info_3_case_study_youtube_video', 'field_595a48f6e72d1'),
(3365, 2, 'content_2_case_study_info_4_case_study_youtube_video', ''),
(3366, 2, '_content_2_case_study_info_4_case_study_youtube_video', 'field_595a48f6e72d1'),
(3367, 2, 'content_2_case_study_info_5_case_study_youtube_video', ''),
(3368, 2, '_content_2_case_study_info_5_case_study_youtube_video', 'field_595a48f6e72d1'),
(3369, 2, 'content_2_case_study_info_0_case_study_vimeo_video', 'https://player.vimeo.com/video/22439234'),
(3370, 2, '_content_2_case_study_info_0_case_study_vimeo_video', 'field_595a56eaea12c'),
(3371, 2, 'content_2_case_study_info_1_case_study_vimeo_video', ''),
(3372, 2, '_content_2_case_study_info_1_case_study_vimeo_video', 'field_595a56eaea12c'),
(3373, 2, 'content_2_case_study_info_2_case_study_vimeo_video', ''),
(3374, 2, '_content_2_case_study_info_2_case_study_vimeo_video', 'field_595a56eaea12c'),
(3375, 2, 'content_2_case_study_info_3_case_study_vimeo_video', ''),
(3376, 2, '_content_2_case_study_info_3_case_study_vimeo_video', 'field_595a56eaea12c'),
(3377, 2, 'content_2_case_study_info_4_case_study_vimeo_video', ''),
(3378, 2, '_content_2_case_study_info_4_case_study_vimeo_video', 'field_595a56eaea12c'),
(3379, 2, 'content_2_case_study_info_5_case_study_vimeo_video', ''),
(3380, 2, '_content_2_case_study_info_5_case_study_vimeo_video', 'field_595a56eaea12c'),
(3387, 2, 'content_2_case_study_info_0_case_study_content_2_case_study_hwi_three', '270'),
(3388, 2, '_content_2_case_study_info_0_case_study_content_2_case_study_hwi_three', 'field_5950cfe2345de'),
(3389, 2, 'content_2_case_study_info_0_case_study_content_2_case_study_quote', 'Thank you so much for this, it was so good, I was delighted!'),
(3390, 2, '_content_2_case_study_info_0_case_study_content_2_case_study_quote', 'field_594d368bcd8db'),
(3391, 2, 'content_2_case_study_info_0_case_study_content_2_case_study_quote_author', 'Antonia Bance, Head of Communications and Campaigns, TUC'),
(3392, 2, '_content_2_case_study_info_0_case_study_content_2_case_study_quote_author', 'field_5950d0fdd40d0'),
(3393, 2, 'content_2_case_study_info_0_case_study_content_3_case_study_fwi_one', '271'),
(3394, 2, '_content_2_case_study_info_0_case_study_content_3_case_study_fwi_one', 'field_594a83dd79f65'),
(3395, 2, 'content_2_case_study_info_0_case_study_content_4_case_study_hwi_one', '272'),
(3396, 2, '_content_2_case_study_info_0_case_study_content_4_case_study_hwi_one', 'field_594d364acd8da'),
(3397, 2, 'content_2_case_study_info_0_case_study_content_4_case_study_hwi_two', '273'),
(3398, 2, '_content_2_case_study_info_0_case_study_content_4_case_study_hwi_two', 'field_5950ce730216e'),
(3399, 2, 'content_2_case_study_info_0_case_study_content_5_case_study_hwi_one', '274'),
(3400, 2, '_content_2_case_study_info_0_case_study_content_5_case_study_hwi_one', 'field_594d364acd8da'),
(3401, 2, 'content_2_case_study_info_0_case_study_content_5_case_study_hwi_two', '275'),
(3402, 2, '_content_2_case_study_info_0_case_study_content_5_case_study_hwi_two', 'field_5950ce730216e'),
(3411, 2, 'content_2_case_study_info_0_case_study_content_0_case_study_fwi_one', '267'),
(3412, 2, '_content_2_case_study_info_0_case_study_content_0_case_study_fwi_one', 'field_594a83dd79f65'),
(3413, 2, 'content_2_case_study_info_0_case_study_content_1_case_study_hwi_one', '268'),
(3414, 2, '_content_2_case_study_info_0_case_study_content_1_case_study_hwi_one', 'field_594d364acd8da'),
(3415, 2, 'content_2_case_study_info_0_case_study_content_1_case_study_hwi_two', '269'),
(3416, 2, '_content_2_case_study_info_0_case_study_content_1_case_study_hwi_two', 'field_5950ce730216e');

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_posts`
--

CREATE TABLE `ln2017_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(255) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ;

--
-- Dumping data for table `ln2017_posts`
--

INSERT INTO `ln2017_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(2, 1, '2017-06-19 09:09:25', '2017-06-19 08:09:25', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2017-07-04 17:01:03', '2017-07-04 16:01:03', '', 0, 'http://localhost:8888/lloydnorthover/?page_id=2', 0, 'page', '', 0),
(10, 1, '2017-06-19 10:51:55', '2017-06-19 09:51:55', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:15:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:13:\"custom_fields\";i:4;s:10:\"discussion\";i:5;s:8:\"comments\";i:6;s:9:\"revisions\";i:7;s:4:\"slug\";i:8;s:6:\"author\";i:9;s:6:\"format\";i:10;s:15:\"page_attributes\";i:11;s:14:\"featured_image\";i:12;s:10:\"categories\";i:13;s:4:\"tags\";i:14;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Custom Layout', 'custom-layout', 'publish', 'closed', 'closed', '', 'group_59479e8a50c6b', '', '', '2017-07-03 16:16:25', '2017-07-03 15:16:25', '', 0, 'http://localhost:8888/lloydnorthover/?post_type=acf-field-group&#038;p=10', 0, 'acf-field-group', '', 0),
(11, 1, '2017-06-19 10:51:55', '2017-06-19 09:51:55', 'a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:7:{s:13:\"594a456cc96d6\";a:6:{s:3:\"key\";s:13:\"594a456cc96d6\";s:5:\"label\";s:6:\"SLIDER\";s:4:\"name\";s:12:\"slick_slider\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"59479e9adfafc\";a:6:{s:3:\"key\";s:13:\"59479e9adfafc\";s:5:\"label\";s:17:\"TEXT - FULL WIDTH\";s:4:\"name\";s:10:\"text_block\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"594bc09c4427d\";a:6:{s:3:\"key\";s:13:\"594bc09c4427d\";s:5:\"label\";s:18:\"TEXT - TWO COLUMNS\";s:4:\"name\";s:22:\"text_block_two_columns\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"594a818a79f5c\";a:6:{s:3:\"key\";s:13:\"594a818a79f5c\";s:5:\"label\";s:10:\"CASE STUDY\";s:4:\"name\";s:16:\"case_study_block\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"595379b2268ec\";a:6:{s:3:\"key\";s:13:\"595379b2268ec\";s:5:\"label\";s:5:\"OFFER\";s:4:\"name\";s:11:\"offer_block\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"594bc8bbec633\";a:6:{s:3:\"key\";s:13:\"594bc8bbec633\";s:5:\"label\";s:6:\"PEOPLE\";s:4:\"name\";s:10:\"team_block\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"59522086e7424\";a:6:{s:3:\"key\";s:13:\"59522086e7424\";s:5:\"label\";s:9:\"LOCATIONS\";s:4:\"name\";s:22:\"office_locations_block\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:7:\"Add Row\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Content', 'content', 'publish', 'closed', 'closed', '', 'field_59479e957e9d0', '', '', '2017-06-28 10:44:22', '2017-06-28 09:44:22', '', 10, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=11', 0, 'acf-field', '', 0),
(12, 1, '2017-06-19 10:57:47', '2017-06-19 09:57:47', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59479e9adfafc\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section ID', 'section_id', 'publish', 'closed', 'closed', '', 'field_59479ecba89ff', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=12', 0, 'acf-field', '', 0),
(14, 1, '2017-06-19 10:57:47', '2017-06-19 09:57:47', 'a:7:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59479e9adfafc\";s:13:\"default_value\";s:0:\"\";}', 'Background Color', 'background_color', 'publish', 'closed', 'closed', '', 'field_59479f28a8a01', '', '', '2017-06-23 14:41:59', '2017-06-23 13:41:59', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=14', 3, 'acf-field', '', 0),
(15, 1, '2017-06-19 10:57:47', '2017-06-19 09:57:47', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59479e9adfafc\";s:13:\"return_format\";s:0:\"\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Background Image', 'background_image', 'publish', 'closed', 'closed', '', 'field_59479f4da8a02', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=15', 4, 'acf-field', '', 0),
(16, 1, '2017-06-19 10:57:47', '2017-06-19 09:57:47', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59479e9adfafc\";s:13:\"default_value\";s:13:\"center center\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Background Position', 'background_position', 'publish', 'closed', 'closed', '', 'field_59479f74a8a03', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=16', 5, 'acf-field', '', 0),
(17, 1, '2017-06-19 10:57:47', '2017-06-19 09:57:47', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59479e9adfafc\";s:13:\"default_value\";i:0;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Top', 'padding_top', 'publish', 'closed', 'closed', '', 'field_59479f9fa8a04', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=17', 1, 'acf-field', '', 0),
(18, 1, '2017-06-19 10:57:47', '2017-06-19 09:57:47', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59479e9adfafc\";s:13:\"default_value\";i:0;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Bottom', 'padding_bottom', 'publish', 'closed', 'closed', '', 'field_59479fb8a8a05', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=18', 2, 'acf-field', '', 0),
(19, 1, '2017-06-19 10:57:47', '2017-06-19 09:57:47', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59479e9adfafc\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Full Width Text', 'full_width_text', 'publish', 'closed', 'closed', '', 'field_59479fd1a8a06', '', '', '2017-06-22 14:13:44', '2017-06-22 13:13:44', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=19', 6, 'acf-field', '', 0),
(130, 1, '2017-06-21 11:15:58', '2017-06-21 10:15:58', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a456cc96d6\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section ID', 'section_id', 'publish', 'closed', 'closed', '', 'field_594a4577c96d7', '', '', '2017-06-29 11:06:30', '2017-06-29 10:06:30', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=130', 0, 'acf-field', '', 0),
(133, 1, '2017-06-21 11:15:58', '2017-06-21 10:15:58', 'a:17:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:63:\"Add images to the slider.\r\nImage Dimensions should be 1200x850.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a456cc96d6\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Slides', 'slides', 'publish', 'closed', 'closed', '', 'field_594a4739c96da', '', '', '2017-06-29 11:18:51', '2017-06-29 10:18:51', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=133', 1, 'acf-field', '', 0),
(139, 1, '2017-06-21 15:34:49', '2017-06-21 14:34:49', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section ID', 'section_id', 'publish', 'closed', 'closed', '', 'field_594a81ce79f5d', '', '', '2017-06-22 13:47:10', '2017-06-22 12:47:10', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=139', 0, 'acf-field', '', 0),
(140, 1, '2017-06-21 15:34:49', '2017-06-21 14:34:49', 'a:11:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:137:\"Click the blue button to add a case study. It\'s best to add two at a time to avoid whitespace. The optimum number of case studies is six.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:14:\"Add Case Study\";}', 'Case Study Info', 'case_study_info', 'publish', 'closed', 'closed', '', 'field_594a81e279f5e', '', '', '2017-07-03 16:16:25', '2017-07-03 15:16:25', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=140', 3, 'acf-field', '', 0),
(141, 1, '2017-06-21 15:34:49', '2017-06-21 14:34:49', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:98:\"This is the image users will click on to reveal the modal box. Image Dimensions should be 800x568.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Case Study Image', 'case_study_image', 'publish', 'closed', 'closed', '', 'field_594a81fd79f5f', '', '', '2017-06-29 11:18:51', '2017-06-29 10:18:51', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=141', 0, 'acf-field', '', 0),
(142, 1, '2017-06-21 15:34:49', '2017-06-21 14:34:49', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:64:\"Give the Case Study an ID. This must be one word, all lowercase.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"15\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'ID', 'case_study_id', 'publish', 'closed', 'closed', '', 'field_594a826c79f60', '', '', '2017-06-27 16:48:29', '2017-06-27 15:48:29', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=142', 1, 'acf-field', '', 0),
(143, 1, '2017-06-21 15:34:50', '2017-06-21 14:34:50', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"15\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'case_study_title', 'publish', 'closed', 'closed', '', 'field_594a82e479f61', '', '', '2017-06-27 16:48:29', '2017-06-27 15:48:29', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=143', 2, 'acf-field', '', 0),
(144, 1, '2017-06-21 15:34:50', '2017-06-21 14:34:50', 'a:11:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"15\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:8;s:9:\"new_lines\";s:0:\"\";}', 'Subtitle', 'case_study_subtitle', 'publish', 'closed', 'closed', '', 'field_594a834479f62', '', '', '2017-06-27 16:48:29', '2017-06-27 15:48:29', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=144', 4, 'acf-field', '', 0),
(145, 1, '2017-06-21 15:34:50', '2017-06-21 14:34:50', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Description', 'case_study_description', 'publish', 'closed', 'closed', '', 'field_594a837e79f63', '', '', '2017-06-27 16:48:29', '2017-06-27 15:48:29', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=145', 6, 'acf-field', '', 0),
(146, 1, '2017-06-21 15:34:50', '2017-06-21 14:34:50', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'What We Did', 'case_study_what_we_did', 'publish', 'closed', 'closed', '', 'field_594a838e79f64', '', '', '2017-06-27 16:42:02', '2017-06-27 15:42:02', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=146', 7, 'acf-field', '', 0),
(147, 1, '2017-06-21 15:34:50', '2017-06-21 14:34:50', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:139:\"Image Dimensions should be 1200x850.\r\nThey don\'t have to be 850 high. This is only if you want all images to have consistent aspect ratios.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:3:\"100\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5df5a2973\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Full Width Image', 'case_study_fwi_one', 'publish', 'closed', 'closed', '', 'field_594a83dd79f65', '', '', '2017-07-03 16:16:25', '2017-07-03 15:16:25', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=147', 0, 'acf-field', '', 0),
(156, 1, '2017-06-22 13:47:10', '2017-06-22 12:47:10', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Top', 'padding_top', 'publish', 'closed', 'closed', '', 'field_594bbc21066ba', '', '', '2017-06-22 13:47:10', '2017-06-22 12:47:10', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=156', 1, 'acf-field', '', 0),
(157, 1, '2017-06-22 13:47:10', '2017-06-22 12:47:10', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Bottom', 'padding_bottom', 'publish', 'closed', 'closed', '', 'field_594bbc31066bb', '', '', '2017-06-22 13:47:10', '2017-06-22 12:47:10', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=157', 2, 'acf-field', '', 0),
(158, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section ID', 'section_id', 'publish', 'closed', 'closed', '', 'field_594bc09c4427e', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=158', 0, 'acf-field', '', 0),
(159, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:14:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:7:\"choices\";a:5:{s:5:\"50-50\";s:19:\"50/50 Equal columns\";s:5:\"25-75\";s:29:\"25/75 Narrow left, Wide Right\";s:5:\"75-25\";s:29:\"75/25 Wide left, Narrow Right\";s:3:\"5-7\";s:29:\"5/7 Almost equal. Wider Right\";s:3:\"7-5\";s:28:\"7/5 Almost equal. Wider Left\";}s:13:\"default_value\";a:1:{i:0;s:5:\"50-50\";}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:4:\"ajax\";i:0;s:13:\"return_format\";s:5:\"value\";s:11:\"placeholder\";s:0:\"\";}', 'Column Width', 'column_width', 'publish', 'closed', 'closed', '', 'field_594bc09c4427f', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=159', 1, 'acf-field', '', 0),
(160, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"default_value\";i:0;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Top', 'padding_top', 'publish', 'closed', 'closed', '', 'field_594bc09c44283', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=160', 2, 'acf-field', '', 0),
(161, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"default_value\";i:0;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Bottom', 'padding_bottom', 'publish', 'closed', 'closed', '', 'field_594bc09c44284', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=161', 3, 'acf-field', '', 0),
(162, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:7:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"default_value\";s:0:\"\";}', 'Background Color', 'background_color', 'publish', 'closed', 'closed', '', 'field_594bc09c44280', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=162', 4, 'acf-field', '', 0),
(163, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Background Image', 'background_image', 'publish', 'closed', 'closed', '', 'field_594bc09c44281', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=163', 5, 'acf-field', '', 0),
(164, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"default_value\";s:13:\"center center\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Background Position', 'background_position', 'publish', 'closed', 'closed', '', 'field_594bc09c44282', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=164', 6, 'acf-field', '', 0),
(165, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Left column text', 'left_column_text', 'publish', 'closed', 'closed', '', 'field_594bc09c44285', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=165', 7, 'acf-field', '', 0),
(166, 1, '2017-06-22 14:10:33', '2017-06-22 13:10:33', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc09c4427d\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Right column text', 'right_column_text', 'publish', 'closed', 'closed', '', 'field_594bc09c44286', '', '', '2017-06-22 14:10:33', '2017-06-22 13:10:33', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=166', 8, 'acf-field', '', 0),
(173, 1, '2017-06-22 14:44:09', '2017-06-22 13:44:09', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc8bbec633\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section ID', 'section_id', 'publish', 'closed', 'closed', '', 'field_594bc8bbec634', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=173', 0, 'acf-field', '', 0),
(174, 1, '2017-06-22 14:44:09', '2017-06-22 13:44:09', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc8bbec633\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Top', 'padding_top', 'publish', 'closed', 'closed', '', 'field_594bc8bbec635', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=174', 1, 'acf-field', '', 0),
(175, 1, '2017-06-22 14:44:09', '2017-06-22 13:44:09', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc8bbec633\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Bottom', 'padding_bottom', 'publish', 'closed', 'closed', '', 'field_594bc8bbec636', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=175', 2, 'acf-field', '', 0),
(176, 1, '2017-06-22 14:44:09', '2017-06-22 13:44:09', 'a:11:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:133:\"Click the blue button to add a team member. They are presented in a row of four. It\'s best to add four at a time to avoid whitespace.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc8bbec633\";s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:15:\"Add Team Member\";}', 'Team Member Content', 'team_member_content', 'publish', 'closed', 'closed', '', 'field_594bc914ec63f', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=176', 3, 'acf-field', '', 0),
(177, 1, '2017-06-22 14:44:09', '2017-06-22 13:44:09', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:35:\"Image Dimensions should be 400x400.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc8bbec633\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Team Member Image', 'team_member_image', 'publish', 'closed', 'closed', '', 'field_594bc8bbec637', '', '', '2017-06-29 11:32:58', '2017-06-29 10:32:58', '', 176, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=177', 0, 'acf-field', '', 0),
(178, 1, '2017-06-22 14:44:09', '2017-06-22 13:44:09', 'a:11:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"80\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594bc8bbec633\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:5;s:9:\"new_lines\";s:7:\"wpautop\";}', 'Team Member Info', 'team_member_info', 'publish', 'closed', 'closed', '', 'field_594bc8bbec638', '', '', '2017-06-27 16:59:13', '2017-06-27 15:59:13', '', 176, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=178', 1, 'acf-field', '', 0),
(188, 1, '2017-06-23 14:41:59', '2017-06-23 13:41:59', 'a:7:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:131:\"This changes the colour of the title when the image is hovered on. If no colour is chosen, the default value of black will be used.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"15\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:5:\"black\";}', 'Title Color', 'title_color', 'publish', 'closed', 'closed', '', 'field_594d1a6db8302', '', '', '2017-06-29 11:27:17', '2017-06-29 10:27:17', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=188', 3, 'acf-field', '', 0),
(193, 1, '2017-06-23 16:45:09', '2017-06-23 15:45:09', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:35:\"Image Dimensions should be 800x568.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5e23a2976\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Half Width Image One', 'case_study_hwi_one', 'publish', 'closed', 'closed', '', 'field_594d364acd8da', '', '', '2017-07-03 16:16:25', '2017-07-03 15:16:25', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=193', 0, 'acf-field', '', 0),
(194, 1, '2017-06-23 16:45:09', '2017-06-23 15:45:09', 'a:11:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:37:\"Enter the quote without speech marks.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"35\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5e9da2978\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Quote (Half Width)', 'case_study_quote', 'publish', 'closed', 'closed', '', 'field_594d368bcd8db', '', '', '2017-07-03 16:13:18', '2017-07-03 15:13:18', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=194', 1, 'acf-field', '', 0),
(205, 1, '2017-06-26 10:08:37', '2017-06-26 09:08:37', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:35:\"Image Dimensions should be 800x568.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5e23a2976\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Half Width Image Two', 'case_study_hwi_two', 'publish', 'closed', 'closed', '', 'field_5950ce730216e', '', '', '2017-07-03 16:16:25', '2017-07-03 15:16:25', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=205', 1, 'acf-field', '', 0),
(211, 1, '2017-06-26 10:13:07', '2017-06-26 09:13:07', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:35:\"Image Dimensions should be 800x568.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5e9da2978\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Half Width Image', 'case_study_hwi_three', 'publish', 'closed', 'closed', '', 'field_5950cfe2345de', '', '', '2017-07-03 16:16:25', '2017-07-03 15:16:25', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=211', 0, 'acf-field', '', 0),
(212, 1, '2017-06-26 10:17:39', '2017-06-26 09:17:39', 'a:11:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:34:\"E.g. Joe Smith, CEO Famous Company\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"15\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5e9da2978\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:8;s:9:\"new_lines\";s:0:\"\";}', 'Quote Author', 'case_study_quote_author', 'publish', 'closed', 'closed', '', 'field_5950d0fdd40d0', '', '', '2017-07-03 16:13:18', '2017-07-03 15:13:18', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=212', 2, 'acf-field', '', 0),
(217, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section ID', 'section_id', 'publish', 'closed', 'closed', '', 'field_59522086e7425', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=217', 0, 'acf-field', '', 0),
(218, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Top', 'padding_top', 'publish', 'closed', 'closed', '', 'field_59522086e7426', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=218', 1, 'acf-field', '', 0),
(219, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Bottom', 'padding_bottom', 'publish', 'closed', 'closed', '', 'field_59522086e7427', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=219', 2, 'acf-field', '', 0),
(220, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:19:\"Add Office Location\";}', 'Office Location Content', 'office_location_content', 'publish', 'closed', 'closed', '', 'field_595220a3e7437', '', '', '2017-06-29 15:20:22', '2017-06-29 14:20:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=220', 3, 'acf-field', '', 0),
(221, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Office Location Image', 'office_location_image', 'publish', 'closed', 'closed', '', 'field_59522086e7428', '', '', '2017-06-27 17:21:58', '2017-06-27 16:21:58', '', 220, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=221', 0, 'acf-field', '', 0),
(222, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:95:\"Copy and paste the google map url of the office location. E.g. https://goo.gl/maps/dNEAkPfZ5VA2\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Office Location Address Link', 'office_location_address_link', 'publish', 'closed', 'closed', '', 'field_59522086e7429', '', '', '2017-06-27 10:11:42', '2017-06-27 09:11:42', '', 220, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=222', 1, 'acf-field', '', 0),
(223, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:4;s:9:\"new_lines\";s:2:\"br\";}', 'Office Location Address Display', 'office_location_address_display', 'publish', 'closed', 'closed', '', 'field_59522086e742a', '', '', '2017-06-27 10:11:42', '2017-06-27 09:11:42', '', 220, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=223', 2, 'acf-field', '', 0),
(224, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:70:\"Enter the office phone number in full without spaces. E.g. 02071112222\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Office Location Tel Link', 'office_location_tel_link', 'publish', 'closed', 'closed', '', 'field_59522086e742b', '', '', '2017-06-27 10:11:42', '2017-06-27 09:11:42', '', 220, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=224', 3, 'acf-field', '', 0),
(225, 1, '2017-06-27 10:11:42', '2017-06-27 09:11:42', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:105:\"Enter the office phone number how you\'d like it to be displayed on the website.  E.g. +44 (0)20 7111 2222\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"59522086e7424\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Office Location Tel Display', 'office_location_tel_display', 'publish', 'closed', 'closed', '', 'field_59522086e742c', '', '', '2017-06-27 10:11:42', '2017-06-27 09:11:42', '', 220, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=225', 4, 'acf-field', '', 0),
(251, 1, '2017-06-27 10:42:52', '2017-06-27 09:42:52', '', 'city_9', '', 'inherit', 'closed', 'closed', '', 'city_9', '', '', '2017-06-27 10:42:52', '2017-06-27 09:42:52', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/city_9.jpg', 0, 'attachment', 'image/jpeg', 0),
(252, 1, '2017-06-27 10:42:53', '2017-06-27 09:42:53', '', 'nature_4', '', 'inherit', 'closed', 'closed', '', 'nature_4', '', '', '2017-06-27 10:42:53', '2017-06-27 09:42:53', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/nature_4.jpg', 0, 'attachment', 'image/jpeg', 0),
(253, 1, '2017-06-27 10:42:53', '2017-06-27 09:42:53', '', 'nightlife_4', '', 'inherit', 'closed', 'closed', '', 'nightlife_4', '', '', '2017-06-27 10:42:53', '2017-06-27 09:42:53', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/nightlife_4.jpg', 0, 'attachment', 'image/jpeg', 0),
(254, 1, '2017-06-27 10:43:06', '2017-06-27 09:43:06', '', 'TUC', '', 'inherit', 'closed', 'closed', '', 'tuc', '', '', '2017-06-27 10:43:06', '2017-06-27 09:43:06', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/TUC.jpg', 0, 'attachment', 'image/jpeg', 0),
(255, 1, '2017-06-27 10:43:09', '2017-06-27 09:43:09', '', 'BOI', '', 'inherit', 'closed', 'closed', '', 'boi', '', '', '2017-06-27 10:43:09', '2017-06-27 09:43:09', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/BOI.png', 0, 'attachment', 'image/png', 0),
(256, 1, '2017-06-27 10:43:20', '2017-06-27 09:43:20', '', 'Informa', '', 'inherit', 'closed', 'closed', '', 'informa', '', '', '2017-06-27 10:43:20', '2017-06-27 09:43:20', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/Informa.png', 0, 'attachment', 'image/png', 0),
(257, 1, '2017-06-27 10:43:24', '2017-06-27 09:43:24', '', 'warwick', '', 'inherit', 'closed', 'closed', '', 'warwick', '', '', '2017-06-27 10:43:24', '2017-06-27 09:43:24', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/warwick.jpg', 0, 'attachment', 'image/jpeg', 0),
(258, 1, '2017-06-27 10:43:26', '2017-06-27 09:43:26', '', 'manchester', '', 'inherit', 'closed', 'closed', '', 'manchester', '', '', '2017-06-27 10:43:26', '2017-06-27 09:43:26', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/manchester.jpg', 0, 'attachment', 'image/jpeg', 0),
(259, 1, '2017-06-27 10:43:28', '2017-06-27 09:43:28', '', 'wesleyan', '', 'inherit', 'closed', 'closed', '', 'wesleyan', '', '', '2017-06-27 10:43:28', '2017-06-27 09:43:28', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/wesleyan.png', 0, 'attachment', 'image/png', 0),
(260, 1, '2017-06-27 10:44:17', '2017-06-27 09:44:17', '', 'member-1_bw', '', 'inherit', 'closed', 'closed', '', 'member-1_bw', '', '', '2017-06-27 10:44:17', '2017-06-27 09:44:17', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/member-1_bw.jpg', 0, 'attachment', 'image/jpeg', 0),
(261, 1, '2017-06-27 10:44:17', '2017-06-27 09:44:17', '', 'member-2_bw', '', 'inherit', 'closed', 'closed', '', 'member-2_bw', '', '', '2017-06-27 10:44:17', '2017-06-27 09:44:17', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/member-2_bw.jpg', 0, 'attachment', 'image/jpeg', 0),
(262, 1, '2017-06-27 10:44:18', '2017-06-27 09:44:18', '', 'member-3_bw', '', 'inherit', 'closed', 'closed', '', 'member-3_bw', '', '', '2017-06-27 10:44:18', '2017-06-27 09:44:18', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/member-3_bw.jpg', 0, 'attachment', 'image/jpeg', 0),
(263, 1, '2017-06-27 10:44:19', '2017-06-27 09:44:19', '', 'member-4_bw', '', 'inherit', 'closed', 'closed', '', 'member-4_bw', '', '', '2017-06-27 10:44:19', '2017-06-27 09:44:19', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/member-4_bw.jpg', 0, 'attachment', 'image/jpeg', 0),
(264, 1, '2017-06-27 10:45:07', '2017-06-27 09:45:07', '', 'london', '', 'inherit', 'closed', 'closed', '', 'london', '', '', '2017-06-27 10:45:07', '2017-06-27 09:45:07', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/london.jpg', 0, 'attachment', 'image/jpeg', 0),
(265, 1, '2017-06-27 10:45:08', '2017-06-27 09:45:08', '', 'new-york', '', 'inherit', 'closed', 'closed', '', 'new-york', '', '', '2017-06-27 10:45:08', '2017-06-27 09:45:08', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/new-york.jpg', 0, 'attachment', 'image/jpeg', 0),
(266, 1, '2017-06-27 10:45:09', '2017-06-27 09:45:09', '', 'shanghai', '', 'inherit', 'closed', 'closed', '', 'shanghai', '', '', '2017-06-27 10:45:09', '2017-06-27 09:45:09', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/shanghai.jpg', 0, 'attachment', 'image/jpeg', 0),
(267, 1, '2017-06-27 10:47:24', '2017-06-27 09:47:24', '', 'tuc_1', '', 'inherit', 'closed', 'closed', '', 'tuc_1', '', '', '2017-06-27 10:47:24', '2017-06-27 09:47:24', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_1.png', 0, 'attachment', 'image/png', 0),
(268, 1, '2017-06-27 10:47:25', '2017-06-27 09:47:25', '', 'tuc_2', '', 'inherit', 'closed', 'closed', '', 'tuc_2', '', '', '2017-06-27 10:47:25', '2017-06-27 09:47:25', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_2.jpg', 0, 'attachment', 'image/jpeg', 0),
(269, 1, '2017-06-27 10:47:25', '2017-06-27 09:47:25', '', 'tuc_3', '', 'inherit', 'closed', 'closed', '', 'tuc_3', '', '', '2017-06-27 10:47:25', '2017-06-27 09:47:25', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_3.jpg', 0, 'attachment', 'image/jpeg', 0),
(270, 1, '2017-06-27 10:47:26', '2017-06-27 09:47:26', '', 'tuc_4', '', 'inherit', 'closed', 'closed', '', 'tuc_4', '', '', '2017-06-27 10:47:26', '2017-06-27 09:47:26', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_4.jpg', 0, 'attachment', 'image/jpeg', 0),
(271, 1, '2017-06-27 10:47:27', '2017-06-27 09:47:27', '', 'tuc_5', '', 'inherit', 'closed', 'closed', '', 'tuc_5', '', '', '2017-06-27 10:47:27', '2017-06-27 09:47:27', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_5.jpg', 0, 'attachment', 'image/jpeg', 0),
(272, 1, '2017-06-27 10:47:28', '2017-06-27 09:47:28', '', 'tuc_6', '', 'inherit', 'closed', 'closed', '', 'tuc_6', '', '', '2017-06-27 10:47:28', '2017-06-27 09:47:28', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_6.jpg', 0, 'attachment', 'image/jpeg', 0),
(273, 1, '2017-06-27 10:47:28', '2017-06-27 09:47:28', '', 'tuc_7', '', 'inherit', 'closed', 'closed', '', 'tuc_7', '', '', '2017-06-27 10:47:28', '2017-06-27 09:47:28', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_7.jpg', 0, 'attachment', 'image/jpeg', 0),
(274, 1, '2017-06-27 10:47:29', '2017-06-27 09:47:29', '', 'tuc_8', '', 'inherit', 'closed', 'closed', '', 'tuc_8', '', '', '2017-06-27 10:47:29', '2017-06-27 09:47:29', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_8.jpg', 0, 'attachment', 'image/jpeg', 0),
(275, 1, '2017-06-27 10:47:30', '2017-06-27 09:47:30', '', 'tuc_9', '', 'inherit', 'closed', 'closed', '', 'tuc_9', '', '', '2017-06-27 10:47:30', '2017-06-27 09:47:30', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/tuc_9.jpg', 0, 'attachment', 'image/jpeg', 0),
(278, 1, '2017-06-27 13:49:39', '2017-06-27 12:49:39', '', 'full-width', '', 'inherit', 'closed', 'closed', '', 'full-width', '', '', '2017-06-27 13:49:39', '2017-06-27 12:49:39', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/full-width.png', 0, 'attachment', 'image/png', 0),
(279, 1, '2017-06-27 13:54:10', '2017-06-27 12:54:10', '', 'slider', '', 'inherit', 'closed', 'closed', '', 'slider', '', '', '2017-06-27 13:54:10', '2017-06-27 12:54:10', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/slider.png', 0, 'attachment', 'image/png', 0),
(280, 1, '2017-06-27 13:56:54', '2017-06-27 12:56:54', '', 'case-study', '', 'inherit', 'closed', 'closed', '', 'case-study', '', '', '2017-06-27 13:56:54', '2017-06-27 12:56:54', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/case-study.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `ln2017_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(281, 1, '2017-06-27 16:42:02', '2017-06-27 15:42:02', 'a:7:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:120:\"This changes the background colour of the modal window. If no colour is chosen, the default value of white will be used.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"15\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:13:\"default_value\";s:7:\"#FFFFFF\";}', 'Modal Background Color', 'modal_bkg_color', 'publish', 'closed', 'closed', '', 'field_59527c7261cba', '', '', '2017-06-29 11:27:17', '2017-06-29 10:27:17', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=281', 5, 'acf-field', '', 0),
(282, 1, '2017-06-27 17:07:48', '2017-06-27 16:07:48', '', 'london_ldn', '', 'inherit', 'closed', 'closed', '', 'london_ldn', '', '', '2017-06-27 17:07:48', '2017-06-27 16:07:48', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/london_ldn.jpg', 0, 'attachment', 'image/jpeg', 0),
(283, 1, '2017-06-27 17:07:48', '2017-06-27 16:07:48', '', 'new-york_ny', '', 'inherit', 'closed', 'closed', '', 'new-york_ny', '', '', '2017-06-27 17:07:48', '2017-06-27 16:07:48', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/new-york_ny.jpg', 0, 'attachment', 'image/jpeg', 0),
(284, 1, '2017-06-27 17:07:49', '2017-06-27 16:07:49', '', 'shanghai_sh', '', 'inherit', 'closed', 'closed', '', 'shanghai_sh', '', '', '2017-06-27 17:07:49', '2017-06-27 16:07:49', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/shanghai_sh.jpg', 0, 'attachment', 'image/jpeg', 0),
(286, 1, '2017-06-28 09:38:46', '2017-06-28 08:38:46', '', 'offer_graphic', '', 'inherit', 'closed', 'closed', '', 'offer_graphic', '', '', '2017-06-28 09:38:46', '2017-06-28 08:38:46', '', 0, 'http://localhost:8888/lloydnorthover/wp-content/uploads/offer_graphic.png', 0, 'attachment', 'image/png', 0),
(287, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Section ID', 'section_id', 'publish', 'closed', 'closed', '', 'field_595379b2268ed', '', '', '2017-06-28 10:44:22', '2017-06-28 09:44:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=287', 0, 'acf-field', '', 0),
(288, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Top', 'padding_top', 'publish', 'closed', 'closed', '', 'field_595379b2268ee', '', '', '2017-06-28 10:44:22', '2017-06-28 09:44:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=288', 1, 'acf-field', '', 0),
(289, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Padding Bottom', 'padding_bottom', 'publish', 'closed', 'closed', '', 'field_595379b2268ef', '', '', '2017-06-28 10:44:22', '2017-06-28 09:44:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&p=289', 2, 'acf-field', '', 0),
(290, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Offer Content Left', 'offer_content_left', 'publish', 'closed', 'closed', '', 'field_595379b2268f3', '', '', '2017-06-29 10:47:46', '2017-06-29 09:47:46', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=290', 3, 'acf-field', '', 0),
(291, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:7:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:7:\"#FFFFFF\";}', 'Modal Background Color One', 'modal_bkg_color_one', 'publish', 'closed', 'closed', '', 'field_595379b2268f6', '', '', '2017-06-29 14:59:34', '2017-06-29 13:59:34', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=291', 7, 'acf-field', '', 0),
(292, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Modal Content Left One', 'modal_content_left_one', 'publish', 'closed', 'closed', '', 'field_595379b2268f2', '', '', '2017-06-29 14:59:34', '2017-06-29 13:59:34', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=292', 8, 'acf-field', '', 0),
(293, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Modal Content Right One', 'modal_content_right_one', 'publish', 'closed', 'closed', '', 'field_59537a3226904', '', '', '2017-06-29 14:59:34', '2017-06-29 13:59:34', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=293', 9, 'acf-field', '', 0),
(294, 1, '2017-06-28 10:44:22', '2017-06-28 09:44:22', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Offer Content Right - First', 'offer_content_right_first', 'publish', 'closed', 'closed', '', 'field_59537a4426905', '', '', '2017-06-29 10:47:46', '2017-06-29 09:47:46', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=294', 4, 'acf-field', '', 0),
(296, 1, '2017-06-28 16:26:30', '2017-06-28 15:26:30', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Offer ID One', 'offer_id_one', 'publish', 'closed', 'closed', '', 'field_5953ca8a91a50', '', '', '2017-06-29 14:59:34', '2017-06-29 13:59:34', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=296', 6, 'acf-field', '', 0),
(297, 1, '2017-06-28 16:39:14', '2017-06-28 15:39:14', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Offer ID Two', 'offer_id_two', 'publish', 'closed', 'closed', '', 'field_5953cd88a0e5a', '', '', '2017-06-29 14:59:34', '2017-06-29 13:59:34', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=297', 10, 'acf-field', '', 0),
(298, 1, '2017-06-28 17:27:52', '2017-06-28 16:27:52', 'a:7:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:7:\"#FFFFFF\";}', 'Modal Background Color Two', 'modal_bkg_color_two', 'publish', 'closed', 'closed', '', 'field_5953d8aaa8baa', '', '', '2017-06-29 14:59:34', '2017-06-29 13:59:34', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=298', 11, 'acf-field', '', 0),
(299, 1, '2017-06-28 17:27:52', '2017-06-28 16:27:52', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Modal Content Two', 'modal_content_two', 'publish', 'closed', 'closed', '', 'field_5953d8c5a8bab', '', '', '2017-06-30 10:33:15', '2017-06-30 09:33:15', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=299', 12, 'acf-field', '', 0),
(301, 1, '2017-06-29 09:31:04', '2017-06-29 08:31:04', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Offer ID Three', 'offer_id_three', 'publish', 'closed', 'closed', '', 'field_5954ba7f657f9', '', '', '2017-06-30 10:33:15', '2017-06-30 09:33:15', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=301', 14, 'acf-field', '', 0),
(302, 1, '2017-06-29 09:31:04', '2017-06-29 08:31:04', 'a:7:{s:4:\"type\";s:12:\"color_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:7:\"#FFFFFF\";}', 'Modal Background Color Three', 'modal_bkg_color_three', 'publish', 'closed', 'closed', '', 'field_5954ba6e657f8', '', '', '2017-06-30 10:33:15', '2017-06-30 09:33:15', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=302', 15, 'acf-field', '', 0),
(303, 1, '2017-06-29 09:31:04', '2017-06-29 08:31:04', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Modal Content Three Left', 'modal_content_three_left', 'publish', 'closed', 'closed', '', 'field_5954ba61657f7', '', '', '2017-06-30 10:33:15', '2017-06-30 09:33:15', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=303', 17, 'acf-field', '', 0),
(304, 1, '2017-06-29 09:31:04', '2017-06-29 08:31:04', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Modal Content Three Right', 'modal_content_three_right', 'publish', 'closed', 'closed', '', 'field_5954ba56657f6', '', '', '2017-06-30 10:33:15', '2017-06-30 09:33:15', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=304', 18, 'acf-field', '', 0),
(305, 1, '2017-06-29 10:47:46', '2017-06-29 09:47:46', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Offer Content Right - Second', 'offer_content_right_second', 'publish', 'closed', 'closed', '', 'field_5954cc4087fea', '', '', '2017-06-29 10:49:09', '2017-06-29 09:49:09', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=305', 5, 'acf-field', '', 0),
(306, 1, '2017-06-29 15:20:22', '2017-06-29 14:20:22', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:15:\"Must be 800x568\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image One', 'mcs_image_one', 'publish', 'closed', 'closed', '', 'field_59550c72b9450', '', '', '2017-06-30 15:15:22', '2017-06-30 14:15:22', '', 313, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=306', 3, 'acf-field', '', 0),
(307, 1, '2017-06-29 15:20:22', '2017-06-29 14:20:22', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:15:\"Must be 800x568\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Two', 'mcs_image_two', 'publish', 'closed', 'closed', '', 'field_59550c8cb9451', '', '', '2017-06-30 15:15:22', '2017-06-30 14:15:22', '', 313, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=307', 4, 'acf-field', '', 0),
(311, 1, '2017-06-29 17:04:00', '2017-06-29 16:04:00', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:3:\"100\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Modal Content Three Intro', 'modal_content_three_intro', 'publish', 'closed', 'closed', '', 'field_595524d492611', '', '', '2017-06-30 10:33:15', '2017-06-30 09:33:15', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=311', 16, 'acf-field', '', 0),
(312, 1, '2017-06-29 17:04:59', '2017-06-29 16:04:59', '', 'brand-purpose-story', '', 'inherit', 'closed', 'closed', '', 'brand-purpose-story', '', '', '2017-06-29 17:04:59', '2017-06-29 16:04:59', '', 2, 'http://localhost:8888/lloydnorthover/wp-content/uploads/brand-purpose-story.png', 0, 'attachment', 'image/png', 0),
(313, 1, '2017-06-30 10:18:30', '2017-06-30 09:18:30', 'a:11:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Mini Case Study Info', 'mini_case_study_info', 'publish', 'closed', 'closed', '', 'field_5956168f5882d', '', '', '2017-06-30 15:15:22', '2017-06-30 14:15:22', '', 11, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=313', 13, 'acf-field', '', 0),
(314, 1, '2017-06-30 10:18:30', '2017-06-30 09:18:30', 'a:11:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"40\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Description', 'mcs_description', 'publish', 'closed', 'closed', '', 'field_595616c25882e', '', '', '2017-06-30 15:15:22', '2017-06-30 14:15:22', '', 313, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=314', 2, 'acf-field', '', 0),
(315, 1, '2017-06-30 10:18:30', '2017-06-30 09:18:30', 'a:16:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:15:\"Must be 800x568\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:4:\"full\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Three', 'mcs_image_three', 'publish', 'closed', 'closed', '', 'field_5956172a58830', '', '', '2017-06-30 15:15:22', '2017-06-30 14:15:22', '', 313, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=315', 5, 'acf-field', '', 0),
(316, 1, '2017-06-30 15:11:54', '2017-06-30 14:11:54', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'mcs_title', 'publish', 'closed', 'closed', '', 'field_59565bdfb42bb', '', '', '2017-06-30 15:15:22', '2017-06-30 14:15:22', '', 313, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=316', 0, 'acf-field', '', 0),
(317, 1, '2017-06-30 15:11:54', '2017-06-30 14:11:54', 'a:11:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595379b2268ec\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Subtitle', 'mcs_subtitle', 'publish', 'closed', 'closed', '', 'field_59565c08b42bc', '', '', '2017-06-30 15:15:22', '2017-06-30 14:15:22', '', 313, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=317', 1, 'acf-field', '', 0),
(319, 1, '2017-07-03 10:51:32', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2017-07-03 10:51:32', '0000-00-00 00:00:00', '', 0, 'http://localhost:8888/lloydnorthover/?p=319', 0, 'post', '', 0),
(321, 1, '2017-07-03 14:40:52', '2017-07-03 13:40:52', 'a:8:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:86:\"Copy and paste the YouTube video link.\r\ne.g. https://www.youtube.com/embed/OPf0YbXqDm0\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:3:\"100\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5cada3651\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'YouTube Video', 'case_study_youtube_video', 'publish', 'closed', 'closed', '', 'field_595a48f6e72d1', '', '', '2017-07-03 16:05:09', '2017-07-03 15:05:09', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=321', 0, 'acf-field', '', 0),
(322, 1, '2017-07-03 15:39:06', '2017-07-03 14:39:06', 'a:8:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:82:\"Copy and paste the Vimeo video link.\r\ne.g. https://player.vimeo.com/video/22439234\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:3:\"100\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"595a5db6a2971\";s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Vimeo Video', 'case_study_vimeo_video', 'publish', 'closed', 'closed', '', 'field_595a56eaea12c', '', '', '2017-07-03 16:13:18', '2017-07-03 15:13:18', '', 323, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=322', 0, 'acf-field', '', 0),
(323, 1, '2017-07-03 16:05:08', '2017-07-03 15:05:08', 'a:10:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:3:\"100\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:13:\"594a818a79f5c\";s:7:\"layouts\";a:5:{s:13:\"595a5cada3651\";a:6:{s:3:\"key\";s:13:\"595a5cada3651\";s:5:\"label\";s:13:\"YouTube Video\";s:4:\"name\";s:24:\"case_study_youtube_video\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"595a5db6a2971\";a:6:{s:3:\"key\";s:13:\"595a5db6a2971\";s:5:\"label\";s:11:\"Vimeo Video\";s:4:\"name\";s:22:\"case_study_vimeo_video\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"595a5df5a2973\";a:6:{s:3:\"key\";s:13:\"595a5df5a2973\";s:5:\"label\";s:16:\"Full Width Image\";s:4:\"name\";s:14:\"case_study_fwi\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"595a5e23a2976\";a:6:{s:3:\"key\";s:13:\"595a5e23a2976\";s:5:\"label\";s:17:\"Half Width Images\";s:4:\"name\";s:14:\"case_study_hwi\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}s:13:\"595a5e9da2978\";a:6:{s:3:\"key\";s:13:\"595a5e9da2978\";s:5:\"label\";s:15:\"Image and Quote\";s:4:\"name\";s:22:\"case_study_image_quote\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:22:\"Add Case Study Content\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}', 'Case Study Content', 'case_study_content', 'publish', 'closed', 'closed', '', 'field_595a5c9476e73', '', '', '2017-07-03 16:16:25', '2017-07-03 15:16:25', '', 140, 'http://localhost:8888/lloydnorthover/?post_type=acf-field&#038;p=323', 8, 'acf-field', '', 0),
(324, 1, '2017-07-04 15:24:35', '2017-07-04 14:24:35', '', 'Home', '', 'inherit', 'closed', 'closed', '', '2-autosave-v1', '', '', '2017-07-04 15:24:35', '2017-07-04 14:24:35', '', 2, 'http://localhost:8888/lloydnorthover/2-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_termmeta`
--

CREATE TABLE `ln2017_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_terms`
--

CREATE TABLE `ln2017_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ;

--
-- Dumping data for table `ln2017_terms`
--

INSERT INTO `ln2017_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_term_relationships`
--

CREATE TABLE `ln2017_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ;

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_term_taxonomy`
--

CREATE TABLE `ln2017_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ;

--
-- Dumping data for table `ln2017_term_taxonomy`
--

INSERT INTO `ln2017_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_usermeta`
--

CREATE TABLE `ln2017_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext
) ;

--
-- Dumping data for table `ln2017_usermeta`
--

INSERT INTO `ln2017_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Joe'),
(2, 1, 'first_name', 'Joseph'),
(3, 1, 'last_name', 'Sayegh'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'false'),
(10, 1, 'locale', ''),
(11, 1, 'ln2017_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(12, 1, 'ln2017_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '0'),
(16, 1, 'ln2017_dashboard_quick_press_last_post_id', '319'),
(17, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:2:\"::\";}'),
(18, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(19, 1, 'metaboxhidden_dashboard', 'a:3:{i:0;s:18:\"dashboard_activity\";i:1;s:21:\"dashboard_quick_press\";i:2;s:17:\"dashboard_primary\";}'),
(20, 1, 'acf_user_settings', 'a:0:{}'),
(21, 1, 'ln2017_user-settings', 'editor=html&hidetb=1&mfold=o&libraryContent=browse&imgsize=full&align=center'),
(22, 1, 'ln2017_user-settings-time', '1498667614'),
(23, 1, 'meta-box-order_page', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:36:\"submitdiv,pageparentdiv,postimagediv\";s:6:\"normal\";s:41:\"acf-group_59479e8a50c6b,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}'),
(24, 1, 'screen_layout_page', '1'),
(25, 1, 'meta-box-order_acf-field-group', 'a:3:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:80:\"acf-field-group-fields,acf-field-group-locations,acf-field-group-options,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(26, 1, 'screen_layout_acf-field-group', '1'),
(27, 2, 'nickname', 'darren.baker'),
(28, 2, 'first_name', 'Darren'),
(29, 2, 'last_name', 'Baker'),
(30, 2, 'description', ''),
(31, 2, 'rich_editing', 'true'),
(32, 2, 'comment_shortcuts', 'false'),
(33, 2, 'admin_color', 'fresh'),
(34, 2, 'use_ssl', '0'),
(35, 2, 'show_admin_bar_front', 'true'),
(36, 2, 'locale', ''),
(37, 2, 'ln2017_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(38, 2, 'ln2017_user_level', '10'),
(39, 2, 'dismissed_wp_pointers', ''),
(40, 3, 'nickname', 'jeremy.shaw'),
(41, 3, 'first_name', 'Jeremy'),
(42, 3, 'last_name', 'Shaw'),
(43, 3, 'description', ''),
(44, 3, 'rich_editing', 'true'),
(45, 3, 'comment_shortcuts', 'false'),
(46, 3, 'admin_color', 'fresh'),
(47, 3, 'use_ssl', '0'),
(48, 3, 'show_admin_bar_front', 'true'),
(49, 3, 'locale', ''),
(50, 3, 'ln2017_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(51, 3, 'ln2017_user_level', '10'),
(52, 3, 'dismissed_wp_pointers', ''),
(53, 4, 'nickname', 'simon.ward'),
(54, 4, 'first_name', 'Simon'),
(55, 4, 'last_name', 'Ward'),
(56, 4, 'description', ''),
(57, 4, 'rich_editing', 'true'),
(58, 4, 'comment_shortcuts', 'false'),
(59, 4, 'admin_color', 'fresh'),
(60, 4, 'use_ssl', '0'),
(61, 4, 'show_admin_bar_front', 'true'),
(62, 4, 'locale', ''),
(63, 4, 'ln2017_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(64, 4, 'ln2017_user_level', '10'),
(65, 4, 'dismissed_wp_pointers', ''),
(66, 1, 'session_tokens', 'a:7:{s:64:\"80f1be4907574033bd2fdfae98b8d49700a95d63c795e959eb888bc8b8f2967b\";a:4:{s:10:\"expiration\";i:1499246137;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:117:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4\";s:5:\"login\";i:1499073337;}s:64:\"6f4a85f135580469d0599d636f3cb63c00a1a090177aecd82f3235c1d6c7790e\";a:4:{s:10:\"expiration\";i:1499266767;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36\";s:5:\"login\";i:1499093967;}s:64:\"d5017c7b7e0f3fcf619d493fe89f6f23467faa95de8beef35f40858f7a78d951\";a:4:{s:10:\"expiration\";i:1499268474;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:117:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4\";s:5:\"login\";i:1499095674;}s:64:\"efc7760f85194fee5f66845472e0e8269fdf9dbc557b7f5477519fa9f3cb3d92\";a:4:{s:10:\"expiration\";i:1499271525;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36\";s:5:\"login\";i:1499098725;}s:64:\"33f19fa0c3ceaa3292a7a8713b6e313c9a61a14e2f98a32abf5d512eccde6d2c\";a:4:{s:10:\"expiration\";i:1499344057;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36\";s:5:\"login\";i:1499171257;}s:64:\"054a6e68eee9665510317ae03b367ec2ef01ee3e03dff387909a1246081257b2\";a:4:{s:10:\"expiration\";i:1499350996;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36\";s:5:\"login\";i:1499178196;}s:64:\"219b6a0d39a2c27fb0c3f5fc23bf90590dd3b392834aa8e41f618e7f08e35fae\";a:4:{s:10:\"expiration\";i:1499417384;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:117:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/603.2.4 (KHTML, like Gecko) Version/10.1.1 Safari/603.2.4\";s:5:\"login\";i:1499244584;}}'),
(67, 1, 'last_login_time', '2017-07-05 09:49:44'),
(68, 5, 'nickname', 'matt.klippel'),
(69, 5, 'first_name', 'Matt'),
(70, 5, 'last_name', 'Klippel'),
(71, 5, 'description', ''),
(72, 5, 'rich_editing', 'true'),
(73, 5, 'comment_shortcuts', 'false'),
(74, 5, 'admin_color', 'fresh'),
(75, 5, 'use_ssl', '0'),
(76, 5, 'show_admin_bar_front', 'true'),
(77, 5, 'locale', ''),
(78, 5, 'ln2017_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(79, 5, 'ln2017_user_level', '10'),
(80, 5, 'dismissed_wp_pointers', ''),
(81, 6, 'nickname', 'richard.hockey'),
(82, 6, 'first_name', 'Richard'),
(83, 6, 'last_name', 'Hockey'),
(84, 6, 'description', ''),
(85, 6, 'rich_editing', 'true'),
(86, 6, 'comment_shortcuts', 'false'),
(87, 6, 'admin_color', 'fresh'),
(88, 6, 'use_ssl', '0'),
(89, 6, 'show_admin_bar_front', 'true'),
(90, 6, 'locale', ''),
(91, 6, 'ln2017_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(92, 6, 'ln2017_user_level', '10'),
(93, 6, 'dismissed_wp_pointers', '');

-- --------------------------------------------------------

--
-- Table structure for table `ln2017_users`
--

CREATE TABLE `ln2017_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ;

--
-- Dumping data for table `ln2017_users`
--

INSERT INTO `ln2017_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'joesayegh', '$P$B6TmEkhj7UJcNmHiW9wzuPUYkFTIBG.', 'joesayegh', 'joesayegh@square-solve.com', '', '2017-06-19 09:09:24', '', 0, 'Joe'),
(2, 'darren.baker', '$P$BMfc2l62Epv5ZCjOo0.hRdn6gcNaDu0', 'darren-baker', 'darren@lloydnorthover.com', '', '2017-07-03 08:53:29', '', 0, 'Darren Baker'),
(3, 'jeremy.shaw', '$P$BFq4LbkCV0Ls8ARVFgYaMdz4rsQxRu0', 'jeremy-shaw', 'jeremy.shaw@lloydnorthover.com', '', '2017-07-03 08:55:04', '', 0, 'Jeremy Shaw'),
(4, 'simon.ward', '$P$BRNFGI2SoWHbuoJzPHDaxZ6jVdsJIZ0', 'simon-ward', 'simon.ward@lloydnorthover.com', '', '2017-07-03 08:55:36', '', 0, 'Simon Ward'),
(5, 'matt.klippel', '$P$BRG3LLqjDbEqz9zmoMbqwCotBQ6jH2.', 'matt-klippel', 'matt.klippel@stackworks.com', '', '2017-07-05 08:50:57', '', 0, 'Matt Klippel'),
(6, 'richard.hockey', '$P$Bn.x/5QMShjHPW2BiSdw3OwPh5hx7J1', 'richard-hockey', 'richard.hockey@stackworks.com', '', '2017-07-05 08:52:48', '', 0, 'Richard Hockey');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ln2017_aiowps_events`
--
ALTER TABLE `ln2017_aiowps_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ln2017_aiowps_failed_logins`
--
ALTER TABLE `ln2017_aiowps_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ln2017_aiowps_global_meta`
--
ALTER TABLE `ln2017_aiowps_global_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `ln2017_aiowps_login_activity`
--
ALTER TABLE `ln2017_aiowps_login_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ln2017_aiowps_login_lockdown`
--
ALTER TABLE `ln2017_aiowps_login_lockdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ln2017_aiowps_permanent_block`
--
ALTER TABLE `ln2017_aiowps_permanent_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ln2017_commentmeta`
--
ALTER TABLE `ln2017_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ln2017_comments`
--
ALTER TABLE `ln2017_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `ln2017_links`
--
ALTER TABLE `ln2017_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `ln2017_options`
--
ALTER TABLE `ln2017_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `ln2017_postmeta`
--
ALTER TABLE `ln2017_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ln2017_posts`
--
ALTER TABLE `ln2017_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `ln2017_termmeta`
--
ALTER TABLE `ln2017_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ln2017_terms`
--
ALTER TABLE `ln2017_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `ln2017_term_relationships`
--
ALTER TABLE `ln2017_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `ln2017_term_taxonomy`
--
ALTER TABLE `ln2017_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `ln2017_usermeta`
--
ALTER TABLE `ln2017_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `ln2017_users`
--
ALTER TABLE `ln2017_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ln2017_aiowps_events`
--
ALTER TABLE `ln2017_aiowps_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_aiowps_failed_logins`
--
ALTER TABLE `ln2017_aiowps_failed_logins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_aiowps_global_meta`
--
ALTER TABLE `ln2017_aiowps_global_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_aiowps_login_activity`
--
ALTER TABLE `ln2017_aiowps_login_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_aiowps_login_lockdown`
--
ALTER TABLE `ln2017_aiowps_login_lockdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_aiowps_permanent_block`
--
ALTER TABLE `ln2017_aiowps_permanent_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_commentmeta`
--
ALTER TABLE `ln2017_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_comments`
--
ALTER TABLE `ln2017_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_links`
--
ALTER TABLE `ln2017_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_options`
--
ALTER TABLE `ln2017_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_postmeta`
--
ALTER TABLE `ln2017_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_posts`
--
ALTER TABLE `ln2017_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_termmeta`
--
ALTER TABLE `ln2017_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_terms`
--
ALTER TABLE `ln2017_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_term_taxonomy`
--
ALTER TABLE `ln2017_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_usermeta`
--
ALTER TABLE `ln2017_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ln2017_users`
--
ALTER TABLE `ln2017_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
