Lloyd Northover
===============

This is for the new Lloyd Northover website. It's been built on a starter theme called `_s`, or [`underscores`](http://underscores.me). This is the [Sass](http://sass-lang.com) version.

### Development dependancies
* [MAMP](https://www.mamp.info/en/)
* [Node.js](https://nodejs.org/en/)
* [NPM](https://nodejs.org/en/download/)
* [Gulp](http://gulpjs.com)