=== WP Eventbrite Embedded Checkout ===

Contributors: hendcorp
Plugin Name: WP Eventbrite Embedded Checkout
Plugin URI: https://wpeec.pro/
Tags: events, checkout, eventbrite, form
Author: Hendra Setiawan
Donate link: https://www.paypal.me/hendcorp
Requires at least: 4.4
Tested up to: 5.3
Requires PHP: 5.6
Stable tag: 2.0.0
Version: 2.0.0
License: GNU Version 2 or Any Later Version

== Description ==

Allows people to buy Eventbrite tickets without leaving your website. Sell tickets right from your WordPress site!

**DEMO**
Popup Mode : [https://wpeec.pro/demo-popup-mode/](https://wpeec.pro/demo-popup-mode/)
Embed Mode : [https://wpeec.pro/demo-embedded-mode/](https://wpeec.pro/demo-embedded-mode/)

**MAIN FEATURES**
1. Embedded the Eventbrite checkout form on the page with your content
2. Add a button that opens the Eventbrite checkout modal over your content
3. Easy to use. You just need to copy-paste the shortcode to any post or page on your website

**PRO VERSION FEATURES**
1. Multiple Events
2. Shortcode Generator
3. Priority Support

Interested? Visit our website : [https://wpeec.pro/](https://wpeec.pro/)

**REQUIREMENTS**
1. SSL Certificate
2. Eventbrite event ID

== Installation ==

1. Activate the plugin
2. Go to Eventbrite Form
3. Insert your Eventbrite Event ID
4. Choose how the checkout form appears
5. Save Setting
6. Copy the shortcode and paste it on any post or page on your website

== Frequently Asked Questions ==

= Is it official plugin from Eventbrite? =

No, this plugin is not official.

= Is an SSL certificate required? =

Yes, in order to embed the Eventbrite checkout form on your WordPress website, you need to have SSL certificate (https). Otherwise, it won't works.

= How to find the event ID? =

Please see this official instruction from Eventbrite. [How to Find the Event ID](https://www.eventbrite.com/support/articles/en_US/How_To/how-to-find-the-event-id?lg=en_US)

== Screenshots ==

1. WP Eventbrite Embedded Checkout Settings page

2. Embedded Mode

3. Popup Mode

4. Working with Gutenberg

5. Buy Tickets button


== Changelog ==

= 2.0.0 =
*Release Date - 10 Nov 2019*

* Compatibility check to WordPress 5.3

= 1.0.3 =
*Release Date - 8 Nov 2019*

* Compatibility check to WordPress 5.2.4

= 1.0.2 =
*Release Date - 1 May 2019*

* Compatibility check to WordPress 5.2

= 1.0.1 =
*Release Date - 28 February 2019*

* Added functionality to set custom height on Embed mode
* Added functionality to set custom button text on Popup mode

= 1.0.0 =
*Release Date - 29 January 2019*

* First version
