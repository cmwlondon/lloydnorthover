<?php
/**
* WP Eventbrite Embedded Checkout - Admin Functions
*
* In this file,
* you will find all functions related to the plugin settings in WP-Admin area.
*
* @author 	Hendra Setiawan
* @version 	1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function wpeec_admin_js($hook) {
	if( 'toplevel_page_eventbrite-embedded-checkout' != $hook ) {
		return;
	}
  wp_enqueue_script('wpeec_admin_js_file', plugin_dir_url(__FILE__) . 'js/wpeec-admin.js');
}
add_action('admin_enqueue_scripts', 'wpeec_admin_js');

add_action( 'admin_menu', 'wpeec_admin_menu' );
function wpeec_admin_menu() {
	add_menu_page(__('Eventbrite Form','wpeec'), __('Eventbrite Form','wpeec'), 'manage_options', 'eventbrite-embedded-checkout', 'wpeec_toplevel_page', 'dashicons-cart', 79 );
}

function wpeec_register_settings() {
    register_setting('wpeec-settings-group', 'wpeec-event-id');
    register_setting('wpeec-settings-group', 'wpeec-event-form-mode');
		register_setting('wpeec-settings-group', 'wpeec-frame-height');
		register_setting('wpeec-settings-group', 'wpeec-button-text');
}
add_action('admin_init', 'wpeec_register_settings');

function wpeec_toplevel_page() {
	// Permission check
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	// SSL Check
	if( !is_ssl() ):
		wp_die( __( 'To use Eventbrite Embedded Checkout, your website has to follow the latest security standards and must serve pages over HTTPS encryption. <a href="https://www.eventbrite.com/support/articleredirect?anum=41024" target="_blank">Learn more</a>' ) );
	endif;


	// Get the embed frame height
	if( get_option('wpeec-frame-height') ) {
		$wpeecFrameHeight = get_option('wpeec-frame-height'); }
	else {
		$wpeecFrameHeight = 425; }

	// Button Text
	if( get_option('wpeec-button-text') ) {
		$wpeecButtonText = get_option('wpeec-button-text'); }
	else {
		$wpeecButtonText = 'Buy Tickets'; }
?>
<div class="wpwrap">
	<div class="card">
	<a href="https://wpeec.pro/downloads/wpeec-pro/" target="_blank" style="text-decoration: none;"><img src="<?php echo plugin_dir_url(__FILE__).'../img/banner.png'; ?>" style="width: 100%; margin-top: 10px;"></a>		
	<h1 style="padding-top: 15px; text-align: center; border-top: dashed 2px #ddd;"><?php _e('WP Eventbrite Embedded Checkout','wpeec'); ?></h1>
	<hr />
		<div class="form-wrap">
			<form method="post" action="options.php">
				<?php settings_fields('wpeec-settings-group'); ?>
				<?php do_settings_sections('wpeec-settings-group'); ?>
				<div class="form-field wpeec-event-id-wrap">
					<h3>Set The Event ID</h3>
					<label for="wpeec-event-id" style="font-weight: bold;">Event ID</label>
					<input type="text" style="width: 100%;" value="<?php echo get_option('wpeec-event-id'); ?>" name="wpeec-event-id">
					<p>How to find your event ID? Please check this <a href="https://www.eventbrite.com/support/articles/en_US/How_To/how-to-find-the-event-id?lg=en_US" target="_blank">documentation</a>.</p>
				</div>
				<div class="form-field wpeec-form-mode-wrap">
					<h3>Choose How Checkout Appears</h3>
					<input type="radio" <?php if (get_option('wpeec-event-form-mode') == 'modal'): ?>checked="checked"<?php endif; ?> value="modal" id="wpeec-event-form-mode-modal" name="wpeec-event-form-mode">
          <label for="wpeec-event-form-mode-modal" style="display: inline-block;"><?php _e('A button that opens the checkout modal over your content', 'wpeec') ?></label>
          <br />
          <input type="radio" <?php if (get_option('wpeec-event-form-mode') == 'embed' || get_option('wpeec-event-form-mode') == ''): ?>checked="checked"<?php endif; ?> value="embed" id="wpeec-event-form-mode-embed" name="wpeec-event-form-mode">
          <label for="wpeec-event-form-mode-embed" style="display: inline-block;"><?php _e('Embedded on the page with your content', 'wpeec') ?></label>
					<div class="wpeec-frame-height-wrapper" style="display: none; padding: 15px; margin-top: 10px; border: 1px solid #eee; border-radius: 5px;">
					 	  <label for="wpeec-frame-height" style="font-weight: bold; display: inline-block;">Height </label>
							<input type="number" name="wpeec-frame-height" id="wpeec-frame-height" value="<?php echo $wpeecFrameHeight; ?>" style="width: 80px;"> <strong>px</strong>
					</div>
					<div class="wpeec-button-text-wrapper" style="display: none; padding: 15px; margin-top: 10px; border: 1px solid #eee; border-radius: 5px;">
							<label for="wpeec-button-text" style="font-weight: bold; display: inline-block;">Button Text </label>
							<input type="text" name="wpeec-button-text" id="wpeec-button-text" value="<?php echo $wpeecButtonText; ?>" style="width: 200px;">
					</div>
				</div>
				<hr />
				<h3>Shortcode</h3>
				<?php
				$eid = get_option('wpeec-event-id');
				if($eid){
					echo '<p>Copy and paste this shortcode directly into any post or page.</p>';
					echo '<textarea style="width: 100%;" readonly="readonly">[wp-eventbrite-checkout]</textarea>';
				} else {
					echo '<p style="color: red;">Problem detected! Please set your Event ID.</p>';
				}
				?>
				<?php submit_button('Save Settings'); ?>
			</form>
			<hr />
			<p style="text-align: center;">WP Eventbrite Embedded Checkout. Made with <span class="dashicons dashicons-heart"></span> by <a href="https://hellohendra.com" target="_blank">Hendra</a>.</p>
		</div>
	</div>
</div>
<?php
}
