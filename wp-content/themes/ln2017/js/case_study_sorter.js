// acf java script events
// https://www.advancedcustomfields.com/resources/adding-custom-javascript-fields/

// run code with jQuery as '$'
(function($) {

	function SelectOrderer(parameters) {
		this.parameters = parameters;

		this.options = parameters.options;
		this.selectRegex = parameters.selectRegex;

		this.orderFields = [],
		this.orderFieldCount = 0;

		this.init();
	}

	SelectOrderer.prototype = {
		"constructor" : SelectOrderer,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			console.log('case_study_sorter.js => SelectOrderer');

			this.indexCaseStudies();
			this.populate();
		},
		"indexCaseStudies" : function () {
			var that = this;

			// get case_study_order field IDs and Values
			$('select').each(function(){
				var id = $(this).attr('id');
				
				if( typeof id !== 'undefined' ) {
					var matches = id.match(that.selectRegex);
					if ( matches ) {
						that.orderFields.push({
							"id" : matches[0],
							"value" : $(this).find('option:selected').val()
						});
					}
				}
			});

			this.orderFieldCount = this.orderFields.length;
			// console.log(this.orderFields);
		},
		"populate" : function () {
			var that = this;

			if ( this.orderFieldCount > 0) {
				// console.log(orderFields);

				// rebuild fields based on number of case studies available	
				var fieldIndex = 0,
					thisField;
					
				do {
					thisField = this.orderFields[fieldIndex];

					// automatically set position drop down if a position hasn't already been set
					if ( parseInt(thisField.value,10) === 0 ) {
						thisField.value = fieldIndex + 1;
					}

					var optionIndex = 0,
						fieldNode = $('#' + thisField.id);

					// clear select object		
					fieldNode.empty();

					// build feault 'NULL' option 'Select Position'
					var newOption = $('<option></option>').attr({"value" : '0'}).html('Place at bottom of list');
					fieldNode.append(newOption);

					// build position optipons based on number of case studies
					do {
						var offsetOptionValue = (optionIndex + 1);
						var newOption = $('<option></option>').attr({"value" : offsetOptionValue}).html(optionCaptions[optionIndex]);

						// set selected position in dropdown
						if ( parseInt(thisField.value, 10) === offsetOptionValue ) {
							newOption.attr({"selected" : true});
						}

						fieldNode.append(newOption);
						optionIndex++;
					} while (optionIndex < this.orderFieldCount)
					
					fieldIndex++;
				} while (fieldIndex < this.orderFieldCount)
			}

			/*
			orderFields.map(function(field,index) {
				var optionIndex = 0,
					fieldNode = $('#' + field.id);
				
				fieldNode.empty();

				do {
					var newOption = $('<option></option>').attr({"value" : optionIndex}).html(optionCaptions[optionIndex]);
					if ( parseInt(field.value,10) === optionIndex ) {
						newOption.attr({"selected" : true});
					}
					fieldNode.append(newOption);
					optionIndex++;
				} while (optionIndex < orderFieldCount)
			});
			*/

		}
	}
	window.SelectOrderer = SelectOrderer;

	function DragOrderer(parameters) {
		this.parameters = parameters;

		this.fieldKeys = parameters.field_keys;
		/*
		this.fieldKeys.case_studies
		this.fieldKeys.ordinate
		this.fieldKeys.image
		this.fieldKeys.id
		this.fieldKeys.title
		*/

		this.itemdata = [];
		this.orderData = [];
		this.itemCount = 0,

		this.init();
	}

	DragOrderer.prototype = {
		"constructor" : DragOrderer,
		"template" : function () {var that = this; },
		
		"init" : function () {
			var that = this;

			console.log('case_study_sorter.js => DragOrderer');
			// console.log(this.fieldKeys);

			this.indexCaseStudies();
			this.populate();
			this.buildWidget();
		},
		"indexCaseStudies" : function () {
			var that = this;

			var itemNodes = $('.acf-field-' + this.fieldKeys.case_studies + ' tr.acf-row').not('tr.acf-clone');

			this.itemdata = [];
			this.orderData = [];
			this.itemCount = itemNodes.length;

			itemNodes.each(function(index) {
				var data = {
					"index" : index,
					"image" : $(this).find('div.acf-field-' + that.fieldKeys.image + ' img').eq(0).attr('src'),
					"id" : $(this).find('div.acf-field-' + that.fieldKeys.id + ' input').eq(0).val(),
					"title" : $(this).find('div.acf-field-' + that.fieldKeys.title + ' input').eq(0).val()
				};
				that.itemdata.push(data);
				that.orderData.push(data.id);		
			});

			console.log(this.itemdata);
			console.log(this.orderData);
		},
		"populate" : function () {
			var that = this;

			this.ordinate_field = $('#' + this.fieldKeys.ordinate),
			this.of = this.ordinate_field.parents('.acf-field').eq(0);

			this.ordinate_field.val( this.orderData.join(','));
		},
		"buildWidget" : function () {
			var that = this;
			// build sorter widget
			var olist = $('<ul></ul>').attr({"id" : "case_study_ordinate_widget"});

			var fieldIndex = 0;
			do {
				thisField = this.itemdata[fieldIndex];

				var outer = $('<li></li>').attr({"data-id" : thisField.id});

				// var inner = $('<p></p>').html(thisField.title);
				var frame = $('<div></div>').addClass('frame');
				var inner = $('<img></img>').attr({"alt" : thisField.title, "title" : thisField.title, "src" : thisField.image});

				outer.append( frame );
				frame.append( inner );

				olist.append( outer );
				fieldIndex++;
			} while (fieldIndex < this.itemCount)

			// add jQuery UI sortable behaviour to widget
		    olist.sortable({
		    	/*
		    	"start" : function (event,ui) {
		    	},
		    	"stop" : function (event,ui) {
		    	},
		    	*/
		    	"update" : function (event,ui) {
		    		that.handleChange(event,ui);
		    	}
		    });

			this.of.append(olist);
		},
		"handleChange" : function () {
			var that = this;

			var updatedSorter = $('#case_study_ordinate_widget li');

			var a = [], i = 0, n = updatedSorter.length, p;
			do {
				p = updatedSorter.eq(i);
				a.push(p.attr('data-id'));
				i++;
			} while (i < n)
			this.ordinate_field.val( a.join(','));
		}
	}
	window.DragOrderer = DragOrderer;

	// capture IDs and values of 'case_study_order' (field key: field_5c8f733b8cf1f) fields
	var optionCaptions = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'];
		selectRegex = /(acf-field_59479e957e9d0-2-field_594a81e279f5e-([0-9]+)-field_5c98eaab500d6)/,
		orderFields = [],
		orderFieldCount = 0;

	// use select field for each case study to define order
	var selectOrderer = new SelectOrderer({
		"options" : optionCaptions,
		"selectRegex" : selectRegex
	});

	// use drag and drop widget to arrange case studies
	var dragOrderer = new DragOrderer({
		"field_keys" : fieldKeys
	});

	/*
	// ACF javascript API

	// acf.add_action('new_field/name=case_study_order', function($el) {
	// 	console.log('acf new field');
	// }); 

	// fires on any change to HTML/DOM
	acf.add_action('append', function( $el ){
		console.log('acf append');
	});

	// fire on page DOM ready
	// before 'load'
	acf.add_action('ready', function( $el ){
		console.log('acf ready');
	});

	// fire on ACF load
	// after 'ready'
	acf.add_action('load', function( $el ){
		console.log('acf load');
	});
	*/

})(jQuery);	
