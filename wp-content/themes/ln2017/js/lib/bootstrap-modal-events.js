$(document).ready(function() {

	// https://mdbootstrap.com/docs/jquery/modals/events/
	var modalBasePage = window.location.href;

	// check to see if the page has been opened from a bookmarked modal
	// #[a-zA-Z0-9-_]
	var anchorRegex = /(#[a-zA-Z0-9-_]+)$/;
	var v = anchorRegex.exec(modalBasePage);
	if (v !== null) {
		// strip off anchor to get base url	
		modalBasePage = modalBasePage.substr(0 ,modalBasePage.length - v[0].length);
	}

	// before the modal is opened
	$(".modal").on('show.bs.modal', function(event){

		var button = $(event.relatedTarget) // Button that triggered the modal
		var recipient = button.attr('data-target');

		// don't update the address bar if the modal was triggered by an anchor
		if (recipient !== undefined) {
			history.pushState(null, null, modalBasePage + recipient);		
		}
			
	});

	// after the model has been opened
	// $(".modal").on('shown.bs.modal', function(){
	// });

	// before the modal is closed
	// $(".modal").on('hide.bs.modal', function(){
	// });

	// after the modal has been closed
	$(".modal").on('hidden.bs.modal', function(){
		// strip anchor off of address bar when the modal is closed
		history.pushState(null, null, modalBasePage);		
	});
});