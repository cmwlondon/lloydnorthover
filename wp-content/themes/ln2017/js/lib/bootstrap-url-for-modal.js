/*
/* This allows you to send a URL that links directly to an open modal window with Bootstrap.
/*
/*
/* Then you can send people a link such as
/* http://www.website.com/page.html#myModal
/* and it'll load the page with the modal open.
*/

/* Make sure the modal has an id:
/* <div class="modal" id="myModal" ... >
*/


// OLD
// $(document).ready(function() {

//   if(window.location.href.indexOf('#tuc') != -1) {
//     $('#tuc').modal('show');
//   }

// });

function findHashItem(hrefitem) {
	if (hrefitem.indexOf('#') != -1) {
		var hp = hrefitem.split('#');
		if (hp[1].length > 0) {
			if( $('#' + hp[1]).length > 0 ) {
				// close open modal
				if ($('.modal.in').length > 0 ) {
					$('.modal.in').modal('toggle');
				}

				$('#' + hp[1]).modal('show');
			}
		}
	}
}

$(document).ready(function() {

	if(!window.HashChangeEvent)(function(){
		var lastURL=document.URL;
		window.addEventListener("hashchange",function(event){
			Object.defineProperty(event,"oldURL",{enumerable:true,configurable:true,value:lastURL});
			Object.defineProperty(event,"newURL",{enumerable:true,configurable:true,value:document.URL});
			lastURL=document.URL;
		});
	}());

	// open overlay on page load
	findHashItem( window.location.href );

	// HTML5 hashchange event
	// https://developer.mozilla.org/en-US/docs/Web/Events/hashchange 
	if ("onhashchange" in window) {
		window.onhashchange = function(ev) {
			findHashItem( ev.newURL );
		};
	}

	/*
    $('.modal').each(function() {
        var id = $(this).attr('id');
        if (window.location.href.indexOf(id) != -1) {
        	console.log(id);
            $('#' + id).modal('show');
        }
    });
	*/
});
