var supportsCSSTransitions, supportsCSSAnimation, supportsSVG, isIOS, ishighdpi, isIE, dpi, windowSize, mobileWidth, doVisualUpdates;
var isIE10 = false;
var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);

var thisMatrix;

var hoverItem = -1, active = false;

Modernizr.addTest('isios', function() {
    return navigator.userAgent.match(/(iPad|iPhone|iPod)/g);
});

// SINGLE ITEM
$(document).ready(function() {

    supportsCSSAnimation = $('html').hasClass('cssanimations');
    supportsCSSTransitions = $('html').hasClass('csstransitions');
    supportsSVG = $('html').hasClass('csstransitions');
    isIE = $('html').hasClass('ie') || isIE11 || isIE10;

    // if device supports devicePixelRatio check for retina display, otherwise assume standard res (IE 9/10) 
    if (typeof window.devicePixelRatio != 'undefined' ) {
        ishighdpi = ( window.devicePixelRatio > 1); 
        dpi = window.devicePixelRatio;
    } else {
        ishighdpi = false;
        dpi = 1;
    }
    $('html').addClass('dpr' + dpi);

    // test for iOS devices - relevant to background image attachment/dedvice pixel ratio
    isIOS = Modernizr.isios;
    if ( location.href.indexOf('iostest=1') !== -1 ) { isIOS = true; }
    if ( isIOS ) { $('html').addClass('ios'); } else { $('html').addClass('not-ios'); } 

  /*
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight){
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
      }
    };
  }

  // For specifically opening all profile info at the same time:
  var profilePics = document.querySelector("#people");
  var profilePanels = document.getElementsByClassName("accordion1");
  var i

  profilePics.onclick = function () {
  	for (i = 0; i < profilePanels.length; i++) {
  		$(profilePanels[i]).toggleClass("active");
  	};
  }

  // For specifically opening all office location info at the same time:
  var officeLocations = document.querySelector("#office-locations");
  var locationPanels = document.getElementsByClassName("accordion2");
  var i

  officeLocations.onclick = function () {
  	for (i = 0; i < locationPanels.length; i++) {
  		$(locationPanels[i]).toggleClass("active");
  	};
  }
  */

  var team = $('.team-member-grid');
  var ti = $('.team-member__profile');
  var teamAccordions = $(".accordion1");
  var officeAccordions = $(".accordion2");

  if ( teamAccordions.length > 0 ) {
    teamAccordions.each(function(i) {

      $(this).on('click',function(i){
        var itemIndex = teamAccordions.index($(this));
        var parent = $(this).parent();

        if (hoverItem == -1 ) {
          parent.addClass('hover');
          ti.addClass('active');
          hoverItem = itemIndex;
        } else if ( hoverItem == itemIndex ) {
          parent.removeClass('hover');
          hoverItem = -1;
          ti.removeClass('active');
        }

      });
    });
  }

  if ( officeAccordions.length > 0 ) {
    officeAccordions.each(function(i) {
      $(this).on('click',function(i){
        officeAccordions.each(function(i){
          $(this).toggleClass('active');  
        });
      });
    });
  }

  // our people vacancies expanders
  var vacancies = $('section.vacanciesList article');
  if ( teamAccordions.length > 0 ) {
    vacancies.each(function(i){
      $(this).find('header').on('click',function(e){
        e.preventDefault(e);

        var item = $(this).parents('article').eq(0);

        var action = item.hasClass('open') ? 'close' : 'open';
        console.log("action: %s", action);

        switch ( action ) {
          case 'open' : {
            // close any open articles
            vacancies.not(item).removeClass('open');
            // open selected article
            item.addClass('open');
          } break;
          case 'close' : {
            item.removeClass('open');
          } break;
        }
      });
    })

  }
});