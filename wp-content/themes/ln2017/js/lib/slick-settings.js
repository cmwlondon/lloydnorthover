/*
*  Slick Slider Settings

*/

// SINGLE ITEM
$(document).ready(function() {
    $('.single-item').slick({
        arrows: false,
        // lazyLoad: 'ondemand',
        infinite: true,
        speed: 2000,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 2000,
    });
});
