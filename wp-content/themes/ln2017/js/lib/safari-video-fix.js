$(document).ready(function() {

    /*
    https://stackoverflow.com/questions/41672355/how-to-prevent-delay-when-looping-html5-mp4-video-in-safari

    https://stackoverflow.com/questions/20753861/html5-video-seamless-looping

    https://stackoverflow.com/questions/32058967/html5-video-loop-with-a-gap-or-delay-of-few-seconds
    document.getElementById('myVideo').addEventListener('ended',myHandler,false);
    */
    /*
    */
    function videoEventHandler( event ) {
        switch( event.type ) {
            case "canplay" : {
                event.target.play();
            } break;

            case "ended" : {
                setTimeout(function(){
                    // event.target.currentTime = 0;
                    event.target.play();
                }, 5);
                
            } break;
        }
    }

    $('video').each(function(i){

        // $(this).get(0).addEventListener('canplay', videoEventHandler, false);
        $(this).get(0).addEventListener('ended', videoEventHandler, false);
        $(this).get(0).play();
    });
});
