/*
our-work
work filter
all,identity,digital,engagement
*/

var currentFilter = 'all';
$(document).ready(function() {
	if ( $('.workFilter').length > 0 && $('#case-study').length > 0) {

		var caseStudies = $('#case-study ul.caseStudyList');
		var filterCommandBox = $('.workFilter');

		$('.workFilter > a').on('click',function(e) {
			e.preventDefault(e);
			var filter = $(this).attr('data-ref');

			if ( filter !== currentFilter) {
				filterCommandBox.find('a[data-ref="' + currentFilter + '"]').eq(0).removeClass('selected');
				filterCommandBox.find('a[data-ref="' + filter + '"]').eq(0).addClass('selected');
				currentFilter = filter;

				if( filter === 'all' ) {
					caseStudies.find('li').show();
				} else {
					caseStudies.find('li[data-category*=' + filter + ']').show();
					caseStudies.find('li').not('[data-category*=' + filter + ']').hide();
				}
			}
		});
	}
});

