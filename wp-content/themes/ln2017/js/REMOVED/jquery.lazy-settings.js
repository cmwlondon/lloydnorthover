/*
 *  jQuery Lazy Settings
 */

$(document).ready(function() {
	$('.lazy').lazy({
		// chainable: false,
		// bind: 'event',
		// threshold: 250,
		// visibleOnly: true,
		// appendScroll: window,
		scrollDirection: 'vertical',
		// delay: 1000,
		// combined: true,

		effect: 'fadeIn',

		// enableThrottle: true,
		// throttle: 800,

		afterLoad: function(element) {
			console.log('images have been lazy loaded');
		},
		onError: function(element) {
			console.log('error loading ' + element.data('src'));
		},
		onFinishedAll: function() {
			console.log('lazy loading finished');
		}

	});
});
