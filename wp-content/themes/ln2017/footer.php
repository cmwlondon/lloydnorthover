<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ln2017
 */

?>

	</div><!-- #content -->

	<!-- <footer id="colophon" class="site-footer" role="contentinfo">

		<div class="container site-info">
			<div class="first-2"></div>
			<div class="second-2 copyright">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>"><?php echo date("Y"); ?> &copy; <?php bloginfo('name'); ?></a>
			</div>
		</div>

	</footer>
-->
<!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
