	// acf java script events
	// https://www.advancedcustomfields.com/resources/adding-custom-javascript-fields/

(function($) {

	console.log('case_study_sorter.js');

	console.log(acf);

	acf.add_action('append', function( $el ){
		console.log('acf appendf');
	});

	acf.add_action('ready', function( $el ){
		console.log('acf ready');
	});

	acf.add_action('load', function( $el ){
		console.log('acf load');
	});


	// capture IDs and values of 'case_study_order' (field key: field_5c8f733b8cf1f) fields
	var optionCaptions = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20'];
		selectRegex = /(acf-field_59479e957e9d0-2-field_594a81e279f5e-([0-9]+)-field_5c98eaab500d6)/,
		orderFields = [],
		orderFieldCount = 0;

	// get case_study_order field IDs and Values
	$('select').each(function(){
		var id = $(this).attr('id');
		
		if( typeof id !== 'undefined' ) {
			var matches = id.match(selectRegex);
			if ( matches ) {
				orderFields.push({
					"id" : matches[0],
					"value" : $(this).find('option:selected').val()
				});
			}
		}
	});

	orderFieldCount = orderFields.length;

	// rebuild fields based on number of case studies available	
	var fieldIndex = 0,
		thisField = orderFields[fieldIndex];
		
	do {
		thisField = orderFields[fieldIndex];

		console.log("fieldIndex:[%s] value:[%s]", fieldIndex, thisField.value);

		var optionIndex = 0,
			fieldNode = $('#' + thisField.id);
		
		fieldNode.empty();

		var newOption = $('<option></option>').attr({"value" : '0'}).html('Select Position');
		fieldNode.append(newOption);

		/*
		do {
			var offsetOptionValue = (optionIndex + 1);
			console.log(offsetOptionValue, optionIndex);
			var newOption = $('<option></option>').attr({"value" : offsetOptionValue}).html(optionCaptions[optionIndex]);
			if ( parseInt(thisField.value, 10) === offsetOptionValue ) {
				newOption.attr({"selected" : true});
			}
			fieldNode.append(newOption);
			optionIndex++;
		} while (optionIndex < orderFieldCount)
		*/
		
		fieldIndex++;
	} while (fieldIndex < orderFieldCount)

	/*
	orderFields.map(function(field,index) {
		var optionIndex = 0,
			fieldNode = $('#' + field.id);
		
		fieldNode.empty();

		do {
			var newOption = $('<option></option>').attr({"value" : optionIndex}).html(optionCaptions[optionIndex]);
			if ( parseInt(field.value,10) === optionIndex ) {
				newOption.attr({"selected" : true});
			}
			fieldNode.append(newOption);
			optionIndex++;
		} while (optionIndex < orderFieldCount)
	});
	*/
})(jQuery);	
