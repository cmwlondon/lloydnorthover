<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ln2017
 */
$page_id = get_the_ID();
$page_name = $post->post_name;
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<!-- FAVICONS -->
<meta name="apple-mobile-web-app-title" content="Lloyd Northover">
<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo(template_url);?>/img/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo(template_url);?>/img/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo(template_url);?>/img/favicons/favicon-16x16.png">
<link rel="manifest" href="<?php bloginfo(template_url);?>/img/favicons/manifest.json">
<link rel="mask-icon" href="<?php bloginfo(template_url);?>/img/favicons/safari-pinned-tab.svg" color="#b3b3b2">
<link rel="shortcut icon" href="<?php bloginfo(template_url);?>/img/favicons/favicon.ico">
<meta name="msapplication-config" content="<?php bloginfo(template_url);?>/img/favicons/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ln2017' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<div class="container">

				<div class="first-2">
					<div class="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg" alt="Lloyd Northover Logo" width="248" height="37"></a>
					</div>
				</div>

<!-- 				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><span class="heavy-metal-text">lloyd</span><span class="nobel-text">northover</span></a></h1> -->
				<div class="second-2">
					<nav>
						<ul>
							<li <?php if ( $page_name === 'our-people' ) : echo 'class="current"'; endif ?>><a href="/our-people">OUR PEOPLE</a></li>
							<li <?php if ( $page_name === 'our-work' ) : echo 'class="current"'; endif ?>><a href="/our-work">OUR WORK</a></li>
							<li <?php if ( $page_name === 'our-services' ) : echo 'class="current"'; endif ?>><a href="/our-services">OUR SERVICES</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
