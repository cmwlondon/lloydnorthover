<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ln2017
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->

		<!-- ADVANCED CUSTOM FIELDS  -->
			<?php
			// check if the flexible content field has rows of data
			if( have_rows('content') ):
				// loop through the rows of data
				while ( have_rows('content') ) : the_row();

					if( get_row_layout() == 'what_we_offer' ):

						get_template_part( 'partials/acf','offer2_block');
					elseif( get_row_layout() == 'text_block' ):

						// single column text block
						get_template_part( 'partials/acf','text_block');

					elseif( get_row_layout() == 'text_block_two_columns' ):

						// two columg text block (first element in landing page)
						get_template_part( 'partials/acf','text_block_two_columns');

					elseif( get_row_layout() == 'footer_two_column_privacy_policy' ):

						// footer: two column with provacy policy overlay
						get_template_part( 'partials/acf','footer_two_column_privacy_policy');

					elseif( get_row_layout() == 'offer_block' ):

						// what we offer block + related overlays
						get_template_part( 'partials/acf','offer_block');

					elseif( get_row_layout() == 'office_locations_block' ):

						// offices block
						get_template_part( 'partials/acf','office_locations_block');

					elseif( get_row_layout() == 'team_block' ):

						// team block
						get_template_part( 'partials/acf','team_block');

					elseif( get_row_layout() == 'slick_slider' ):

						// hero/slider block
						get_template_part( 'partials/acf','slick_slider');

					elseif( get_row_layout() == 'case_study_block' ):

						// case study / our work block
						get_template_part( 'partials/acf','case_study_block');

					endif;

				endwhile;
			else :
				// no layouts found
			endif;
			?>

	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();

