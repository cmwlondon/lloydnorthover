<?php

$sectionid = get_sub_field('section_id');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');

?>
<section id="<?php echo $sectionid;?>">

	<div style="padding-top: <?php echo $paddingtop ?>px; padding-bottom: <?php echo $paddingbottom ?>px;">
		<div class="container">

		<div class="team-member-grid">

		<ul>

		<?php
		// check if the repeater field has rows of data
		if( have_rows('team_member_content') ):
		// loop through the rows of data
		while ( have_rows('team_member_content') ) : the_row(); ?>

			<li class="team-member__profile">
				<img class="lazyload accordion clickable" data-src="<?php the_sub_field('team_member_image'); ?>" />

				<div class="panel">
					<div class="accordion__content">
						<p><?php the_sub_field('team_member_info'); ?></p>
					</div>
				</div>
			</li>

		<?php endwhile; ?>
		<?php endif; ?>

		</ul>

		</div> <!-- team-member-grid -->

		</div> <!-- container -->
	</div> <!-- padding -->

</section>