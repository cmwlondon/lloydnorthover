<?php

$sectionid = get_sub_field('section_id');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');

?>
<section id="<?php echo $sectionid;?>">

<div style="padding-top: <?php echo $paddingtop ?>px; padding-bottom: <?php echo $paddingbottom ?>px;">

<div class="container">

<div class="first-2">

	<div class="clickable" data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('offer_id_one'); ?>" style="padding-left: 15px; padding-bottom: 20px;">
	<?php the_sub_field('offer_content_left'); ?>
	</div>

	<!-- Modal -->
	<div class="modal fades" id="<?php the_sub_field('offer_id_one'); ?>" tabindex='-1' role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="background-color: <?php the_sub_field('modal_bkg_color_one'); ?>;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">

					<div class="first-2 pad-bottom-20">
						<?php the_sub_field('modal_content_left_one'); ?>
					</div>

					<div class="second-2 pad-bottom-20">
						<?php the_sub_field('modal_content_right_one'); ?>
					</div>

				</div> <!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

			</div> <!-- modal-content -->

		</div> <!-- modal-dialog -->
	</div> <!-- modal-fades -->

</div>

<div class="second-2">

	<div class="clickable" data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('offer_id_two'); ?>" style="padding-left: 15px;">
	<?php the_sub_field('offer_content_right_first'); ?>
	</div>

	<div class="clickable" data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('offer_id_three'); ?>" style="padding-left: 15px;">
	<?php the_sub_field('offer_content_right_second'); ?>
	</div>


	<!-- Modal for need help with a great business idea -->
	<div class="modal fades" id="<?php the_sub_field('offer_id_two'); ?>" tabindex='-1' role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="background-color: <?php the_sub_field('modal_bkg_color_two'); ?>;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">

					<div class="first-2">
						<div class="mcs-intro">
							<?php the_sub_field('modal_content_two'); ?>
						</div>
					</div>
					<div class="second-2"></div>

				<?php
				// check if the repeater field has rows of data
				if( have_rows('mini_case_study_info') ):
					// loop through the rows of data
					while ( have_rows('mini_case_study_info') ) : the_row(); ?>

					<h3 class="modal-title heavy-metal-text pad-top-30"><?php the_sub_field('mcs_title'); ?></h3>
					<h4 class="modal-subtitle white-text pad-bottom-20"><?php the_sub_field('mcs_subtitle'); ?></h4>

					<?php if( get_sub_field('mcs_description') ): ?>
						<div class="first-2 pad-bottom-30">
						<?php the_sub_field('mcs_description'); ?>
						</div>
					<?php endif; ?>

					<?php if( get_sub_field('mcs_image_one') ): ?>
						<div class="second-2 pad-bottom-10">
							<img class="lazyload" data-src="<?php the_sub_field('mcs_image_one'); ?>" />
						</div>
					<?php endif; ?>

					<?php if( get_sub_field('mcs_image_two') ): ?>
						<div class="first-2 pad-bottom-10">
							<img class="lazyload" data-src="<?php the_sub_field('mcs_image_two'); ?>" />
						</div>
					<?php endif; ?>

					<?php if( get_sub_field('mcs_image_three') ): ?>
						<div class="second-2 pad-bottom-10">
							<img class="lazyload" data-src="<?php the_sub_field('mcs_image_three'); ?>" />
						</div>
					<?php endif; ?>

					<?php if( get_sub_field('mcs_image_four') ): ?>
						<div class="first-2 pad-bottom-10">
							<img class="lazyload" data-src="<?php the_sub_field('mcs_image_four'); ?>" />
						</div>
					<?php endif; ?>

					<?php if( get_sub_field('mcs_image_five') ): ?>
						<div class="second-2 pad-bottom-10">
							<img class="lazyload" data-src="<?php the_sub_field('mcs_image_five'); ?>" />
						</div>
					<?php endif; ?>

				<?php endwhile; ?>
				<?php endif; ?>

				</div> <!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

			</div> <!-- modal-content -->

		</div> <!-- modal-dialog -->
	</div> <!-- modal-fades -->



	<!-- Modal for question, is your brand on brand? -->
	<div class="modal fades" id="<?php the_sub_field('offer_id_three'); ?>" tabindex='-1' role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="background-color: <?php the_sub_field('modal_bkg_color_three'); ?>;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">

					<div class="first-2">
						<div class="mcs-intro">
							<?php the_sub_field('modal_content_three_intro'); ?>
						</div>
					</div>
					<div class="second-2"></div>

					<div class="first-2 pad-bottom-10">
						<?php the_sub_field('modal_content_three_left'); ?>
					</div>

					<div class="second-2 pad-bottom-10">
						<?php the_sub_field('modal_content_three_right'); ?>
					</div>

				</div> <!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

			</div> <!-- modal-content -->

		</div> <!-- modal-dialog -->
	</div> <!-- modal-fades -->

</div>

</div> <!-- container -->
</div> <!-- padding -->

</section>