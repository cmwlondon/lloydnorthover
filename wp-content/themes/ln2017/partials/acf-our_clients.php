<?php
$sectionid = get_sub_field('section_id');
$sectionTitle = get_sub_field('our_clients_title');
$paddingtop = get_sub_field('oc_padding-top');
$paddingbottom = get_sub_field('oc_padding-bottom');
$backgroundColour = get_sub_field('oc_background-colour');
/*
/themes/ln2017/functions.php my_acf_load_case_study_info
sort case studies into arbitrary order -> 'case_study_order': comma seperated list of case study 'id' field
*/
?>
<section id="<?php echo $sectionid;?>">
	<div class="container">
		<div class="bgbox" style="background-color:<?php echo $backgroundColour; ?>;padding-top:<?php echo $paddingtop; ?>;padding-bottom:<?php echo $paddingbottom; ?>;">
			<header>
				<h2><?php echo $sectionTitle; ?></h2>
			</header>
			<div class="oc_colums">
	<!-- [oc_columns] -->			

				<?php while ( have_rows('oc_columns') ) : the_row(); ?>
					<div class="oc_column">
					<!-- 
					[oc_column_title]
					[oc_column_copy]
					-->
					<?php echo get_sub_field('oc_column_copy') ?>
				</div>

				<?php endwhile ?>
			</div>
		</div>
	</div>
</section>