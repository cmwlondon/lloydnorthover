<?php

$sectionid = get_sub_field('section_id');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');
$backgroundColor = get_sub_field('background_colour');

?>
<section id="<?php echo $sectionid;?>">

	<div class="marginNull" style="padding-top: <?php echo $paddingtop ?>px; padding-bottom: <?php echo $paddingbottom ?>px;">
		<div class="container" style="background-color:<?php echo $backgroundColor ?>;padding-left:0px;padding-right:0px;">

			<img class="lazyload" src="<?php the_sub_field('venn_diagram_gif'); ?>" alt="">
			<!-- 
			<div class="first-2">
				<?php the_sub_field('content_left_text'); ?>
			</div>

			<div class="second-2">
				<img class="lazyload" src="<?php the_sub_field('content_right_chart'); ?>" alt="">
			</div>
			-->
		</div> <!-- container -->
	</div>

</section>