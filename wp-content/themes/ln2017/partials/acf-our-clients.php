<?php
$sectionid = get_sub_field('section_id');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');
/*
/themes/ln2017/functions.php my_acf_load_case_study_info
sort case studies into arbitrary order -> 'case_study_order': comma seperated list of case study 'id' field
*/
?>
<section id="<?php echo $sectionid;?>">
	<header>
		<h2>[our_clients_title]</h2>
	</header>
	<div class="bgbox" data-style="background-colour:[oc_background-colour];padding-top:[oc_padding-top];padding-bottom:[oc_padding-bottom];">
		<div class="oc_colums">
<!-- [oc_columns] -->			
			<div class="oc_column">
				<!-- 
				[oc_column_title]
				[oc_column_copy]
				-->
				<h3>COLUMN</h3>
				<ul>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
				</li>
			</div>
			<div class="oc_column">
				<h3>COLUMN</h3>
				<ul>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
				</li>
			</div>
			<div class="oc_column">
				<h3>COLUMN</h3>
				<ul>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
				</li>
			</div>
			<div class="oc_column">
				<h3>COLUMN</h3>
				<ul>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
					<li>Item</li>
				</li>
			</div>
		</div>
	</div>
</section