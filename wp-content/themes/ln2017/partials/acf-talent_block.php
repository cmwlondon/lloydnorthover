<?php
$sectionid = get_sub_field('section_id');
$sectionTitle = get_sub_field('title');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');
$backgroundColour = get_sub_field('background_colour');
/*
/themes/ln2017/functions.php my_acf_load_case_study_info
sort case studies into arbitrary order -> 'case_study_order': comma seperated list of case study 'id' field
*/
?>
<section id="<?php echo $sectionid;?>">
	<div class="container">
		<header>
			<h2><?php echo $sectionTitle; ?></h2>
		</header>
		<div class="columns">
			<div class="column ltc"  style="background-color:<?php echo $backgroundColour; ?>;padding-top:<?php echo $paddingtop; ?>;padding-bottom:<?php echo $paddingbottom; ?>;">
				<div class="vmid"><?php echo get_sub_field('left_column'); ?></div>
			</div>
			<div class="column rtc"  style="background-color:<?php echo $backgroundColour; ?>;padding-top:<?php echo $paddingtop; ?>;padding-bottom:<?php echo $paddingbottom; ?>;">
				<section class="vacanciesList">

				<?php while ( have_rows('vacancies') ) : the_row(); ?>

					<article>
						<header>
							<h2><?php echo get_sub_field('vacancy_title') ?></h2>
							<h3><?php echo get_sub_field('location') ?><h3>
							<div class="control"><span class="alpha">+</span><span class="beta">-</span></div>
						</header>
						
						<div class="clipper">
							<div class="content">
								<?php echo get_sub_field('vacancy_description') ?>
							</div>
						</div>
					</article>
					

				<?php endwhile ?>



				</section>				
			</div>
		</div>
	</div>
</section
