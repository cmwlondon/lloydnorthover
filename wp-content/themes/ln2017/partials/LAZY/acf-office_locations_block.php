<?php

$sectionid = get_sub_field('section_id');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');

?>
<section id="<?php echo $sectionid;?>">

	<div style="padding-top: <?php echo $paddingtop ?>px; padding-bottom: <?php echo $paddingbottom ?>px;">
		<div class="container">

		<div class="office-location-grid">

		<ul>

		<?php
		// check if the repeater field has rows of data
		if( have_rows('office_location_content') ):
		// loop through the rows of data
		while ( have_rows('office_location_content') ) : the_row(); ?>

			<li class="office-location__info">

				<img class="lazy office-image accordion clickable" data-src="<?php the_sub_field('office_location_image'); ?>" />

					<div class="panel">
						<div class="office__details">
							<p><a href="<?php the_sub_field('office_location_address_link'); ?>" target="_blank"><?php the_sub_field('office_location_address_display'); ?></a><br>
							<br>
							<a href="tel:<?php the_sub_field('office_location_tel_link'); ?>"><?php the_sub_field('office_location_tel_display'); ?></a></p>
						</div>
					</div>
			</li>

		<?php endwhile; ?>
		<?php endif; ?>

		</ul>

		</div> <!-- office-location-grid -->

		</div> <!-- container -->
	</div> <!-- padding -->

</section>