<?php

$sectionid = get_sub_field('section_id');
$slidertype = get_sub_field('slider_type');
$sliderwidth = get_sub_field('slider_width');

?>
<section id="<?php echo $sectionid;?>">

	<div class="slider single-item widescreen">
		<?php $images = get_sub_field('slides');
			foreach ($images as $image):?>
				<div>
				<img class="lazy" data-src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
		<?php endforeach;?>
	</div>

</section>