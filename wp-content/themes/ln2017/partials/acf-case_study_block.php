<?php
$sectionid = get_sub_field('section_id');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');
/*
/themes/ln2017/functions.php my_acf_load_case_study_info
sort case studies into arbitrary order -> 'case_study_order': comma seperated list of case study 'id' field
*/
?>

<section id="<?php echo $sectionid;?>">

<div style="padding-top: <?php echo $paddingtop ?>px; padding-bottom: <?php echo $paddingbottom ?>px;">

<div class="container">

<div class="grid">
<div class="case-study-grid">

<!-- case_study_info REPEATER -->
<?php
// check if the repeater field has rows of data
if( have_rows('case_study_info') ):
	?>
<ul class="caseStudyList">
<?php
	// loop through the rows of data
	while ( have_rows('case_study_info') ) : the_row();
$row_categories_raw = get_sub_field('case_study_category');
$row_categories = implode(',', $row_categories_raw);
?>

<li class="case-study__item" data-category="<?php echo $row_categories; ?>">

<figure class="effect-joe">

	<img src="<?php the_sub_field('case_study_image'); ?>" data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('case_study_id'); ?>" width="800" height="568" />

	<!-- Hover Info -->
	<figcaption data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('case_study_id'); ?>">
		<h2 style="color: <?php the_sub_field('title_color'); ?>;" ><?php the_sub_field('case_study_title'); ?></h2>
			<!-- <p class="description">Click here to read more</p> -->
	</figcaption>

</figure>

	<!-- Modal -->
	<div class="modal fades" id="<?php the_sub_field('case_study_id'); ?>" tabindex='-1' role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="background-color: <?php the_sub_field('modal_bkg_color'); ?>;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">

					<h3 class="modal-title heavy-metal-text"><?php the_sub_field('case_study_title'); ?></h3>
					<h4 class="modal-subtitle"><?php the_sub_field('case_study_subtitle'); ?></h4>

					<div class="first-2 pad-right-30 pad-bottom-20">
						<?php the_sub_field('case_study_description'); ?>
					</div>
					<div class="second-2 pad-bottom-20">
						<p class="heavy-metal-text">What we did</p>
						<?php the_sub_field('case_study_what_we_did'); ?>
					</div>

				<?php

				// check if the flexible content field has rows of data
				if( have_rows('case_study_content') ):

					// loop through the rows of data
					while ( have_rows('case_study_content') ) : the_row();

					// YOUTUBE VIDEO
					if( get_row_layout() == 'case_study_youtube_video' ):

						?>
						<div class="video pad-bottom-10">
							<iframe src="<?php the_sub_field('case_study_youtube_video'); ?>?modestbranding=1&autohide=1&showinfo=0&controls=1&rel=0" width="1280" height="720" frameborder="0" allowfullscreen></iframe>
						</div>
					<?php

					// VIMEO VIDEO
					elseif( get_row_layout() == 'case_study_vimeo_video' ):

						?>
						<div class="video pad-bottom-10">
							<iframe src="<?php the_sub_field('case_study_vimeo_video'); ?>?title=0&byline=0&portrait=0" width="1280" height="720" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
					<?php

					// FULL WIDTH IMAGE
					elseif( get_row_layout() == 'case_study_fwi' ):

						?>
						<div class="pad-bottom-10">
							<img src="<?php the_sub_field('case_study_fwi_one'); ?>" />
						</div>
					<?php

					// HALF WIDTH IMAGES
					elseif( get_row_layout() == 'case_study_hwi' ):

						?>
						<div class="first-2 pad-bottom-10">
							<img src="<?php the_sub_field('case_study_hwi_one'); ?>" width="800" height="568" />
						</div>

						<div class="second-2 pad-bottom-10">
							<img src="<?php the_sub_field('case_study_hwi_two'); ?>" width="800" height="568" />
						</div>
					<?php

					// IMAGE AND QUOTE
					elseif( get_row_layout() == 'case_study_image_quote' ):

						?>
						<div class="iq-container">
							<div class="first-2 iq pad-bottom-10">
								<img src="<?php the_sub_field('case_study_hwi_three'); ?>" width="800" height="568" />
							</div>

							<div class="second-2 iq pad-bottom-10">
								<div class="testimonial">
									<blockquote class="testimonial-quote heavy-metal-text">"<?php the_sub_field('case_study_quote');?>"</blockquote>
									<p class="testimonial-author nobel-text"><?php the_sub_field('case_study_quote_author');?></p>
								</div>
							</div>
						</div>
					<?php

					endif;

				endwhile;

			else :

				// no layouts found

			endif;

			?>

				</div> <!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

			</div> <!-- modal-content -->

		</div> <!-- modal-dialog -->
	</div> <!-- modal-fades -->

</li>

<?php endwhile; ?>
</ul>
<?php endif; ?>


</div> <!-- case-study-grid -->
</div> <!-- grid -->

</div> <!-- container -->
</div> <!-- padding -->
</section>

