<?php

$sectionid = get_sub_field('section_id');
$paddingtop = get_sub_field('padding_top');
$paddingbottom = get_sub_field('padding_bottom');

?>
<section id="<?php echo $sectionid;?>">

<div style="padding-top: <?php echo $paddingtop ?>px; padding-bottom: <?php echo $paddingbottom ?>px;">

<div class="container">

<div class="first-2">

	<div class="clickable" data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('offer_id_one'); ?>" style="padding-left: 15px; padding-bottom: 20px;">
	<?php the_sub_field('offer_content_left'); ?>
	</div>

	<!-- Modal -->
	<div class="modal fades" id="<?php the_sub_field('offer_id_one'); ?>" tabindex='-1' role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="background-color: <?php the_sub_field('modal_bkg_color_one'); ?>;">
				<div class="modal-header">
					<button type="button" class="close whiteclose" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">

					<div class="first-2 pad-bottom-20">
						<?php the_sub_field('modal_content_left_one'); ?>
					</div>

					<div class="second-2 pad-bottom-20">
						<?php the_sub_field('modal_content_right_one'); ?>
					</div>

				</div> <!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="close whiteclose" data-dismiss="modal">&times;</button>
				</div>

			</div> <!-- modal-content -->

		</div> <!-- modal-dialog -->
	</div> <!-- modal-fades -->

</div>

<div class="second-2">

	<div class="clickable" data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('offer_id_two'); ?>" style="padding-left: 15px;">
	<?php the_sub_field('offer_content_right_first'); ?>
	</div>

	<div class="clickable" data-keyboard="true" data-toggle="modal" data-target="#<?php the_sub_field('offer_id_three'); ?>" style="padding-left: 15px;">
	<?php the_sub_field('offer_content_right_second'); ?>
	</div>


	<!-- Modal for need help with a great business idea -->
	<div class="modal fades" id="<?php the_sub_field('offer_id_two'); ?>" tabindex='-1' role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="background-color: <?php the_sub_field('modal_bkg_color_two'); ?>;">
				<div class="modal-header">
					<button type="button" class="close whiteclose" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">

					<div class="first-2">
						<?php the_sub_field('modal_two_intro_left_text'); ?>
					</div>
					<div class="second-2">
						<?php the_sub_field('modal_two_intro_right_text'); ?>
					</div>
					
					<?php if( have_rows('places_images') ):
						while ( have_rows('places_images') ) : $row = the_row(); ?>
						
						<?php if( get_row_layout() == 'places_full_width_image' ) : ?>
							<div class="pad-bottom-10"><img class="lazyload" alt="" src="<?php the_sub_field('places_full_width_image'); ?>"></div>
						<?php elseif( get_row_layout() == 'places_image_pair' ) : ?>
							<div class="first-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('places_image_pair_left'); ?>" alt=""></div>
							<div class="second-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('places_image_pair_right'); ?>" alt=""></div>
						<?php elseif( get_row_layout() == 'places_text_then_image' ) : ?>
							<div class="first-2">
								<?php the_sub_field('places_ti_row_text'); ?>
							</div>
							<div class="second-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('places_ti_row_image'); ?>" alt=""></div>
						<?php elseif( get_row_layout() == 'places_image_then_text' ) : ?>
							<div class="first-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('places_it_row_image'); ?>" alt=""></div>
							<div class="second-2">
								<?php the_sub_field('places_it_row_text'); ?>
							</div>
						<?php endif; ?> 
					<?php endwhile; ?>
					<?php endif; ?>

				</div> <!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="close whiteclose" data-dismiss="modal">&times;</button>
				</div>

			</div> <!-- modal-content -->

		</div> <!-- modal-dialog -->
	</div> <!-- modal-fades -->



	<!-- Modal for question, is your brand on brand? -->
	<div class="modal fades" id="<?php the_sub_field('offer_id_three'); ?>" tabindex='-1' role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content" style="background-color: <?php the_sub_field('modal_bkg_color_three'); ?>;">
				<div class="modal-header">
					<button type="button" class="close whiteclose" data-dismiss="modal">&times;</button>
				</div>

				<div class="modal-body">

					<div class="first-2">
						<?php the_sub_field('people_intro_left'); ?>
					</div>
					<div class="second-2">
						<?php the_sub_field('people_intro_right'); ?>
					</div>

					<?php if( have_rows('people_images') ):
						while ( have_rows('people_images') ) : $row = the_row(); ?>
						
						<?php if( get_row_layout() == 'people_full_width_image' ) : ?>
							<div class="pad-bottom-10"><img class="lazyload" alt="" src="<?php the_sub_field('people_full_width_image'); ?>"></div>
						<?php elseif( get_row_layout() == 'people_pair_images' ) : ?>
							<div class="first-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('people_image_left'); ?>" alt=""></div>
							<div class="second-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('people_image_right'); ?>" alt=""></div>
						<?php elseif( get_row_layout() == 'people_text_then_image' ) : ?>
							<div class="first-2">
								<?php the_sub_field('people_ti_row_text'); ?>
							</div>
							<div class="second-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('people_ti_row_image'); ?>" alt=""></div>
						<?php elseif( get_row_layout() == 'people_image_then_text' ) : ?>
							<div class="first-2 pad-bottom-10"><img class="lazyload" src="<?php the_sub_field('people_it_row_image'); ?>" alt=""></div>
							<div class="second-2">
								<?php the_sub_field('people_it_row_text'); ?>
							</div>
						<?php endif; ?> 
					<?php endwhile; ?>
					<?php endif; ?>

				</div> <!-- modal-body -->

				<div class="modal-footer">
					<button type="button" class="close whiteclose" data-dismiss="modal">&times;</button>
				</div>

			</div> <!-- modal-content -->

		</div> <!-- modal-dialog -->
	</div> <!-- modal-fades -->

</div>

</div> <!-- container -->
</div> <!-- padding -->

</section>