// grab our gulp packages
var gulp 			= require('gulp');
var	autoprefixer 	= require('gulp-autoprefixer');
var	concat 			= require('gulp-concat');
var imagemin 		= require('gulp-imagemin');
var jshint 			= require('gulp-jshint');
var livereload 		= require('gulp-livereload');
var cleanCSS 		= require('gulp-clean-css');
var plumber 		= require('gulp-plumber');
var notify 			= require('gulp-notify');
var sass 			= require('gulp-sass');
var uglify 			= require('gulp-uglify');
var rename 			= require('gulp-rename');
var watch 			= require('gulp-watch');

// Run our default tasks
gulp.task('default', ['sass', 'js', 'img', 'watch']);

// Compile Sass to CSS
gulp.task('sass', function() {
	gulp.src('./sass/*.scss')
	.pipe(plumber(plumberErrorHandler))
	.pipe(sass())
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(cleanCSS({debug: true}, function(details) {
		console.log(details.name + ': ' + details.stats.originalSize + ' (Original Size)');
		console.log(details.name + ': ' + details.stats.minifiedSize + ' (Minified Size)');
	}))
	.pipe(gulp.dest(''))
	.pipe(livereload())
	.pipe(notify({
		message: 'Sass task complete'
	}));
});

// Concatenate js files and minify
gulp.task('js', function () {
	gulp.src('js/lib/*.js')
	.pipe(plumber(plumberErrorHandler))
	.pipe(jshint())
	.pipe(jshint.reporter('default'))
	.pipe(concat('global.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('js'))
	.pipe(livereload())
	.pipe(notify({
		message: 'JS task complete'
	}));
});

// Compress images
gulp.task('img', function() {
	gulp.src('img/src/*.{png,jpg,gif}')
	.pipe(plumber(plumberErrorHandler))
	.pipe(imagemin({
		optimizationLevel: 4,
		progressive: true,
		interlaced: true
	}))
	.pipe(gulp.dest('img'))
	.pipe(livereload())
	.pipe(notify({
		message: 'Images task complete'
	}));
});

// Configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
	livereload.listen();
	gulp.watch('**/*.scss', ['sass']);
	gulp.watch('js/lib/*.js', ['js']);
	gulp.watch('img/src/*', ['img']);
});

var plumberErrorHandler = { errorHandler: notify.onError({
		title: 'Gulp',
		message: 'Error: <%= error.message %>'
	})
};