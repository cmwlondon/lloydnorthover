<?php
/**
 * ln2017 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ln2017
 */

if ( ! function_exists( 'ln2017_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ln2017_setup() {


	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ln2017, use a find and replace
	 * to change 'ln2017' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'ln2017', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'ln2017' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ln2017_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'ln2017_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ln2017_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ln2017_content_width', 640 );
}
add_action( 'after_setup_theme', 'ln2017_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ln2017_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ln2017' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ln2017' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ln2017_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ln2017_scripts() {
	wp_enqueue_style( 'ln2017-style', get_stylesheet_uri() );

// FONT AWESOME
	wp_enqueue_style( 'ln2017-font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

// DEBUG SCRIPT (ADDS BORDER-RED TO ALL DIVS)
	// wp_enqueue_script( 'ln2017-debug', get_template_directory_uri() . '/js/debug.js', array(), '20151215', true );

// ALL SCRIPTS
	wp_enqueue_script( 'ln2017-modrnzr', get_template_directory_uri() . '/js/modernizr-custom.js', array(), '20151215', true );
	wp_enqueue_script( 'ln2017-global-min', get_template_directory_uri() . '/js/global.min.js', array(), '20151215', true );

// DEREGISTER DEFAULT WP-JQUERY AND ADD JQUERY AND JQUERY UI FROM GOOGLE CDN
	wp_deregister_script('jquery');
	wp_register_script('jquery', ('https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'), false, '2.2.4');
	wp_enqueue_script('jquery');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ln2017_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Additional features to allow styling of the templates.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


//////////////////////////////////////
// CHANGE 'HOWDY' WHEN LOGGED IN
//////////////////////////////////////

function replace_howdy( $wp_admin_bar ) {
	$my_account = $wp_admin_bar->get_node('my-account');
	$newtitle = str_replace('Howdy,', 'Logged in as', $my_account->title);
	$wp_admin_bar->add_node(array(
		'id' => 'my-account',
		'title' => $newtitle,
	));
}
add_filter('admin_bar_menu', 'replace_howdy', 25);


//////////////////////////
// CUSTOM LOGIN SCREEN
//////////////////////////

// LOGO URL
function my_login_logo_url() {
	return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

// LOGO TITLE
function my_login_logo_url_title() {
	return 'Lloyd Northover';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// LOGO IMAGE AND STYLING
function my_login_logo() { ?>
	<style type="text/css">
		.login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg) !important;
			text-indent: -9999px !important;
			height: 65px !important;
			width: 100% !important;
			background-size: contain !important;
			margin: 0 auto !important;
			padding: 6px;
		}
		.login form {
			margin-top: 20px !important;
		}
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


////////////////////////////////////////////////
// REMOVE QUERY STRING FROM STATIC RESOURCES
////////////////////////////////////////////////

function remove_cssjs_ver( $src ) {
 if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
 return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );


//////////////////////////////////////////////////////////
// STOP WORDPRESS GENERATING OTHER SIZED IMAGES ON UPLOAD
//////////////////////////////////////////////////////////

function add_image_insert_override( $sizes ){
    unset( $sizes[ 'thumbnail' ]);
    unset( $sizes[ 'medium' ]);
    unset( $sizes[ 'medium_large' ] );
    unset( $sizes[ 'large' ]);
    unset( $sizes[ 'full' ] );
    return $sizes;
}
add_filter( 'intermediate_image_sizes_advanced', 'add_image_insert_override' );

////////////////////////////////////////////////
// DISABLE WORDPRESS FROM ADDING <P> TAGS
// https://codex.wordpress.org/Function_Reference/wpautop#Disabling_the_filter
////////////////////////////////////////////////

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

////////////////////////////////////////////////
// REMOVE WP-EMOJI-RELEASE.MIN.JS
////////////////////////////////////////////////

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


////////////////////////////////////////////////
// REMOVE WP-EMBED.MIN.JS
////////////////////////////////////////////////
function stop_loading_wp_embed() {
	if (!is_admin()) {
		wp_deregister_script('wp-embed');
	}
}
add_action('init', 'stop_loading_wp_embed');


//////////////////////////////////////
// REMOVE WP META BOX
//////////////////////////////////////

add_filter('acf/settings/remove_wp_meta_box', '__return_true');

////////////////////////////////////////////////
// add admin specific CSS styelsheet
////////////////////////////////////////////////

function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');

////////////////////////////////////////////////
// add arbitrary sort order to case studies 'case_study_order'
////////////////////////////////////////////////

function my_acf_load_case_study( $value, $post_id, $field ) {
	global $caseStudyOrder;

	$caseStudyOrder = $value;

	return $value;
}
add_filter('acf/load_value/name=case_study_order', 'my_acf_load_case_study', 10, 3);


function my_acf_load_value( $value, $post_id, $field ) {
	//echo ('<p>my_acf_load_value/name=case_study_info</p>');

	/**/
	// drag-and-drop sorter
	// get order list
	global $caseStudyOrder;
	$caseStudyOrderArray = explode ( ',' , $caseStudyOrder );
	$caseStudyCount = count($caseStudyOrderArray);
	$maxItem = $caseStudyCount;
	$newItems = [];

	// echo "<pre>".print_r($caseStudyOrderArray, true)."</pre>";

	// https://www.advancedcustomfields.com/resources/how-to-sorting-a-repeater-field/
	$order = array();
	
	// bail early if no value
	if( empty($value) ) {
		return $value;
	}

	// need to accommodate deleted items
	// items in $caseStudyOrderArray which do not have a case_study
	// add to end of $caseStudyOrderArray
	
	// need to accommodate new items
	// case_study.id NOT in $caseStudyOrderArray
	// add to end of $caseStudyOrderArray

	// populate order
	// iterate over case studies
	foreach( $value as $i => $row ) {
		// find location of this item's id in the order list
		$thisID = $row['field_594a826c79f60'];

		$v = array_search($thisID, $caseStudyOrderArray);

		if ( $v === false ) {
			$newItems[] = [
				"id" => $thisID,
				"rowIndex" => $i
			]; 
		} else {
			$vv = $v +1;
			$order[] = $vv;
			$maxItem = ( $vv > $maxItem) ? $vv : $maxItem;
		}
	}

	// add new itme sonto end of order list
	foreach( $newItems AS $i => $newItem ) {
		$maxItem++;
		$order[] = $maxItem;
	}

	// echo "<pre>".print_r($order, true)."</pre>";

	/**/

	/*
	// case study select field sorter
	// select field
	// '0' - 'select position' default value
	// 1 - N
	// code at MARKER A locates items with order set to '0' and substitutes in position based on the number of items already ordered
	// 

	$order = array();
	$maxItem = 0;
	// bail early if no value
	if( empty($value) ) {
		return $value;
	}
	
	// populate order
	foreach( $value as $i => $row ) {
		$thisItem = $row['field_5c98eaab500d6'];
		$order[ $i ] = $thisItem; // field key for case_study_order field
		$maxItem = ( $thisItem > $maxItem) ? $thisItem : $maxItem;
	}

	$items = count($order);

	// find new items and move them to end
	// MARKER A
	foreach( $order as $i => $item ) {
		if ($item === '0') {
			$maxItem++;
			$order[ $i ] = $maxItem;
		}
	}

	// deal with deleted items in middle of list
	// MARKER B
	// * javascript that loads into admin interface:
	// builds number of drop down options based on existing number of case studies
	// case studies with order values greater than the number of case studies will be reset to 'select position' and placed at end of order list
	
	if ( $maxItem > $items ) {
		$offset = $items - $maxItem ;
		// reindex all items
		foreach ( $order AS $i => $item ) {
			if ($item > $items ) {
				$order[$i] = $item + $offset;
			}
		}
	}
	*/

	// multisort
	array_multisort( $order, SORT_ASC, $value );
	
	// return	
	return $value;
}
// add_filter('acf/load_value/name=scores', 'my_acf_load_value', 10, 3);
add_filter('acf/load_value/name=case_study_info', 'my_acf_load_value', 10, 3);

// loade4 *BEFORE* my_acf_load_value
// *NOT POSSIBLE TO COUNT NUMBER OF CASE STUDIES*
function acf_load_order_field_choices( $field ) {
	// define option LABELs not values
	$field['choices'] = array(
		'0' => 'Place at bottom of list',
		'1' => '1',
		'2' => '2',
		'3' => '3',
		'4' => '4',
		'5' => '5',
		'6' => '6',
		'7' => '7',
		'8' => '8',
		'9' => '9',
		'10' => '10',
		'11' => '11',
		'12' => '12',
		'13' => '13',
		'14' => '14',
		'15' => '15',
		'16' => '16',
		'17' => '17',
		'18' => '18',
		'19' => '19',
		'20' => '20',
		'21' => '21',
		'22' => '22',
		'23' => '23',
		'24' => '24',
		'25' => '25',
		'26' => '26',
		'27' => '27',
		'28' => '28'
		);
	return $field;
}
// add_filter('acf/load_field/name=case_study_order', 'acf_load_order_field_choices');
add_filter('acf/load_field/key=field_5c98eaab500d6', 'acf_load_order_field_choices');

// attach js file to admin interface
function my_admin_enqueue_scripts() {

	wp_enqueue_script( 'my-admin-js', get_template_directory_uri() . '/js/case_study_sorter.js', array(), '1.0.0', true );

}
add_action('acf/input/admin_enqueue_scripts', 'my_admin_enqueue_scripts');

// run javscript after the ACF fields have been populated in the admin interface
function my_acf_input_admin_footer() {

$fieldKeys = [
	'case_studies' => '594a81e279f5e',
	'ordinate' => 'acf-field_59479e957e9d0-2-field_5ca5c7edfcf09',
	'image' => '594a81fd79f5f',
	'id' => '594a826c79f60',
	'title' => '594a82e479f61'
];

?>
<script type="text/javascript">
/* case study field keys */
var fieldKeys = {
	"case_studies" : "<?php echo $fieldKeys['case_studies']; ?>",
	"ordinate" : "<?php echo $fieldKeys['ordinate']; ?>",
	"image" : "<?php echo $fieldKeys['image']; ?>",
	"id" : "<?php echo $fieldKeys['id']; ?>",
	"title"  : "<?php echo $fieldKeys['title']; ?>"
};
</script>
<?php
}
add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');


function get_acf_key($field_name) {
  global $wpdb;
  $length = strlen($field_name);
  $sql = "
    SELECT `meta_key`
    FROM {$wpdb->postmeta}
    WHERE `meta_key` LIKE 'field_%' AND `meta_value` LIKE '%\"name\";s:$length:\"$field_name\";%';
    ";
  return $wpdb->get_var($sql);
}

////////////////////////////////////////////////
// GOOGLE ANALYTICS
////////////////////////////////////////////////

add_action('wp_footer', 'add_googleanalytics');
function add_googleanalytics() { ?>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-153567086-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	 
	  gtag('config', 'UA-153567086-1');
	</script>
<?php } ?>
