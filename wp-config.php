<?php

define('FORCE_SSL_ADMIN', false);
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
    $_SERVER['HTTPS']='on';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/* DEV Environment setup
point to dev domain and set admin ti http
*/
define('WP_SITEURL', 'http://local.lloydnorthover');
define('WP_HOME', 'http://local.lloydnorthover/');

/** This limits the amount of post revisions */
define('WP_POST_REVISIONS', false);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
/*
define('DB_NAME', 'lloydnorthover');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');
*/


define('DB_NAME', 'lloydnorthover');
define('DB_USER', 'lloydnorthover');
define('DB_PASSWORD', '9zLv76htXU5SAPrw');
define('DB_HOST', 'localhost'); // local dev db locatios

// define('DB_PASSWORD', '7Xk1jiNiMJ5TsDXH');
// define('DB_HOST', '192.168.200.26'); // live db server IP address

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S+FqJj-jRZb|MOcI|`YIma,}&[_;BTH/NTrcbWmym#|TkoTY}`E@R>UaI^#F)*X7');
define('SECURE_AUTH_KEY',  'T+O52TPBC;KGuQi0doz,ax<I#;jmYMBRZYBbe[,%x/0~3GHd!SreG@J4:S`c&T$R');
define('LOGGED_IN_KEY',    'R9t7K#~]k+jdYA|lRvBZhh/t;9)<[trXHVHau0Ki @P>@P|#H^1/{ec([_);N{rM');
define('NONCE_KEY',        ',B:c?8+uGl>6#m$0*Ey+T9oJR1eOw~?+Kn*}@AzZ/%]JW4%Bd-<[x2&:_)j&B~Jh');
define('AUTH_SALT',        '5XpA<(Pq>0SQZN2~Q-{G?~DL%kq_}N]!D8}6(kX!,!_-U|n0{!6+`TkeB6j$7Qx=');
define('SECURE_AUTH_SALT', ':-Vo#5{ah8Vlb=CtUYy[}43)o]`Ts&,{1xDs}(](hgDSO+^5Cf Ztnz_4VG9$s`c');
define('LOGGED_IN_SALT',   ' G7=[oPc!V;JkzpRHrWJL=,ifoGWLuH,<0E@;{]] Wr/wCl<tI}h+q2-sGMv#N2+');
define('NONCE_SALT',       'GS@thI`uJW3U,B;*kT04cmxPncoiw=Yp[_E-Cj;9kwp%@=0^IVe@_fS&.7l=b!|I');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ln2017_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
